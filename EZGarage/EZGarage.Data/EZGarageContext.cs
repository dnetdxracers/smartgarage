﻿using EZGarage.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Data
{
    public class EZGarageContext : DbContext
    {
        public EZGarageContext(DbContextOptions<EZGarageContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<ServiceItem> ServiceItems { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<VehicleType> VehicleTypes { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<UserReset> UserResets { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Model ---
            builder.Entity<Model>()
                .HasOne(m => m.Manufacturer)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            // Service ---
            builder.Entity<Service>()
                .HasOne(se => se.ServiceItem)
                .WithMany()
                .HasForeignKey(jo => jo.ServiceItemId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Service>()
                .HasOne(se => se.Status)
                .WithMany()
                .HasForeignKey(jo => jo.StatusId)
                .OnDelete(DeleteBehavior.Restrict);

            // User ---
            builder.Entity<User>()
                .HasOne(e => e.Role)
                .WithMany()
                .HasForeignKey(r => r.RoleId)
                .OnDelete(DeleteBehavior.Restrict);

            // Vehicle --- 
            builder.Entity<Vehicle>()
                .HasOne(v => v.Model)
                .WithMany()
                .HasForeignKey(v => v.ModelId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Vehicle>()
                .HasOne(v => v.VehicleType)
                .WithMany()
                .HasForeignKey(v => v.VehicleTypeId)
                .OnDelete(DeleteBehavior.Restrict);

            // Visit ---
            builder.Entity<Visit>()
                .HasOne(vi => vi.Vehicle)
                .WithMany()
                .HasForeignKey(vi => vi.VehicleId)
                .OnDelete(DeleteBehavior.Restrict);

            // User reset---
            builder.Entity<UserReset>()
                .HasOne(u => u.User)
                .WithMany()
                .HasForeignKey(u => u.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            // Seed data ------------------------------------------

            // Manufacturers
            var manufacturers = new List<Manufacturer>()
            {
                new Manufacturer
                {
                    Id = 1,
                    Name = "Aston Martin"
                },
                new Manufacturer
                {
                    Id = 2,
                    Name = "Lamborghini"
                },
                new Manufacturer
                {
                    Id = 3,
                    Name = "Ducati"
                },
                new Manufacturer
                {
                    Id = 4,
                    Name = "McLaren"
                },
                new Manufacturer
                {
                    Id = 5,
                    Name = "Ferrari"
                },
                new Manufacturer
                {
                    Id = 6,
                    Name = "Porsche"
                },
                new Manufacturer
                {
                    Id = 7,
                    Name = "Maserati"
                },
                new Manufacturer
                {
                    Id = 8,
                    Name = "Koenigsegg"
                }
            };

            // Models
            var models = new List<Model>()
            {
                new Model
                {
                    Id = 1,
                    ManufacturerId = 1,
                    Name = "Vantage"
                },
                new Model
                {
                    Id = 2,
                    ManufacturerId = 2,
                    Name = "Aventador"
                },
                new Model
                {
                    Id = 3,
                    ManufacturerId = 3,
                    Name = "Panigale"
                },
                new Model
                {
                    Id = 4,
                    ManufacturerId = 1,
                    Name = "DB11"
                },
                new Model
                {
                    Id = 5,
                    ManufacturerId = 2,
                    Name = "Huracan"
                },
                new Model
                {
                    Id = 6,
                    ManufacturerId = 4,
                    Name = "F1"
                },
                new Model
                {
                    Id = 7,
                    ManufacturerId = 5,
                    Name = "Portofino"
                },
                new Model
                {
                    Id = 8,
                    ManufacturerId = 5,
                    Name = "Monza"
                },                
                new Model
                {
                    Id = 9,
                    ManufacturerId = 5,
                    Name = "Roma"
                },
                new Model
                {
                    Id = 10,
                    ManufacturerId = 6,
                    Name = "911"
                },
                new Model
                {
                    Id = 11,
                    ManufacturerId = 6,
                    Name = "Cayenne"
                },
                new Model
                {
                    Id = 12,
                    ManufacturerId = 6,
                    Name = "Panamera"
                },
                new Model
                {
                    Id = 13,
                    ManufacturerId = 6,
                    Name = "Carrera"
                },
                new Model
                {
                    Id = 14,
                    ManufacturerId = 7,
                    Name = "Ghibli"
                },
                new Model
                {
                    Id = 15,
                    ManufacturerId = 7,
                    Name = "Quattroporte"
                },
                new Model
                {
                    Id = 16,
                    ManufacturerId = 8,
                    Name = "Regera"
                }

            };

            // Roles 
            var roles = new List<Role>()
            {
               new Role
               {
                    Id = 1,
                    Name = "Employee"
               },
               new Role
               {
                   Id = 2,
                   Name = "Customer"
               }
            };

            // Users
            var users = new List<User>()
            {
                new User
                {
                    Id = 1,
                    FirstName = "Stoyan",
                    LastName = "Kuklev",
                    Email = "stoyan@ez.garage",
                    Password = "stoyan123",
                    Phone = "0889999988",
                    RoleId = 1
                },
                new User
                {
                    Id = 2,
                    FirstName = "Yana",
                    LastName = "Staneva",
                    Email = "yana@ez.garage",
                    Password = "passw0rd",
                    Phone = "0888888420",
                    RoleId = 1
                },
                new User
                {
                    Id = 3,
                    FirstName = "Anakin",
                    LastName = "Skywalker",
                    Email = "anakinezgarage@abv.bg",
                    Password = "anakin123",
                    Phone = "0888111111",
                    RoleId = 2

                },
                new User
                {
                    Id = 4,
                    FirstName = "Obi-Wan",
                    LastName = "Kenobi",
                    Email = "o.kenobi@gmail.com",
                    Password = "obi1",
                    Phone = "0888222222",
                    RoleId = 2
                },
                new User
                {
                    Id = 5,
                    FirstName = "General",
                    LastName = "Grievous",
                    Email = "grievous@gmail.com",
                    Password = "grievous1",
                    Phone = "0888121212",
                    RoleId = 2
                },
                new User
                {
                    Id = 6,
                    FirstName = "Master",
                    LastName = "Yoda",
                    Email = "stoyanezgarage@abv.bg",
                    Password = "stoyan123",
                    Phone = "0888420024",
                    RoleId = 2,
                } 

            };

            //Vehicles
            var vehicles = new List<Vehicle>()
            {
                new Vehicle
                {
                    Id = 1,
                    CustomerId = 3,
                    ModelId = 1,
                    Year = 2017,
                    RegistrationPlate = "CA1234BK",
                    VehicleTypeId = 1,
                    VIN = "VF7LAKFUC74241674",
                    PriceFactor = 1
                },
                new Vehicle
                {
                    Id = 2,
                    CustomerId = 3,
                    ModelId = 2,
                    Year = 2018,
                    RegistrationPlate = "PK1234AK",
                    VehicleTypeId = 1,
                    VIN = "1GCPKSE72CF169223",
                    PriceFactor = 2
                },
                new Vehicle
                {
                    Id = 3,
                    CustomerId = 4,
                    ModelId = 3,
                    Year = 2020,
                    RegistrationPlate = "Y1234CB",
                    VehicleTypeId = 2,
                    VIN = "VF1RFA00858746148",
                    PriceFactor = 2
                },
                new Vehicle
                {
                    Id = 4,
                    CustomerId = 5,
                    ModelId = 4,
                    Year = 2020,
                    RegistrationPlate = "OB1212AA",
                    VehicleTypeId = 1,
                    VIN = "JN1AZ34E73T007745",
                    PriceFactor = 2
                },
                new Vehicle
                {
                    Id = 5,
                    CustomerId = 1,
                    ModelId = 5,
                    Year = 2019,
                    RegistrationPlate = "PA1452KK",
                    VehicleTypeId = 1,
                    VIN = "1LNHM82W43Y739865",
                    PriceFactor = 1
                },
                new Vehicle
                {
                    Id = 6,
                    CustomerId = 2,
                    ModelId = 6,
                    Year = 2020,
                    RegistrationPlate = "A1234AB",
                    VehicleTypeId = 1,
                    VIN = "1FMJU1H53EEF76511",
                    PriceFactor = 2
                },
                new Vehicle
                {
                    Id = 7,
                    CustomerId = 5,
                    ModelId = 7,
                    Year = 2019,
                    RegistrationPlate = "CO5478KB",
                    VehicleTypeId = 1,
                    VIN = "1FTCR14X3RPA42246",
                    PriceFactor = 1
                },
                new Vehicle
                {
                    Id = 8,
                    CustomerId = 1,
                    ModelId = 8,
                    Year = 2020,
                    RegistrationPlate = "CA6573BB",
                    VehicleTypeId = 1,
                    VIN = "YV1MS382162265147",
                    PriceFactor = 2
                },
                new Vehicle
                {
                    Id = 9,
                    CustomerId = 2,
                    ModelId = 9,
                    Year = 2018,
                    RegistrationPlate = "TX5675AM",
                    VehicleTypeId = 1,
                    VIN = "2GNFLEEK0E6166078",
                    PriceFactor = 3
                },
                new Vehicle
                {
                    Id = 10,
                    CustomerId = 1,
                    ModelId = 10,
                    Year = 2020,
                    RegistrationPlate = "B9182MH",
                    VehicleTypeId = 1,
                    VIN = "JN8AS5MV2CW377355",
                    PriceFactor = 2
                },
                new Vehicle
                {
                    Id = 11,
                    CustomerId = 2,
                    ModelId = 11,
                    Year = 2020,
                    RegistrationPlate = "EB1245MK",
                    VehicleTypeId = 1,
                    VIN = "19UYA42503A001018",
                    PriceFactor = 2
                },
                new Vehicle
                {
                    Id = 12,
                    CustomerId = 3,
                    ModelId = 12,
                    Year = 2019,
                    RegistrationPlate = "A4567AA",
                    VehicleTypeId = 1,
                    VIN = "5J8TB1H22BA800882",
                    PriceFactor = 1
                },
                new Vehicle
                {
                    Id = 13,
                    CustomerId = 4,
                    ModelId = 13,
                    Year = 2018,
                    RegistrationPlate = "CO4587BK",
                    VehicleTypeId = 1,
                    VIN = "JH4DB1554MS802906",
                    PriceFactor = 2
                },
                new Vehicle
                {
                    Id = 14,
                    CustomerId = 5,
                    ModelId = 14,
                    Year = 2019,
                    RegistrationPlate = "CC9685AM",
                    VehicleTypeId = 1,
                    VIN = "JH4KB2F60BC000333",
                    PriceFactor = 2
                },
                new Vehicle
                {
                    Id = 15,
                    CustomerId = 1,
                    ModelId = 15,
                    Year = 2020,
                    RegistrationPlate = "A7894BA",
                    VehicleTypeId = 1,
                    VIN = "2G4GW5EV7B9210677",
                    PriceFactor = 2
                },
                new Vehicle
                { 
                    Id = 16,
                    CustomerId = 2,
                    ModelId = 16,
                    Year = 2020,
                    RegistrationPlate = "Y6457MB",
                    VehicleTypeId = 1,
                    VIN = "1G4JS1110HK411003",
                    PriceFactor = 1
                }
            };

            //VehicleTypes
            var vehicleTypes = new List<VehicleType>()
            {
                new VehicleType
                {
                    Id = 1,
                    Name = "Car"
                },
                new VehicleType
                {
                    Id = 2,
                    Name = "Motorbike"
                }
            };

            // Visits
            var visits = new List<Visit>()
            {
                new Visit
                {
                    Id = 1,
                    CustomerId = 3,
                    ArrivalDate = DateTime.Now.AddDays(-1),
                    VehicleId = 1,
                    Currency = "BGN",
                    TotalPrice = (decimal)(89.99+209.99)

                },
                new Visit
                {
                    Id = 2,
                    CustomerId = 3,
                    ArrivalDate = DateTime.Now.AddDays(-1),
                    VehicleId = 2,
                    Currency = "BGN",
                    TotalPrice = (decimal)(169.99+99.99)
                },
                new Visit
                {
                    Id = 3,
                    CustomerId = 4,
                    ArrivalDate = DateTime.Now.AddDays(-1),
                    VehicleId = 3,
                    Currency = "BGN",
                    TotalPrice = (decimal)(169.99+209.99)
                },
                new Visit
                {
                    Id = 4,
                    CustomerId = 5,
                    ArrivalDate = DateTime.Now.AddDays(-2),
                    VehicleId = 4,
                    Currency = "BGN",
                    TotalPrice = (decimal)(89.99+169.99+209.99)
                }
            };

            // ServiceItems
            var serviceItems = new List<ServiceItem>()
           {
                new ServiceItem
                {
                    Id = 1,
                    Name = "Oil & filter change",
                    Price = (decimal)89.99
                },
                new ServiceItem
                {
                    Id = 2,
                    Name = "Tires & wheels service",
                    Price = (decimal)169.99
                },
                new ServiceItem
                {
                    Id = 3,
                    Name = "Brake repair",
                    Price = (decimal)209.99,
                },
                new ServiceItem
                {
                    Id = 4,
                    Name = "Vehicle diagnostics",
                    Price = (decimal)99.99
                },
           };

            // Statuses
            var statuses = new List<Status>()
            {
                new Status
                {
                    Id = 1,
                    Name="Not Started"
                },
                new Status
                {
                    Id = 2,
                    Name="In Progress"
                },
                new Status
                {
                    Id = 3,
                    Name="Ready"
                }
            };

            // Services
            var services = new List<Service>()
            {
                new Service
                {
                    Id = 1,
                    ServiceItemId = 1,
                    StatusId = 2,
                    VisitId = 1,
                    ReadyDate=DateTime.Now.AddDays(2)

                },
                new Service
                {
                    Id = 2,
                    ServiceItemId = 2,
                    StatusId = 1,
                    VisitId = 2,
                    ReadyDate=DateTime.Now.AddDays(3)
                },
                new Service
                {
                    Id = 3,
                    ServiceItemId = 3,
                    StatusId = 2,
                    VisitId = 1,
                    ReadyDate=DateTime.Now.AddDays(5)
                },
                new Service
                {
                    Id = 4,
                    ServiceItemId = 4,
                    StatusId = 3,
                    VisitId = 2,
                    ReadyDate=DateTime.Now.AddDays(1)
                },
                new Service
                {
                    Id = 5,
                    ServiceItemId = 3,
                    StatusId = 3,
                    VisitId = 3,
                    ReadyDate = DateTime.Now.AddDays(3)
                },
                new Service
                {
                    Id = 6,
                    ServiceItemId = 2,
                    StatusId = 2,
                    VisitId = 3,
                    ReadyDate = DateTime.Now.AddDays(4)
                },
                new Service
                {
                    Id = 7,
                    ServiceItemId = 1,
                    StatusId = 1,
                    VisitId = 4,
                    ReadyDate = DateTime.Now.AddDays(2)
                },
                new Service
                {
                    Id = 8,
                    ServiceItemId = 2,
                    StatusId = 2,
                    VisitId = 4,
                    ReadyDate = DateTime.Now.AddDays(2)
                },
                new Service
                {
                    Id = 9,
                    ServiceItemId = 3,
                    StatusId = 2,
                    VisitId = 4,
                    ReadyDate = DateTime.Now.AddDays(2)
                },
            };

            var userResets = new List<UserReset>();

            builder.Entity<ServiceItem>().Property(si => si.Price).HasColumnType("decimal");
            builder.Entity<Visit>().Property(vi => vi.TotalPrice).HasColumnType("decimal");
            builder.Entity<Role>().HasData(roles);
            builder.Entity<ServiceItem>().HasData(serviceItems);
            builder.Entity<Status>().HasData(statuses);
            builder.Entity<Service>().HasData(services);
            builder.Entity<VehicleType>().HasData(vehicleTypes);
            builder.Entity<Manufacturer>().HasData(manufacturers);
            builder.Entity<Model>().HasData(models);
            builder.Entity<Vehicle>().HasData(vehicles);
            builder.Entity<Visit>().HasData(visits);
            builder.Entity<User>().HasData(users);
            builder.Entity<UserReset>().HasData(userResets);
            base.OnModelCreating(builder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }
    }
}
