﻿using EZGarage.Data.Models.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace EZGarage.Data.Models
{
    public class Vehicle
    {
        /// <summary>
        /// Vehicle Id property 
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Customer Id property
        /// </summary>
        [Required]
        public int CustomerId { get; set; }

        /// <summary>
        /// Customer property
        /// </summary>
        public User Customer { get; set; }

        /// <summary>
        /// Type Id property
        /// </summary>
        [Required]
        public int VehicleTypeId { get; set; }

        /// <summary>
        /// Type property
        /// </summary>
        public VehicleType VehicleType { get; set; }

        /// <summary>
        /// Model Id property
        /// </summary>
        [Required]
        public int ModelId { get; set; }
        
        /// <summary>
        /// Model property
        /// </summary>
        public Model Model { get; set; }

        /// <summary>
        /// Year property
        /// </summary>
        [Required]
        public int Year { get; set; }

        /// <summary>
        /// Registration plate property
        /// </summary>
        [Required]
        [RegistrationPlateAttribute]
        public string RegistrationPlate { get; set; }

        /// <summary>
        /// Vehicle identification number property
        /// </summary>
        [Required]
        public string VIN { get; set; }

        /// <summary>
        /// IsDeleted boolean property
        /// </summary>
        [JsonIgnore]
        public bool IsDeleted { get; set; } = false;

        /// <summary>
        /// Vehicle price factor property
        /// </summary>
        public int PriceFactor { get; set; }

    }
}
