﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace EZGarage.Data.Models
{
    public class Visit
    {

        /// <summary>
        /// Visit Id property
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Vehicle Id property
        /// </summary>
        [Required]
        public int VehicleId { get; set; }

        /// <summary>
        /// Vehicle property
        /// </summary>
        public Vehicle Vehicle { get; set; }

        /// <summary>
        /// Customer Id property
        /// </summary>
        [Required]
        public int CustomerId { get; set; }

        /// <summary>
        /// Customer property
        /// </summary>
        public User Customer { get; set; }

        /// <summary>
        /// Date when vehicle arrived at the repair shop property
        /// </summary>
        [Required]
        public DateTime ArrivalDate { get; set; }

        /// <summary>
        /// List of services property
        /// </summary>
        public List<Service> Services { get; set; } = new List<Service>();

        /// <summary>
        /// Date when vehicle is ready for pickup property
        /// </summary>
        public DateTime ReadyDate { get; set; }

        /// <summary>
        /// Total price for all services property
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// Currency used
        /// </summary>
        public string Currency { get; set; } = "BGN";
    }
}
