﻿using EZGarage.Data.Models.Util;
using EZGarage.Exceptions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace EZGarage.Data.Models
{
    public class User
    {
        /// <summary>
        /// Fields
        /// </summary>
        private string firstName;
        private string lastName;
        private string password;

        /// <summary>
        /// Constants
        /// </summary>
        private const int minLength = Constants.nameMinLength;
        private const int maxLength = Constants.nameMaxLength;

        /// <summary>
        /// Id property
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Role Id property
        /// </summary>
        public int RoleId { get; set; } = 2;

        /// <summary>
        /// Role property
        /// </summary>
        public Role Role { get; set; }

        /// <summary>
        /// Password property
        /// </summary>
        [Required]
        [JsonIgnore]
        [MinLength(minLength), MaxLength(maxLength)]
        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                if (value.Length < minLength || value.Length > maxLength)
                    throw new InvalidStringLength("Password", minLength, maxLength);

                this.password = value;
            }
        }

        /// <summary>
        /// First name property
        /// </summary>
        [Required]
        [MinLength(minLength), MaxLength(maxLength)]
        public string FirstName
        {
            get
            {
                return this.firstName;
            }
            set
            {
                if (value.Length < minLength || value.Length > maxLength)
                    throw new InvalidStringLength("First Name", minLength, maxLength);

                this.firstName = value;
            }
        }

        /// <summary>
        /// Last name property
        /// </summary>
        [MinLength(minLength), MaxLength(maxLength)]
        public string LastName
        {
            get
            {
                return this.lastName;
            }
            set
            {
                if (value.Length < minLength || value.Length > maxLength)
                    throw new InvalidStringLength("First Name", minLength, maxLength);

                this.lastName = value;
            }
        }

        /// <summary>
        /// Phone property
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Email address property
        /// </summary>
        [Required]
        [EmailAddress]
        [EmailFormatValidator]
        public string Email { get; set; }

        /// <summary>
        /// List of vehicles property
        /// </summary>
        public List<Vehicle> Vehicles { get; set; } = new List<Vehicle>();

        /// <summary>
        /// List of visits property
        /// </summary>
        public List<Visit> Visits { get; set; } = new List<Visit>();

        /// <summary>
        /// Is Deleted boolean property for soft delete
        /// </summary>
        [JsonIgnore]
        public bool IsDeleted { get; set; } = false;

    }
}
