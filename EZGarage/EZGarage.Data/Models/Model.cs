﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EZGarage.Data.Models
{
    public class Model
    {
        /// <summary>
        /// Model Id property
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Model name property
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Manufacturer Id property
        /// </summary>
        [Required]
        public int ManufacturerId { get; set; }

        /// <summary>
        /// Manufacturer property
        /// </summary>
        public Manufacturer Manufacturer { get; set; }
    }
}
