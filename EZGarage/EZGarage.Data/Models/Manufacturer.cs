﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EZGarage.Data.Models
{
    public class Manufacturer
    {
        /// <summary>
        /// Manufacturer Id property
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Manufacturer name property
        /// </summary>
        public string Name { get; set; }
    }
}
