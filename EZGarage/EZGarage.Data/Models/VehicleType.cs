﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EZGarage.Data.Models
{
    public class VehicleType
    {
        /// <summary>
        /// Vehicle type Id property
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Vehicle type name property
        /// </summary>
        public string Name { get; set; }
    }
}
