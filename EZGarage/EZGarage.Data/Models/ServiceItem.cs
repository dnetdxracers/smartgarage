﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EZGarage.Data.Models
{
    public class ServiceItem
    {
        /// <summary>
        /// ServiceItem Id property
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// ServiceItem name property
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// ServiceItem price property
        /// </summary>
        [Required]
        public decimal Price { get; set; }

        /// <summary>
        /// Currency used
        /// </summary>
        public string Currency { get; set; } = "BGN";

        /// <summary>
        /// IsDeleted boolean property
        /// </summary>
        public bool IsDeleted { get; set; } = false;
    }
}
