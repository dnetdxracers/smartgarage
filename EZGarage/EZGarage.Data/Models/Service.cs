﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EZGarage.Data.Models
{
    public class Service
    {
        /// <summary>
        /// Service Id property
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Service item Id property
        /// </summary>
        [Required]
        public int ServiceItemId { get; set; }

        /// <summary>
        /// Service item property
        /// </summary>
        public ServiceItem ServiceItem { get; set; }
        
        /// <summary>
        /// Status Id property
        /// </summary>
        [Required]
        public int StatusId { get; set; } = 1;

        /// <summary>
        /// Status property
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Expected or finallized date
        /// </summary>
        public DateTime ReadyDate { get; set; }

        /// <summary>
        /// Visit Id property
        /// </summary>
        [Required]
        public int VisitId { get; set; }
        
        /// <summary>
        /// Visit property
        /// </summary>
        public Visit Visit { get; set; }

        /// <summary>
        /// IsDeleted boolean property
        /// </summary>
        public bool IsDeleted { get; set; } = false;
    }
}
