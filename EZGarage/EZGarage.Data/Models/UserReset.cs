﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace EZGarage.Data.Models
{
    public class UserReset
    {
        /// <summary>
        /// Id property
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// UserId property
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// User property
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Temporary password property
        /// </summary>
        [JsonIgnore]
        public string TemporaryPassword { get; set; }

        /// <summary>
        /// Expirty date property
        /// </summary>
        public DateTime ExpiryDate { get; set; }

        /// <summary>
        /// Email property
        /// </summary>
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Boolean prop for expiration
        /// </summary>
        public bool IsExpired { get; set; }

    }
}
