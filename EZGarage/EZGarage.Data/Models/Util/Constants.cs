﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Data.Models.Util
{
    public static class Constants
    {
        public const int nameMinLength = 1;
        public const int nameMaxLength = 50;
    }
}
