﻿using System;
using System.Globalization;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;

namespace EZGarage.Data.Models.Util
{
    
    //public class EmailValidatorrAttribute : ValidationAttribute
    //{
    //    //Regex.IsMatch(email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$")

    //    public override bool IsValid(object value)
    //    {
    //        var email = value.ToString();
    //        if (Regex.IsMatch(email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
    //            return false;

    //        return true;
    //    }

    //}

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class EmailFormatValidatorAttribute : ValidationAttribute
    {
        public EmailFormatValidatorAttribute()
            : base("{0} is invalid email")
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //decimal val = (decimal)value;

            //bool valid = val % 7 == 0;

            //if (valid)
            //    return null;

            //return new ValidationResult(base.FormatErrorMessage(validationContext.MemberName)
            //    , new string[] { validationContext.MemberName });

            var email = value.ToString();
            if (Regex.IsMatch(email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
                return null;

            return new ValidationResult(base.FormatErrorMessage(validationContext.MemberName)
                , new string[] { validationContext.MemberName });

        }
    }


}
