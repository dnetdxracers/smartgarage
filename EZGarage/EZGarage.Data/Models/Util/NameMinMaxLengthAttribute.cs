﻿using EZGarage.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EZGarage.Data.Models.Util
{
    public class NameMinMaxLengthAttribute : ValidationAttribute
    {
      

        public string GetErrorMessage() =>
        $"Invalid property parameters.";

        protected override ValidationResult IsValid(object value,
        ValidationContext validationContext)
        {
            var datatestSubject = value.ToString();
            //var validationDataContext = validationContext.ObjectInstance.ToString();

            if (datatestSubject.Length>10)
            {
                return new ValidationResult(GetErrorMessage());
            }
            //if (validationDataContext.Length > 10)
            //{
            //    return new ValidationResult(GetErrorMessage());
            //}
            


            return ValidationResult.Success;
        }


    }
}
