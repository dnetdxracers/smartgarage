﻿using EZGarage.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace EZGarage.Data.Models.Util
{
    public class RegistrationPlateAttribute : Attribute
    {
        private string registrationPlate;

        public string RegistrationPlate { 
            get => this.registrationPlate; 
            set
            {
                if (value.Length < 7 || value.Length > 8 || !Regex.Match(value, @"[A - Z]{ 1,2}\d{ 4}
                [A-Z]{ 2}").Success)
                {
                    throw new InvalidRegPlate(value);
                }
                this.registrationPlate = value;
            }

        }
    }
}