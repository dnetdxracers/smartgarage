﻿using Microsoft.EntityFrameworkCore;
using EZGarage.Data;
using System;
using EZGarage.Data.Models;

namespace EZGarage.Tests.Utils
{
    public static class Util
    {
        public static DbContextOptions<T> GetInMemoryDatabaseOptions<T>()
            where T : DbContext
        {
            var options = new DbContextOptionsBuilder<T>()
                .UseInMemoryDatabase("FakeDatabase")
                .Options;

            return options;
        }

        public static void SeedDatabase(EZGarageContext context)
        {
            // Customers
            context.Users.AddRange(
                new User
                {
                    Id = 3,
                    FirstName = "Anakin",
                    LastName = "Skywalker",
                    Email = "a.skywalker@gmail.com",
                    Password = "anakin1",
                    Phone = "0888111111",
                    RoleId = 2
                },
                new User
                {
                    Id = 4,
                    FirstName = "Obi-Wan",
                    LastName = "Kenobi",
                    Email = "o.kenobi@gmail.com",
                    Password = "obi1",
                    Phone = "0888222222",
                    RoleId = 2
                },
                new User
                 {
                     Id = 1,
                     FirstName = "Stoyan",
                     LastName = "Kuklev",
                     Email = "stoyan@ez.garage",
                     Password = "stoyan123",
                     Phone = "0889999988",
                     RoleId = 1
                 },
                new User
                {
                    Id = 2,
                    FirstName = "Yana",
                    LastName = "Staneva",
                    Email = "yana@ez.garage",
                    Password = "passw0rd",
                    Phone = "0888888420",
                    RoleId = 1
                }
                );

            // Sevices
            context.Services.AddRange(
                new Service
                {
                    Id = 1,
                    ServiceItemId = 1,
                    StatusId = 1,
                    VisitId = 1

                },
                new Service
                {
                    Id = 2,
                    ServiceItemId = 2,
                    StatusId = 1,
                    VisitId = 2

                },
                new Service
                {
                    Id = 3,
                    ServiceItemId = 3,
                    StatusId = 2,
                    VisitId = 1
                },
                new Service
                {
                    Id = 4,
                    ServiceItemId = 4,
                    StatusId = 3,
                    VisitId = 2
                }
                );

            // Manufacturers
            context.Manufacturers.AddRange(
                new Manufacturer
                {
                    Id = 1,
                    Name = "Toyota"
                },
                new Manufacturer
                {
                    Id = 2,
                    Name = "Lamborghini"
                },
                new Manufacturer
                {
                    Id = 3,
                    Name = "Yamaha"
                }
                );

            // Models
            context.Models.AddRange(
                new Model
                {
                    Id = 1,
                    ManufacturerId = 1,
                    Name = "Yaris"
                },
                new Model
                {
                    Id = 2,
                    ManufacturerId = 2,
                    Name = "Aventador"
                },
                new Model
                {
                    Id = 3,
                    ManufacturerId = 3,
                    Name = "FZ 150"
                }
                );

            // Roles
            context.Roles.AddRange(
               new Role
               {
                   Id = 1,
                   Name = "Employee"
               },
               new Role
               {
                   Id = 2,
                   Name = "Customer"
               }
                );

            // Vehicles
            context.Vehicles.AddRange(
                new Vehicle
                {
                    Id = 1,
                    CustomerId = 3,
                    ModelId = 1,
                    Year = 2014,
                    RegistrationPlate = "CA1234BK",
                    VehicleTypeId = 1,
                    VIN = "VF7LAKFUC74241622",
                    IsDeleted = false,
                    PriceFactor = 2
                },
                new Vehicle
                {
                    Id = 2,
                    CustomerId = 3,
                    ModelId = 2,
                    Year = 2012,
                    RegistrationPlate = "PK1234AK",
                    VehicleTypeId = 1,
                    VIN = "1GCPKSE72CF169223",
                    IsDeleted = false,
                    PriceFactor = 2
                },
                new Vehicle
                {
                    Id = 3,
                    CustomerId = 4,
                    ModelId = 3,
                    Year = 2020,
                    RegistrationPlate = "Y1234CB",
                    VehicleTypeId = 2,
                    VIN = "VF1RFA00858746148",
                    IsDeleted = false,
                    PriceFactor = 2
                }
                );

            // VehicleTypes
            context.VehicleTypes.AddRange(
                new VehicleType
                {
                    Id = 1,
                    Name = "Car"
                },
                new VehicleType
                {
                    Id = 2,
                    Name = "Motorbike"
                }
                );

            // Services
            context.ServiceItems.AddRange(
                new ServiceItem
                {
                    Id = 1,
                    Name = "Oil & filter change",
                    Price = (decimal)19.99
                },
                new ServiceItem
                {
                    Id = 2,
                    Name = "Tires & wheels service",
                    Price = (decimal)29.99
                },
                new ServiceItem
                {
                    Id = 3,
                    Name = "Brake repair",
                    Price = (decimal)99.99,
                },
                new ServiceItem
                {
                    Id = 4,
                    Name = "Vehicle diagnostics",
                    Price = (decimal)49.99
                }
                );

            // Statuses
            context.Statuses.AddRange(
                new Status
                {
                    Id = 1,
                    Name = "Not Started"
                },
                new Status
                {
                    Id = 2,
                    Name = "In Progress"
                },
                new Status
                {
                    Id = 3,
                    Name = "Ready For Pickup"
                }
                );

            // Visits
            context.Visits.AddRange(
                new Visit
                {
                    Id = 1,
                    ArrivalDate = DateTime.Now.AddDays(-2),
                    VehicleId = 1,
                    CustomerId = 1
                },
                new Visit
                {
                    Id = 2,
                    ArrivalDate = DateTime.Now.AddDays(-3),
                    VehicleId = 2,
                    CustomerId = 2
                }
                );
        }
    }
}
