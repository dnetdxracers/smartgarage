﻿using EZGarage.Data;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Tests.MailServiceTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void InstantiateProperly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {

                var mailSettings = new Mock<IOptions<MailSettings>>().Object;

                // Act
                var sut = new MailService(mailSettings);

                // Assert
                Assert.IsInstanceOfType(sut, typeof(MailService));
            }
        }

    }
}
