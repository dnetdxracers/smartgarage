﻿using EZGarage.Data;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Services.Util.Models;
using EZGarage.Tests.Utils;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Tests.MailServiceTests
{
    [TestClass]
    public class SendEmailAsync_Should
    {
        [TestMethod]
        public void InstantiateProperly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {

                var mailSettings = new Mock<IOptions<MailSettings>>().Object;
                var mailService = new MailService(mailSettings);

                var mailRequest = new MailRequest()
                {
                    Subject = "test subject",
                    Body = "test body",
                    ToEmail = "test@email.com"
                };


                // Act
                var sut = mailService.SendEmailAsync(mailRequest);

                // Assert
                Assert.IsInstanceOfType(sut, typeof(Task));
            }
        }

    }
}
