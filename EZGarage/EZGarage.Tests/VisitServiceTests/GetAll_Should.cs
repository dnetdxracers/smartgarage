﻿using EZGarage.Data;
using EZGarage.Services.Contracts;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.VisitServiceTests
{
    [TestClass]
    public class GetAll_Should
    {
        [TestMethod]
        public async Task ExecuteCorrectly_VehicleId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var sut = await visitService.GetAllAsync(1, null, null, null, null, null, null, null, null, null, null, null, null, null);

                // Assert
                Assert.AreEqual(1, sut.Count());
            }

        }

        [TestMethod]
        public async Task ExecuteCorrectly_RegPlate()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var sut = await visitService.GetAllAsync(null, null, "CA1234BK", null, null, null, null, null, null, null, null, null, null, null, null);

                // Assert
                Assert.AreEqual(1, sut.Count());
            }
        }


        [TestMethod]
        public async Task ExecuteCorrectly_VIN_Single()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var sut = await visitService.GetAllAsync(null, null,  null, "LAK", null, null, null, null, null, null, null, null, null, null);

                // Assert
                Assert.AreEqual(1, sut.Count());
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_VIN_Multiple()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var sut = await visitService.GetAllAsync(null, null,  null, "22", null, null, null, null, null, null, null, null, null, null);

                // Assert
                Assert.AreEqual(2, sut.Count());
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_Model()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var sut = await visitService.GetAllAsync(null, null, null, null, "yar", null, null, null, null, null, null, null, null, null);

                // Assert
                Assert.AreEqual(1, sut.Count());
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_ModelId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var sut = await visitService.GetAllAsync(null, null, null, null, null, 1, null, null, null, null, null, null, null, null);

                // Assert
                Assert.AreEqual(1, sut.Count());
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_Manufacturer()
        {
            // Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var sut = await visitService.GetAllAsync(null, null, null, null, null, null, "toyo", null, null, null, null, null, null, null, null);

                // Assert
                Assert.AreEqual(1, sut.Count());
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_ManufacturerId()
        {
            // Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var sut = await visitService.GetAllAsync(null, null, null, null, null, null, null, 1, null, null, null, null, null, null, null);

                // Assert
                Assert.AreEqual(1, sut.Count());
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_DateAfter()
        {
            // Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var date = DateTime.Now.AddDays(-2).ToString();

                var sut = await visitService.GetAllAsync(null, null, null, null, null, null, null,  null, date, null, null, null, null, null);

                // Assert
                Assert.AreEqual(1 , sut.Count());
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_DateBefore()
        {
            // Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var date = DateTime.Now.AddDays(-2).ToString();

                var sut = await visitService.GetAllAsync(null, null, null, null, null, null, null, null, null, DateTime.Now.ToString("dd-MMM-yyyy"),null, null, null, null, null);

                // Assert
                Assert.AreEqual(2, sut.Count());
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_ServiceId()
        {
            // Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();
                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var sut = await visitService.GetAllAsync(null, null, null, null, null, null, null, null, null, null, 1, null, null, null, null);

                // Assert
                Assert.AreEqual(1, sut.Count());
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_ServiceItemId()
        {
            // Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var sut = await visitService.GetAllAsync(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                // Assert
                Assert.AreEqual(context.Visits.Count(), sut.Count());
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_ServiceItem()
        {
            // Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var sut = await visitService.GetAllAsync(null, null, null, null, null, null, null, null, null, null, null, null, "oil", null, null);

                // Assert
                Assert.AreEqual(1, sut.Count());
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_StatusId()
        {
            // Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);


                var sut = await visitService.GetAllAsync(null, null, null, null, null, null, null, null, null, null, null, null, null, 1, null);

                // Assert
                Assert.AreEqual(2, sut.Count());
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_Status()
        {
            // Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var sut = await visitService.GetAllAsync(null, null, null, null, null, null, null, null, null, null, null, null, null, null, "ready");

                // Assert
                Assert.AreEqual(1, sut.Count());
            }
        }
    }
}
