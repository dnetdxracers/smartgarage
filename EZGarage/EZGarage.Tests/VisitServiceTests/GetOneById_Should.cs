﻿using EZGarage.Data;
using EZGarage.Data.Models;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto.Services;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.VisitServiceTests
{
    [TestClass]
    public class GetOneById_Should
    {
        [TestMethod]
        public async Task Execute_Correctly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var visit = new Visit
                {
                    Id = 99,
                    VehicleId = 1,
                    ReadyDate = DateTime.Now.AddDays(2),
                    CustomerId = 3,
                    ArrivalDate = DateTime.Now,

                };

                await context.Visits.AddAsync(visit);
                await context.SaveChangesAsync();

                var sut = await context.Visits.FirstOrDefaultAsync(x=>x.Id== 99);

                // Assert
                Assert.IsTrue(sut.Id == visit.Id);
            }

        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task Throw_NonExistentItemId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                // Act & Assert
                var sut = await visitService.GetOneByIdAsync(99);
            }
        }
    }
}
