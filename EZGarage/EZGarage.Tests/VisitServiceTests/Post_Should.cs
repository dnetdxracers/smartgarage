﻿using EZGarage.Data;
using EZGarage.Data.Models;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto.Visits;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.VisitServiceTests
{
    [TestClass]
    public class Post_Should
    {
        [TestMethod]
        public async Task ExecuteCorrectly_OneService()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var service = context.Services.First();


                var visit = new Visit
                {
                    CustomerId = 3,
                    ArrivalDate = DateTime.Now.AddDays(-1),
                    VehicleId = 1
                };
                var listServices = new int[] { 1 };
                var finisDates = new string[] { "15-jun-2021" };
                var postVisitDto = new PostVisitDto(
                    visit.VehicleId,
                    visit.CustomerId,
                    visit.ArrivalDate.ToString(),
                    listServices,
                    finisDates
                    );

                // Act
                await visitService.PostAsync(postVisitDto);
                await context.SaveChangesAsync();

                // Assert
                Assert.AreEqual(3, context.Visits.Count());
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_SeveralServices()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();


                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);

                var visit = new Visit
                {
                    CustomerId = 3,
                    ArrivalDate = DateTime.Now.AddDays(-1),
                    TotalPrice = 100.0m,
                    VehicleId = 1
                };

                var listServices = new int[] { 1 };
                var finisDates = new string[] { "15-jun-2021" };
                var postVisitDto = new PostVisitDto(
                    visit.VehicleId,
                    visit.CustomerId,
                    visit.ArrivalDate.ToString(),
                    listServices,
                    finisDates
                    );

                // Act
                await visitService.PostAsync(postVisitDto);
                await context.SaveChangesAsync();

                // Assert
                Assert.AreEqual(3, context.Visits.Count());
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task Throws_NonExistentVehicleId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceServiceMock = new Mock<IServiceService>().Object;

                var visitService = new VisitService(context, serviceServiceMock);


                var visit = new Visit
                {
                    CustomerId = 1,
                    ArrivalDate = DateTime.Now.AddDays(-1),
                    TotalPrice = 100.0m,
                    VehicleId = 100
                };
                var listServices = new int[] { 1 };
                var finisDates = new string[] { "15-jun-2021" };
                var postVisitDto = new PostVisitDto(
                    visit.VehicleId,
                    visit.CustomerId,
                    visit.ArrivalDate.ToString(),
                    listServices,
                    finisDates
                    );

                // Act & Assert
                await visitService.PostAsync(postVisitDto);
                await context.SaveChangesAsync();

            }
        
        }
    }
}
