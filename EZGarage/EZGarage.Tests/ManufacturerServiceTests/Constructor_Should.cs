﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Tests.ManufacturerServiceTests
{
    [TestClass]
    public class GetManufacturerById_Should
    {
        [TestMethod]
        public void Execute_Correctly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();
                var manufacturerService = new ManufacturerService(context);

                // Act
                var sut = manufacturerService.GetManufacturerByIdAsync(1).Result;

                // Assert
                Assert.AreEqual(sut.Name, "Toyota");
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task Throw_NonExistentId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();
                var manufacturerService = new ManufacturerService(context);

                // Act
                var sut = await manufacturerService.GetManufacturerByIdAsync(100);

                // Assert
            }
        }
    }
}
