﻿using EZGarage.Data;
using EZGarage.Data.Models;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Tests.ManufacturerServiceTests
{
    [TestClass]
    public class Put_Should
    {
        [TestMethod]
        public async Task Execute_Correctly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();
                var manufacturerService = new ManufacturerService(context);

                // Act
                var newManufacturerName = "Maserati";
                var manufCount = context.Manufacturers.Count();
                var sut = await manufacturerService.PutAsync(1, newManufacturerName);

                var updatedManuf = context.Manufacturers.FirstOrDefault(m => m.Id == 1);

                // Assert
                Assert.AreEqual(newManufacturerName, updatedManuf.Name);
                await context.Database.EnsureDeletedAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NameNullOrEmpty))]
        public async Task Throw_EmptyName()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();
                var manufacturerService = new ManufacturerService(context);

                // Act
                var newManufacturerName = "";
                var manufCount = context.Manufacturers.Count();
                var sut = await manufacturerService.PutAsync(1, newManufacturerName);

                var updatedManuf = context.Manufacturers.FirstOrDefault(m => m.Id == 1);

                // Assert
                await context.Database.EnsureDeletedAsync();
            }
        }
        [TestMethod]
        [ExpectedException(typeof(NegativeIdSelected))]
        public async Task Throw_NegativeId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();
                var manufacturerService = new ManufacturerService(context);

                // Act
                var newManufacturerName = "";
                var manufCount = context.Manufacturers.Count();
                var sut = await manufacturerService.PutAsync(-100, newManufacturerName);

                var updatedManuf = context.Manufacturers.FirstOrDefault(m => m.Id == 1);

                // Assert
                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}
