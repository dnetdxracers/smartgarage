﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Tests.ManufacturerServiceTests
{
    [TestClass]
    public class GetManufacturerByName_Should
    {
        [TestMethod]
        public void Execute_Correctly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();
                var manufacturerService = new ManufacturerService(context);

                // Act
                var sut = manufacturerService.GetManufacturerByNameAsync("toyota").Result;

                // Assert
                Assert.AreEqual(sut.Id, 1);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithName))]
        public async Task Throw_NonExistentName()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();
                var manufacturerService = new ManufacturerService(context);

                // Act
                var sut = await manufacturerService.GetManufacturerByNameAsync("nonexistentname");

                // Assert
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithName))]
        public async Task Throw_NonExistentNameEmpty()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();
                var manufacturerService = new ManufacturerService(context);

                // Act
                var sut = await manufacturerService.GetManufacturerByNameAsync("");

                // Assert
            }
        }
    }
}
