﻿using EZGarage.Data;
using EZGarage.Data.Models;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Tests.ManufacturerServiceTests
{
    [TestClass]
    public class Post_Should
    {
        [TestMethod]
        public async Task Execute_Correctly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();
                var manufacturerService = new ManufacturerService(context);

                // Act
                var newManufacturerName = "Maserati";
                var manufCount = context.Manufacturers.Count();
                var sut = await manufacturerService.PostAsync(newManufacturerName);

                var newManufCount = context.Manufacturers.Count();

                // Assert
                Assert.AreEqual(manufCount + 1, newManufCount);
                await context.Database.EnsureDeletedAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NameNullOrEmpty))]
        public async Task Throw_EmptyName()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();
                var manufacturerService = new ManufacturerService(context);

                // Act
                var newManufacturerName = "";
                var manufCount = context.Manufacturers.Count();
                var sut = await manufacturerService.PostAsync(newManufacturerName);

                var newManufCount = context.Manufacturers.Count();

                // Assert
                Assert.AreEqual(manufCount + 1, newManufCount);
                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}
