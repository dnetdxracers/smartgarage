﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Tests.ManufacturerServiceTests
{
    [TestClass]
    public class GetManufacturersSelect_Should
    {
        [TestMethod]
        public void Execute_Correctly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();
                var manufacturerService = new ManufacturerService(context);

                // Act
                var sut = manufacturerService.GetManufacturersSelectList();

                // Assert
                Assert.IsInstanceOfType(sut, typeof(SelectList));
            }
        }
    }
}
