﻿using EZGarage.Data;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Tests.ManufacturerServiceTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void Execute_Correctly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {



                // Act
                var sut = new ManufacturerService(context);

                // Assert
                Assert.IsInstanceOfType(sut, typeof(ManufacturerService));
            }
        }
    }
}
