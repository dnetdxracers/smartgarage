﻿using EZGarage.Data;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Tests.VehicleTypeServiceTests
{
    [TestClass]
    public class GetAll_Should
    {
        [TestMethod]
        public async Task ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleTypeService(context);


                var sut = await vehicleService.GetAllAsync();

                // Assert
                Assert.AreEqual(2, sut.Count());

                await context.Database.EnsureDeletedAsync();
            }
        }

    }
}