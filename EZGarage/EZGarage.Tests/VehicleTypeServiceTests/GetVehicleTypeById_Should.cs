﻿using EZGarage.Data;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Tests.VehicleTypeServiceTests
{
    [TestClass]
    public class GetVehicleTypeById_Should
    {
        [TestMethod]
        public async Task ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleTypeService(context);

                var sut = await vehicleService.GetVehicleTypeByIdAsync(1);

                // Assert
                Assert.AreEqual(1, sut.Id);

                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}
