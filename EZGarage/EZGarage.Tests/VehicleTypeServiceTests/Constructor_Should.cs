﻿using EZGarage.Data;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Tests.VehicleTypeServiceTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void InstantiateProperly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {

                // Act
                var sut = new VehicleTypeService(context);

                // Assert
                Assert.IsInstanceOfType(sut, typeof(VehicleTypeService));
            }
        }
    }
}
