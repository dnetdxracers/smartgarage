﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;
namespace EZGarage.Tests.ServiceItemServiceTests
{
    [TestClass]
    public class GetOneByNameAsync_Should

    {
        [DataRow(1)]
        [DataRow(2)]
        [DataRow(3)]
        [TestMethod]
        public async Task GetProperServiceItem(int serviceItemId)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceItemService = new ServiceItemService(context);

                //Act
                var name = context.ServiceItems.FirstOrDefault(si=>si.Id == serviceItemId).Name;
                var sut = await serviceItemService.GetOneByNameAsync(name);

                //Assert
                Assert.IsTrue(sut.Name == name);

                await context.Database.EnsureDeletedAsync();
            }
        }

        [DataRow("randomtestname")]
        [DataRow("impossiblename")]
        [DataRow("cannotbenamedthis")]
        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithName))]
        public async Task ThrowIfUnexistingInDb(string invalidTestName)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceItemService = new ServiceItemService(context);

                //Act & Assert
                var sut = await serviceItemService.GetOneByNameAsync(invalidTestName);


                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}
