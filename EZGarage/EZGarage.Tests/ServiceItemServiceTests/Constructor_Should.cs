﻿using EZGarage.Data;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EZGarage.Tests.ServiceItemServiceTests

{
    [TestClass]
    public class Constructor_Should
    {

        [TestMethod]
        public void InstantiateProperly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {

                var sut = new ServiceItemService(context);

                //Act & Assert
                Assert.IsInstanceOfType(sut, typeof(ServiceItemService));
            }
        }
    }
}
