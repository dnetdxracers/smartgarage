﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;
namespace EZGarage.Tests.ServiceItemServiceTests
{
    [TestClass]
    public class GetOneByIdAsync_Should
    {
        [TestMethod]
        public async Task GetProperServiceItem()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceItemService = new ServiceItemService(context);

                //Act
                var sut =await serviceItemService.GetOneByIdAsync(1);

                //Assert
                Assert.IsTrue(sut.Id == context.ServiceItems.FirstOrDefault(si => si.Id == 1).Id);

                await context.Database.EnsureDeletedAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task ThrowIfUnexistingInDb()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceItemService = new ServiceItemService(context);

                //Act & Assert
                var sut = await serviceItemService.GetOneByIdAsync(9321);


                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}
