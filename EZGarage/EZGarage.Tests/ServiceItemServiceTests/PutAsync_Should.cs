﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Dto.ServiceItems;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.ServiceItemServiceTests
{
    [TestClass]
    public class PutAsync_Should
    {
        [DataRow(1, "randomname1", 12312)]
        [DataRow(2, "randomname2", 321335)]
        [DataRow(3, "randomname3", 3215325)]
        [TestMethod]
        public async Task ChangeServiceItemProperly(int id, string newName, int newPrice)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var serviceItemService = new ServiceItemService(context);

                //Act
                var sut =await context.ServiceItems.FirstOrDefaultAsync(si => si.Id == id);

                //Assert
                Assert.IsTrue(sut.Name != newName );
                var replica = await serviceItemService.PutAsync(new ServiceItemDto {Id=sut.Id, Name = newName,Price= sut.Price });
                Assert.IsTrue(sut.Name == newName );

                await context.Database.EnsureDeletedAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NegativeIdSelected))]
        public async Task ThrowIfNegativeIdProvided()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var serviceItemService = new ServiceItemService(context);

                //Act & Assert
                await serviceItemService.PutAsync(new ServiceItemDto {Id=-1, Name = "TestItem", Price = (decimal)29.99 });


                await context.Database.EnsureDeletedAsync();
            }
        }

    }
}
