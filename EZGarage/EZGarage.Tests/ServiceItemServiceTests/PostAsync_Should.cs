﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Dto;
using EZGarage.Services.Dto.ServiceItems;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.ServiceItemServiceTests
{
    [TestClass]
    public class PostAsync_Should
    {
        [DataRow("NewRandomTestName")]
        [DataRow("NoSuchName")]
        [TestMethod]
        public async Task PostProperly(string testName)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var serviceItemService = new ServiceItemService(context);

                var dto = new PostServiceItemDto(testName, 25);
                //Act
                await serviceItemService.PostAsync(dto);

                //Assert
                Assert.IsTrue(context.ServiceItems.Any(mo => mo.Name ==testName));

                await context.Database.EnsureDeletedAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NegativePriceException))]
        public async Task ThrowIfNegativePriceProvided()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var serviceItemService = new ServiceItemService(context);

                //Act & Assert
                var dto = new PostServiceItemDto("TestCarName", -1);

                await serviceItemService.PostAsync(dto);

                await context.Database.EnsureDeletedAsync();
            }
        }

        [DataRow("")]
        [DataRow(null)]
        [TestMethod]
        [ExpectedException(typeof(NameNullOrEmpty))]
        public async Task ThrowIfEmptyOrNullStringProvided(string invalidName)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var serviceItemService = new ServiceItemService(context);

                //Act & Assert
                var dto = new PostServiceItemDto(invalidName, 35);
                await serviceItemService.PostAsync(dto);

                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}
