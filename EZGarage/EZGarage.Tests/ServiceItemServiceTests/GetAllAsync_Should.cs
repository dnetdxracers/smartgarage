﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;
namespace EZGarage.Tests.ServiceItemServiceTests
{
    [TestClass]
    public class GetAllAsync_Should
    {
        [TestMethod]
        public async Task GetProperSet()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var serviceItemService = new ServiceItemService(context);

                //Act
                var set = await serviceItemService.GetAllAsync();
                var comparerSet = context.ServiceItems;

                //Assert
                Assert.IsTrue(set.Count() != 0);
                Assert.IsTrue(set.Select(mo => mo.Id).Except(comparerSet.Select(mo => mo.Id)).Count() == 0);

                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}