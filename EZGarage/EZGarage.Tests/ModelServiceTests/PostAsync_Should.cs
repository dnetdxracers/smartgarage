﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.ModelServiceTests
{
    [TestClass]
    public class PostAsync_Should
    {
        [TestMethod]
        public async Task PostProperlyById()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var modelService = new ModelService(context);

                //Act
                await modelService.PostAsync("TestModelName", 1);

                //Assert
                Assert.IsTrue(context.Models.Include(mo=>mo.Manufacturer).Any(mo => mo.Name == "TestModelName" && mo.ManufacturerId == 1));

                await context.Database.EnsureDeletedAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task ThrowIfNoManufacturerWithThisId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var sut = new ModelService(context);

                //Act & Assert
                var model = await sut.PostAsync("TestModelName123", 9999);

                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}
