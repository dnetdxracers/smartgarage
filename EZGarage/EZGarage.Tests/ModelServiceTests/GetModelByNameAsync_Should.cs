﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.ModelServiceTests
{
    [TestClass]
    public class GetModelByNameAsync_Should
    {
        [TestMethod]
        public async Task GetProperModelByName()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var modelService = new ModelService(context);

                //Act
                var sut = await modelService.GetModelByNameAsync("Yaris");
                var comparer = context.Models.Include(mo => mo.Manufacturer).FirstOrDefault(mo => mo.Name.ToLower() == "Yaris".ToLower());

                //Assert
                Assert.AreEqual(sut.Id, comparer.Id);
                Assert.AreEqual(sut.Name, comparer.Name);
                Assert.AreEqual(sut.ManufacturerId, comparer.ManufacturerId);

                await context.Database.EnsureDeletedAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithName))]
        public async Task ThrowIfNoModelWithThisId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var sut = new ModelService(context);

                //Act & Assert
                var model = await sut.GetModelByNameAsync("there cant be a model with this name");

                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}
