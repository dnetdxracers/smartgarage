﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.ModelServiceTests
{
    [TestClass]
    public class PutAsync_Should
    {
        [TestMethod]
        public async Task UpdateAModelProperly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var manufacturerServiceMock = new Mock<ManufacturerService>(context).Object;

                var modelService = new ModelService(context);

                //Act
                var sut = await context.Models.FirstOrDefaultAsync(mo => mo.Id == 2);

                //Assert
                Assert.IsTrue(sut.Name!= "NewTestName");
                var newModel = await modelService.PutAsync(2, "NewTestName");
                Assert.IsTrue(sut.Name== "NewTestName");

                await context.Database.EnsureDeletedAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task ThrowIfNoModelWithThisId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var modelService = new ModelService(context);

                //Act & Assert
                var sut = await modelService.PutAsync(9871, "NewTestName");

                await context.Database.EnsureDeletedAsync();
            }
        }

        [DataRow(null)]
        [DataRow("")]
        [TestMethod]
        [ExpectedException(typeof(NameNullOrEmpty))]
        public async Task ThrowIfInvalidNewName(string invalidName)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var modelService = new ModelService(context);

                //Act & Assert
                var sut = await modelService.PutAsync(2, invalidName);

                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}
