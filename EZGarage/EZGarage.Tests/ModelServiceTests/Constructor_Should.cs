﻿using EZGarage.Data;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace EZGarage.Tests.ModelServiceTests
{
    [TestClass]
    public class Constructor_Should
    {

        [TestMethod]
        public void InstantiateProperly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {

                var sut = new ModelService(context);

                //Act & Assert
                Assert.IsInstanceOfType(sut, typeof(ModelService));
            }
        }
    }
}
