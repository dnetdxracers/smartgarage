﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.ModelServiceTests
{
    [TestClass]
    public class GetModelByIdAsync_Should
    {
        [TestMethod]
        public async Task GetProperModelById()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var modelService = new ModelService(context);

                //Act
                var sut = await modelService.GetModelByIdAsync(1);
                var comparer = context.Models.Include(mo=>mo.Manufacturer).FirstOrDefault(mo => mo.Id == 1);

                //Assert
                Assert.AreEqual(sut.Id, comparer.Id);
                Assert.AreEqual(sut.Name, comparer.Name);
                Assert.AreEqual(sut.ManufacturerId, comparer.ManufacturerId);

                await context.Database.EnsureDeletedAsync();

            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task ThrowIfNoModelWithThisId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var modelService = new ModelService(context);

                //Act & Assert
                var sut = await modelService.GetModelByIdAsync(9999);

                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}
