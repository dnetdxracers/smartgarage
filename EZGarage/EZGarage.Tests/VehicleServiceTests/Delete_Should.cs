﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.VehicleServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public async Task Execute_Correctly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicles = context.Vehicles.Count();

                var vehicleService = new VehicleService(context);

                await vehicleService.DeleteAsync(1);

                var vehicle = context.Vehicles.FirstOrDefault(v => v.Id == 1);

                // Assert
                Assert.IsTrue(vehicle.IsDeleted == true);
                await context.SaveChangesAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task Throw_NonExistentItemWithId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

              
                var vehicleService = new VehicleService(context);

                await vehicleService.DeleteAsync(100);
                await context.SaveChangesAsync();
            }
        }
    }
}
