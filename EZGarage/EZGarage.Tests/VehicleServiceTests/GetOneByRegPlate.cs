﻿using EZGarage.Data;
using EZGarage.Data.Models;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace EZGarage.Tests.VehicleServiceTests
{
    [TestClass]
    public class GetOneByRegPlate
    {
        [TestMethod]
        public async Task ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                var vehicleService = new VehicleService(context);

                // Act
                var vehicle = new Vehicle()
                {
                    Id = 99,
                    CustomerId = 1,
                    ModelId = 2,
                    VehicleTypeId = 1,
                    RegistrationPlate = "PK1234AB",
                    VIN = "1G6AH5SX6F0107988",
                    IsDeleted = false,
                    Year = 1994
                };

                await context.Vehicles.AddAsync(vehicle);
                await context.SaveChangesAsync();

                var sut = await vehicleService.GetOneByRegPlateAsync("PK1234AB");

                // Assert
                Assert.IsTrue(sut.Id == vehicle.Id);
                Assert.IsTrue(sut.RegistrationPlate.ToLower() == vehicle.RegistrationPlate.ToLower());

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithProperty))]
        public async Task Throw_NonExistentProperty()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                var vehicleService = new VehicleService(context);

                // Act & Assert
                var sut = await vehicleService.GetOneByRegPlateAsync("Y1000YY");
                context.Database.EnsureDeleted();
            }

        }
    }
}
