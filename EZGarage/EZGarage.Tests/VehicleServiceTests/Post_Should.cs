﻿using EZGarage.Data;
using EZGarage.Data.Models;
using EZGarage.Exceptions;
using EZGarage.Services.Dto;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.VehicleServiceTests
{
    [TestClass]
    public class Post_Should
    {
        [TestMethod]
        public async Task ExecuteCorrectly_RegPlate1Letter()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);

                var vehicle = new Vehicle
                {
                    CustomerId = 1,
                    VehicleTypeId = 2,
                    ModelId = 2,
                    Year = 2015,
                    RegistrationPlate = "C7766BP",
                    VIN = "19VDE1F3XEE414842"
                };
                var postDto = new PostVehicleDto(vehicle.CustomerId,
                    vehicle.VehicleTypeId,
                    vehicle.ModelId,
                    vehicle.Year,
                    vehicle.RegistrationPlate,
                    vehicle.VIN);

                // Act
                await vehicleService.PostAsync(postDto);
                await context.SaveChangesAsync();

                // Assert
                Assert.AreEqual(4, context.Vehicles.Count());
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_RegPlate2Letters()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);

                var vehicle = new Vehicle
                {
                    CustomerId = 1,
                    VehicleTypeId = 2,
                    ModelId = 2,
                    Year = 2015,
                    RegistrationPlate = "CB7766BP",
                    VIN = "19VDE1F3XEE414842"
                };
                var postDto = new PostVehicleDto(vehicle.CustomerId,
                    vehicle.VehicleTypeId,
                    vehicle.ModelId,
                    vehicle.Year,
                    vehicle.RegistrationPlate,
                    vehicle.VIN);

                // Act
                await vehicleService.PostAsync(postDto);
                await context.SaveChangesAsync();

                // Assert
                Assert.AreEqual(4, context.Vehicles.Count());
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task Throw_NonExistentCustomerId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);

                var vehicle = new Vehicle
                {
                    CustomerId = 100,
                    VehicleTypeId = 2,
                    ModelId = 2,
                    Year = 2015,
                    RegistrationPlate = "CB7766BP",
                    VIN = "19VDE1F3XEE414842"
                };
                var postDto = new PostVehicleDto(vehicle.CustomerId,
                    vehicle.VehicleTypeId,
                    vehicle.ModelId,
                    vehicle.Year,
                    vehicle.RegistrationPlate,
                    vehicle.VIN);

                // Act
                await vehicleService.PostAsync(postDto);
                await context.SaveChangesAsync();

            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task Throw_NonExistentVehicleTypeId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);

                var vehicle = new Vehicle
                {
                    CustomerId = 1,
                    VehicleTypeId = 100,
                    ModelId = 2,
                    Year = 2015,
                    RegistrationPlate = "CB7766BP",
                    VIN = "19VDE1F3XEE414842"
                };
                var postDto = new PostVehicleDto(vehicle.CustomerId,
                    vehicle.VehicleTypeId,
                    vehicle.ModelId,
                    vehicle.Year,
                    vehicle.RegistrationPlate,
                    vehicle.VIN);

                // Act
                await vehicleService.PostAsync(postDto);
                await context.SaveChangesAsync();

            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task Throw_NonExistentModelId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);

                var vehicle = new Vehicle
                {
                    CustomerId = 1,
                    VehicleTypeId = 2,
                    ModelId = 100,
                    Year = 2015,
                    RegistrationPlate = "CB7766BP",
                    VIN = "19VDE1F3XEE414842"
                };
                var postDto = new PostVehicleDto(vehicle.CustomerId,
                    vehicle.VehicleTypeId,
                    vehicle.ModelId,
                    vehicle.Year,
                    vehicle.RegistrationPlate,
                    vehicle.VIN);

                // Act
                await vehicleService.PostAsync(postDto);
                await context.SaveChangesAsync();

            }
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidYear))]
        public async Task Throw_InvalidYear()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);

                var vehicle = new Vehicle
                {
                    CustomerId = 1,
                    VehicleTypeId = 2,
                    ModelId = 2,
                    Year = 3045,
                    RegistrationPlate = "CB7766BP",
                    VIN = "19VDE1F3XEE414842"
                };
                var postDto = new PostVehicleDto(vehicle.CustomerId,
                    vehicle.VehicleTypeId,
                    vehicle.ModelId,
                    vehicle.Year,
                    vehicle.RegistrationPlate,
                    vehicle.VIN);

                // Act
                await vehicleService.PostAsync(postDto);
                await context.SaveChangesAsync();

            }
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidRegPlate))]
        public async Task Throw_InvalidRegPlate()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);

                var vehicle = new Vehicle
                {
                    CustomerId = 1,
                    VehicleTypeId = 2,
                    ModelId = 2,
                    Year = 2015,
                    RegistrationPlate = "invalidreg",
                    VIN = "19VDE1F3XEE414842"
                };
                var postDto = new PostVehicleDto(vehicle.CustomerId,
                    vehicle.VehicleTypeId,
                    vehicle.ModelId,
                    vehicle.Year,
                    vehicle.RegistrationPlate,
                    vehicle.VIN);

                // Act
                await vehicleService.PostAsync(postDto);
                await context.SaveChangesAsync();

            }
        }
    }
}
