﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.VehicleServiceTests
{
    [TestClass]
    public class Put_Should
    {
        [TestMethod]
        public async Task Execute_Correctly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);


                // Act
                await vehicleService.PutAsync(1, "CA1123AB");
                await context.SaveChangesAsync();

                // Assert
                Assert.AreEqual("CA1123AB", context.Vehicles.First().RegistrationPlate);

            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task Throw_NonExistentItemId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);

                // Act & Assert
                await vehicleService.PutAsync(100, "CA1123AB");
                await context.SaveChangesAsync();
            }
        }
    }
}
