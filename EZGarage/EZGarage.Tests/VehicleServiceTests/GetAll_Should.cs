﻿using EZGarage.Data;
using EZGarage.Services.Services;
using EZGarage.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.VehicleServiceTests
{
    [TestClass]
    public class GetAll_Should
    {
        [TestMethod]
        public async Task ExecuteCorrectly_CustomerId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);


                var sut = await vehicleService.GetAllAsync(3, null, null, null, null, null, null, null, null, null);

                // Assert
                Assert.AreEqual(2, sut.Count());

                await context.Database.EnsureDeletedAsync();
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_CustomerName()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);


                var sut = await vehicleService.GetAllAsync(null, "skywalk", null, null, null, null, null, null, null, null);

                // Assert
                Assert.AreEqual(2, sut.Count());

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_VehicleTypeId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);


                var sut = await vehicleService.GetAllAsync(null, null, 1, null, null, null, null, null, null, null);
                var sut2 = await vehicleService.GetAllAsync(null, null, 2, null, null, null, null, null, null, null);

                // Assert
                Assert.AreEqual(2, sut.Count());
                Assert.AreEqual(1, sut2.Count());

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_VehicleTypeName()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);


                var sut = await vehicleService.GetAllAsync(null, null, null, "car", null, null, null, null, null, null);
                var sut2 = await vehicleService.GetAllAsync(null, null, null, "bike", null, null, null, null, null, null);

                // Assert
                Assert.AreEqual(2, sut.Count());
                Assert.AreEqual(1, sut2.Count());

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_ModelId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);


                var sut = await vehicleService.GetAllAsync(null, null, null, null, 1, null, null, null, null, null);
                var sut2 = await vehicleService.GetAllAsync(null, null, null, null, 2, null, null, null, null, null);

                // Assert
                Assert.AreEqual(1, sut.Count());
                Assert.AreEqual(1, sut2.Count());

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_ModelName()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);


                var sut = await vehicleService.GetAllAsync(null, null, null, null, null, "yaris", null, null, null, null);
                var sut2 = await vehicleService.GetAllAsync(null, null, null, null, null, "150", null, null, null, null);

                // Assert
                Assert.AreEqual(1, sut.Count());
                Assert.AreEqual(1, sut2.Count());

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_ManufacturerId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);


                var sut = await vehicleService.GetAllAsync(null, null, null, null, null, null, 1, null, null, null);
                var sut2 = await vehicleService.GetAllAsync(null, null, null, null, null, null, 3, null, null, null);

                // Assert
                Assert.AreEqual(1, sut.Count());
                Assert.AreEqual(1, sut2.Count());

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_ManufacturerName()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);


                var sut = await vehicleService.GetAllAsync(null, null, null, null, null, null, null, "lambo", null, null);
                var sut2 = await vehicleService.GetAllAsync(null, null, null, null, null, null, null, "yamAHa", null, null);

                // Assert
                Assert.AreEqual(1, sut.Count());
                Assert.AreEqual(1, sut2.Count());

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_Year()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);


                var sut = await vehicleService.GetAllAsync(null, null, null, null, null, null, null, null, 2014, null);

                // Assert
                Assert.AreEqual(1, sut.Count());

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ExecuteCorrectly_VIN()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var vehicleService = new VehicleService(context);


                var sut = await vehicleService.GetAllAsync(null, null, null, null, null, null, null, null, null, "00");
                var sut2 = await vehicleService.GetAllAsync(null, null, null, null, null, null, null, null, null, "VF7LAKFUC74241622");

                // Assert
                Assert.AreEqual(1, sut.Count());
                Assert.AreEqual(1, sut2.Count());

                context.Database.EnsureDeleted();
            }
        }
    }
}
