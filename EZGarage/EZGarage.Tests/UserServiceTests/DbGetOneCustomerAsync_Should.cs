﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.UserServiceTests
{
    [TestClass]
    public class DbGetOneCustomerAsync_Should
    {
        [TestMethod]
        public async Task GetProperCustomer()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();
                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                //Act
                var customer =await context.Users.Where(us => us.RoleId == 2).FirstOrDefaultAsync(cu=>cu.Id==4);
                var sut = await userService.DbGetOneCustomerAsync(4);

                //Assert
                Assert.AreEqual(sut.Id,customer.Id);
                Assert.AreEqual(sut.FirstName,customer.FirstName);
                Assert.AreEqual(sut.LastName,customer.LastName);
                Assert.AreEqual(sut.Email,customer.Email);
                Assert.AreEqual(sut.Phone,customer.Phone);

                await context.Database.EnsureDeletedAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task Throw_NonExistentId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                //Act
                var nonCustomerId = 150;
                var sut = await userService.DbGetOneCustomerAsync(nonCustomerId);

                //Assert
                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}
