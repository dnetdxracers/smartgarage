﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.UserServiceTests
{
    [TestClass]
    public class PutUserAsync_Should
    {
        [TestMethod]
        public async Task ProperlyChangeBasedOnProvidedParams()
        {

            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();

                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                //Act
                var sut = context.Users.FirstOrDefault(us => us.Id == 4 && us.RoleId == 2);

                //Assert
                Assert.IsFalse(sut.FirstName == "Johny");
                Assert.IsFalse(sut.LastName == "Bravo");
                Assert.IsFalse(sut.Phone == "0999999900");
                Assert.IsFalse(sut.Email == "johny@gmail.com");
                await userService.PutUserAsync(4, "Johny", "Bravo", "0999999900", "johny@gmail.com");
                Assert.IsTrue(sut.FirstName == "Johny");
                Assert.IsTrue(sut.LastName == "Bravo");
                Assert.IsTrue(sut.Phone == "0999999900");
                Assert.IsTrue(sut.Email == "johny@gmail.com");

                await context.Database.EnsureDeletedAsync();
            }

        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task ThrowIfNoSuchUserIdExists()
        {

            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();

                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                //Act & Assert
                await userService.PutUserAsync(999999, "Johny", "Bravo", "0999999900", "johny@gmail.com");

                await context.Database.EnsureDeletedAsync();
            }

        }
    }
}
