﻿using EZGarage.Data;
using EZGarage.Services.Contracts;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.UserServiceTests
{
    [TestClass]
    public class SortCustomerAsync_Should
    {
        [TestMethod]
        [DataRow("firstname", "asc")]
        [DataRow("firstname", null)]
        [DataRow("firstname", "desc")]
        [DataRow("lastname", "asc")]
        [DataRow("lastname", null)]
        [DataRow("lastname", "desc")]
        //[DataRow("visitdate", "asc")]
        //[DataRow("visitdate", "null")]
        //[DataRow("visitdate", "desc")]
        [DataRow(null, null)]
        public async Task SortCustomersInDesiredOrder(string testCriteria, string testOrder)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();

                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                //Act
                var sut = await userService.SortCustomersAsync(testCriteria, testOrder);

                var legitimateCustomers = context.Users
                    .Include(us=>us.Visits)
                    .AsQueryable();

                string legitimateReport;

                var visits = legitimateCustomers.SelectMany(cu => cu.Visits).ToList();
                testCriteria = testCriteria ?? "";
                testOrder = testOrder ?? "";

                (legitimateReport, legitimateCustomers) = (testCriteria.ToLower(), testOrder.ToLower()) switch
                {
                    ("firstname", "asc") => ("First Name Asc", legitimateCustomers.OrderBy(cu => cu.FirstName)),
                    ("firstname", null) => ("First Name Asc (default)", legitimateCustomers.OrderBy(cu => cu.FirstName)),
                    ("firstname", "desc") => ("First Name Desc", legitimateCustomers.OrderByDescending(cu => cu.FirstName)),

                    ("lastname", "asc") => ("Last Name Asc ", legitimateCustomers.OrderBy(cu => cu.LastName)),
                    ("lastname", null) => ("Last Name Asc (default)", legitimateCustomers.OrderBy(cu => cu.LastName)),
                    ("lastname", "desc") => ("Last Name Desc", legitimateCustomers.OrderByDescending(cu => cu.LastName)),

                    ("visitdate", "asc") => ("Visit Date Asc", legitimateCustomers.OrderBy(cu => cu.Visits.Select(vi => vi.ArrivalDate).Max())),
                    ("visitdate", null) => ("Visit Date Asc (default)", legitimateCustomers.OrderBy(cu => cu.Visits.Select(vi => vi.ArrivalDate).Max())),
                    ("visitdate", "desc") => ("Visit Date Desc", legitimateCustomers.OrderByDescending(cu => cu.Visits.Select(vi => vi.ArrivalDate).Max())),

                    _ => ("Id Asc", legitimateCustomers.OrderBy(cu => cu.Id))
                };
                
                //Assert
                Assert.IsTrue(sut.Item2.Select(cu=>cu.Id).SequenceEqual((legitimateReport, legitimateCustomers).legitimateCustomers.Select(cu=>cu.Id)));


                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}
