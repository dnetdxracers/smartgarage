﻿using EZGarage.Data;
using EZGarage.Services.Contracts;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;


namespace EZGarage.Tests.UserServiceTests
{
    [TestClass]
    public class GetAllCustomersAsync_Should
    {
        [TestMethod]
        public async Task ProperlyGetAllCustomers()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();

                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                //Act
                var sut = await userService.GetAllCustomersAsync();
                var legittimates = context.Users;
                //Assert:


                Assert.IsTrue(sut.Count() == legittimates.Count());
                Assert.IsTrue(sut.Select(us => us.Id).Except(legittimates.Select(us => us.Id)).Count() == 0);

                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}
