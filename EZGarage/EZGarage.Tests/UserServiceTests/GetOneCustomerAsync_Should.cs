﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.UserServiceTests
{
    [TestClass]
    public class GetOneCustomerAsync_Should
    {
        [TestMethod]
        public void GetProperlyMappedDto()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                //Act
                var sut = userService.GetOneCustomerAsync(3).Result;

                var legitCustomer = context.Users.Where(cu => !cu.IsDeleted)
                .Include(cu => cu.Role)
                .Include(cu => cu.Vehicles)
                    .ThenInclude(ve => ve.Model)
                        .ThenInclude(mo => mo.Manufacturer)
                .Include(cu => cu.Vehicles)
                    .ThenInclude(ve => ve.VehicleType)
                .FirstOrDefault(cu => cu.Id == 3);
                var dto = new CustomerDtoDetailed(legitCustomer);


                //Assert
                Assert.IsTrue(sut.Id == dto.Id);
                Assert.IsTrue(sut.FirstName == dto.FirstName);
                Assert.IsTrue(sut.Phone == dto.Phone);
                Assert.IsTrue(sut.Vehicles.Intersect(dto.Vehicles).Count() == 0);
                Assert.IsTrue(sut.Email == dto.Email);

                context.Database.EnsureDeleted();
            }
        }

       

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task ThrowIfNonExistantUserIdIsProvided()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();

                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                //Act & Assert
                var sut = await userService.GetOneCustomerAsync(99999999);

                await context.Database.EnsureDeletedAsync();
            }
        }



    }
}

