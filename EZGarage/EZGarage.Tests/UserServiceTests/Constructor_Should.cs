﻿using EZGarage.Data;
using EZGarage.Services.Contracts;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace EZGarage.Tests.UserServiceTests
{
    [TestClass]
    public class Constructor_Should
    {

        [TestMethod]
        public void InstantiateProperly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                var appSettingsMock = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var sut = new UserService(context, appSettingsMock, mailServiceMock);

                //Act & Assert
                Assert.IsInstanceOfType(sut, typeof(UserService));
            }
        }
    }
}
