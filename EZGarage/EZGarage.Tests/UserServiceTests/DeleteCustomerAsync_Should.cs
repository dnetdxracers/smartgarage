﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.UserServiceTests
{
    [TestClass]
    public class DeleteCustomerAsync_Should
    {
        [TestMethod]
        public async Task DeleteProperly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();

                Util.SeedDatabase(context);
                await context .SaveChangesAsync();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                //Act & Assert
                var customer = context.Users.Where(us => us.RoleId == 2).FirstOrDefault();
                var customerId = customer.Id;

                Assert.IsFalse(customer.IsDeleted);
                await userService.DeleteCustomerAsync(customerId);
                Assert.IsTrue(customer.IsDeleted);

                await context.Database.EnsureDeletedAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task ThrowIfEmployeeIdIsProvided()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();

                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                var employee = context.Users.Where(us => us.RoleId == 1).FirstOrDefault();
                var employeeId = employee.Id;

                //Act & Assert
                await userService.DeleteCustomerAsync(employeeId);

                await context.Database.EnsureDeletedAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistentItemWithId))]
        public async Task ThrowIfNoUserWithSuchId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();

                Util.SeedDatabase(context);
                await context .SaveChangesAsync();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                //Act & Assert
                await userService.DeleteCustomerAsync(99999);

                await context.Database.EnsureDeletedAsync();
            }
        }

        [TestMethod]
        public async Task GiveProperResponse()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();

                Util.SeedDatabase(context);
                await context .SaveChangesAsync();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                var customer = context.Users.Where(us => us.RoleId == 2).FirstOrDefault();
                var customerId = customer.Id;

                //Act & Assert
                var response = await userService.DeleteCustomerAsync(customerId);
                Assert.AreEqual(response, $"Customer with Id {customerId}: {customer.FirstName} {customer.LastName} deleted");

                await context.Database.EnsureDeletedAsync();
            }
        }



    }
}
