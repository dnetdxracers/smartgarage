﻿using EZGarage.Data;
using EZGarage.Services.Contracts;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.UserServiceTests
{
    [TestClass]
    public class DbGetOneUserAsync_Should
    {
        [TestMethod]
        public async Task GetProperUser()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();

                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                //Act
                var user = context.Users.FirstOrDefault(cu => cu.Id == 3);
                var sut = await userService.DbGetOneUserAsync(3);

                //Assert
                Assert.AreEqual(sut.Id, user.Id);
                Assert.AreEqual(sut.FirstName, user.FirstName);
                Assert.AreEqual(sut.LastName, user.LastName);
                Assert.AreEqual(sut.Email, user.Email);
                Assert.AreEqual(sut.Phone, user.Phone);

                await context.Database.EnsureDeletedAsync();
            }
        }

        [TestMethod]
        public async Task ReturnNullIfInvalidId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();

                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                //Act
                var sut = await userService.DbGetOneUserAsync(999999);

                //Assert
                Assert.AreEqual(sut, null);

                await context.Database.EnsureDeletedAsync();
            }
        }
    }
}
