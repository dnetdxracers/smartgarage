﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.UserServiceTests
{
    
    [TestClass]
    public class GetManyCustomersAsync_Should
    {
        [TestMethod]
        public async Task ProperlyFindEntry()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();

                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                //Act
                var comparison = context.Users
                    .Include(cu=> cu.Vehicles)
                        .ThenInclude(ve=>ve.Model)
                        .ThenInclude(mo=>mo.Manufacturer)
                    .Include(cu => cu.Visits)
                    .FirstOrDefault(us => us.Id == 3);
                var compVehicle = comparison.Vehicles.FirstOrDefault();

                var sut = (await userService.GetManyCustomersAsync(comparison.FirstName, 
                    comparison.Phone, 
                    comparison.Email, 
                    compVehicle.Id, 
                    compVehicle.Model.Name, 
                    compVehicle.Model.Manufacturer.Name, 
                    DateTime.Now.AddDays(10).ToString("dd-MMM-yyyy"), 
                    DateTime.Now.AddDays(-10).ToString("dd-MMM-yyyy")))
                    .FirstOrDefault();

                //Assert
                Assert.IsTrue(sut.FirstName==comparison.FirstName);
                Assert.IsTrue(sut.LastName==comparison.LastName);
                Assert.IsTrue(sut.Id==comparison.Id);
                Assert.IsTrue(sut.Phone==comparison.Phone);
              
                Assert.IsTrue(sut.Email==comparison.Email);

                await context.Database.EnsureDeletedAsync();
            }


        }
    }
}
