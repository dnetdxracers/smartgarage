﻿using EZGarage.Data;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using EZGarage.Services.Services;
using EZGarage.Services.Util.Helpers;
using EZGarage.Tests.Utils;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Tests.UserServiceTests
{
    [TestClass]
    public class PostUserAsync_Should
    {
        [TestMethod]
        public async Task ProperlyUpdateTheDatabaseWithTheNewUser()
        {

            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<EZGarageContext>();

            using (var context = new EZGarageContext(options))
            {
                await context.Database.EnsureDeletedAsync();

                Util.SeedDatabase(context);
                await context.SaveChangesAsync();

                var appSettings = new Mock<IOptions<AppSettings>>().Object;
                var mailServiceMock = new Mock<IMailService>().Object;

                var userService = new UserService(context, appSettings, mailServiceMock);

                //Act
                var guid = new Guid().ToString().Substring(0,15);
                var data = new PostUserDtoRequest(guid, "Silver", "0889191919", "jack@ma.bg", 2);

                var sut =await userService.PostUserAsync(data);

                Assert.IsTrue(context.Users.Any(us => us.FirstName == guid && us.FirstName==data.FirstName));
                Assert.IsTrue(context.Users.Any(us => us.LastName == data.LastName));
                Assert.IsTrue(context.Users.Any(us => us.Phone == data.Phone));
                Assert.IsTrue(context.Users.Any(us => us.Email == data.Email));

                await context.Database.EnsureDeletedAsync();
            }

        }
    }
}
