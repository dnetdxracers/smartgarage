﻿using EZGarage.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.VehicleViewModels
{
    public class VehicleViewModel
    {
        /// <summary>
        /// Empty ctor
        /// </summary>
        public VehicleViewModel()
        {

        }
        /// <summary>
        /// Ctor with dto injection
        /// </summary>
        /// <param name="vehicleDto"></param>
        public VehicleViewModel(VehicleDto vehicleDto)
        {
            this.Id = vehicleDto.Id;
            this.CustomerId = vehicleDto.CustomerId;
            this.CustomerName = vehicleDto.CustomerName;
            this.VehicleTypeId = vehicleDto.VehicleTypeId;
            this.VehicleType = vehicleDto.VehicleType;
            this.ManufacturerId = vehicleDto.ManufacturerId;
            this.Manufacturer = vehicleDto.Manufacturer;
            this.ModelId = vehicleDto.ModelId;
            this.Model = vehicleDto.Model;
            this.Year = vehicleDto.Year;
            this.RegistrationPlate = vehicleDto.RegistrationPlate;
            this.VIN = vehicleDto.VIN;
        }

        /// <summary>
        /// Vehicle Id prop
        /// </summary>
        [Required(ErrorMessage = "Vehicle Id is required")]
        public int Id { get; set; }

        /// <summary>
        /// Customer Id prop
        /// </summary>
        [Required(ErrorMessage = "Customer Id is required")]
        public int CustomerId { get; set; }

        /// <summary>
        /// Customer names prop
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Vehicle type Id prop
        /// </summary>
        public int VehicleTypeId { get; set; }

        /// <summary>
        /// Vehicle type prop
        /// </summary>
        public string VehicleType { get; set; }

        /// <summary>
        /// Manufacturer Id prop
        /// </summary>
        public int ManufacturerId { get; set; }

        /// <summary>
        /// Manufacturer prop
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// Manufacturers prop for drop down list
        /// </summary>
        public IEnumerable<SelectListItem> Manufacturers { get; set; }

        /// <summary>
        /// Model Id prop
        /// </summary>
        public int ModelId { get; set; }

        /// <summary>
        /// Model prop
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Models prop for drop down
        /// </summary>
        public IEnumerable<SelectListItem> Models { get; set; }

        /// <summary>
        /// Year prop
        /// </summary>
        [Required(ErrorMessage = "Year is required")]
        public int Year { get; set; }

        /// <summary>
        /// Registration plate prop
        /// </summary>
        [Required(ErrorMessage = "Registration plate is required")]
        [StringLength(8, MinimumLength = 7, ErrorMessage = "Please enter a valid registraion plate, i.e. A(A)1234AA")]
        public string RegistrationPlate { get; set; }

        /// <summary>
        /// VIN prop
        /// </summary>
        [Required(ErrorMessage = "VIN is required")]
        [StringLength(17, MinimumLength = 17, ErrorMessage ="Please enter a valid VIN")]
        public string VIN { get; set; }
    }
}
