﻿using EZGarage.Web.Models.ServiceItemModels;
using EZGarage.Web.Models.VisitViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.MngVisitsModels
{
    public class VisitAndServiceItemsVM
    {
        public VisitAndServiceItemsVM()
        {
        }

        public VisitAndServiceItemsVM(VisitDetailedViewModel visitVM, IEnumerable<ServiceItemViewModel> serviceItems)
        {
            this.Visit = visitVM;
            this.ServiceItems = serviceItems;
        }
        public VisitDetailedViewModel Visit { get; set; }
        public IEnumerable<ServiceItemViewModel> ServiceItems { get; set; }

        public string Context { get; set; }
        public string Message { get; set; }
    }

}
