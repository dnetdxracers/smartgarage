﻿using EZGarage.Web.Models.ServiceItemModels;
using EZGarage.Web.Models.UserViewModels;
using EZGarage.Web.Models.VehicleViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.MngVisitsModels
{
    public class CreateNewVisitFormVM
    {

        public CreateNewVisitFormVM(IEnumerable<VehicleViewModel> vehicles, IEnumerable<ServiceItemViewModel> serviceItems)
        {
            this.Vehicles = vehicles;
            this.ServiceItems = serviceItems;
        }
        public IEnumerable<VehicleViewModel> Vehicles { get; set; }
        public IEnumerable<ServiceItemViewModel> ServiceItems { get; set; }

    }
}
