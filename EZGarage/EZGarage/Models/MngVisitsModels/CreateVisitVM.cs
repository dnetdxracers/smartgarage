﻿using EZGarage.Web.Models.VisitViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.MngVisitsModels
{
    public class CreateVisitVM
    {
        public CreateVisitVM(VisitDetailedViewModel visitVM, string context, string message)
        {
            this.Visit = visitVM;
            this.Context = context;
            this.Message = message;
        }
        public VisitDetailedViewModel Visit { get; set; }
        public string Context { get; set; }
        public string Message { get; set; }
    }
}
