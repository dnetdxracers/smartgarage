﻿namespace EZGarage.Web.Models.ServiceItemModels
{
    public class ServiceItemInputViewModel
    {
        /// <summary>
        /// Service item Id property
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Service item name property
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Service item price property
        /// </summary>
        public decimal Price { get; set; }
    }
}
