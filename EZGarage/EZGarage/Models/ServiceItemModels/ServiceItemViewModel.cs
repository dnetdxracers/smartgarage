﻿using EZGarage.Services.Dto.ServiceItems;

namespace EZGarage.Web.Models.ServiceItemModels
{
    public class ServiceItemViewModel
    {
        /// <summary>
        /// Empty ctor
        /// </summary>
        public ServiceItemViewModel()
        {

        }
        /// <summary>
        /// Ctor with dto injection
        /// </summary>
        /// <param name="serviceItem">Service item</param>
        public ServiceItemViewModel(ServiceItemDto serviceItem)
        {
            this.Id = serviceItem.Id;
            this.Name = serviceItem.Name.Substring(0, 1).ToUpper() 
                + serviceItem.Name[1..];
            this.Price = serviceItem.Price;
        }

        /// <summary>
        /// Id property
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name property
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Price property
        /// </summary>
        public decimal? Price { get; set; }

        public bool IsSuccessful { get; set; }
    }
}
