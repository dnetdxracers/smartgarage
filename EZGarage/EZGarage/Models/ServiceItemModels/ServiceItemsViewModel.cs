﻿using EZGarage.Services.Dto.ServiceItems;
using System.Collections.Generic;
using System.Linq;

namespace EZGarage.Web.Models.ServiceItemModels
{
    public class ServiceItemsViewModel
    {
        public ServiceItemsViewModel(IEnumerable<ServiceItemDto> services)
        {
            this.Services = services.Select(s => new ServiceItemViewModel(s));
        }
        public IEnumerable<ServiceItemViewModel> Services { get; set; } = new List<ServiceItemViewModel>();
    }
}
