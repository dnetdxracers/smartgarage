﻿using EZGarage.Services.Dto.Services;
using EZGarage.Services.Dto.Visits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.MyServicesModels
{
    public class ServiceViewModel
    {
        public ServiceViewModel()
        {

        }
        public ServiceViewModel(ServiceDto serviceDto)
        {
            this.Id = serviceDto.Id;

            this.CustomerId = serviceDto.CustomerId;
            this.Customer = serviceDto.Customer;
            this.VisitId = serviceDto.VisitId;
            this.VehicleMakeModel = serviceDto.VehicleMakeModel;
            this.ServiceItem = serviceDto.ServiceItem;
            this.Status = serviceDto.Status;
            this.VisitArrivalDate = serviceDto.VisitArrivalDate;
            this.ReadyDate = serviceDto.ReadyDate;
            this.Price = serviceDto.Price;
            this.Currency = serviceDto.Currency;

            //the days passed from start to finish in % as of yet:
            var inProgressCase = (int)Math.Round((DateTime.Now.Date - Convert.ToDateTime(this.VisitArrivalDate).Date)
                                / (Convert.ToDateTime(this.ReadyDate).Date - Convert.ToDateTime(this.VisitArrivalDate).Date) * 100);
            if (inProgressCase > 100)
                inProgressCase = 100;
            else if (inProgressCase < 0)
                inProgressCase = 0;

            this.ProgressBarPercent = serviceDto.Status.ToLower() switch
            {
                "not started" => 0,
                "in progress" => inProgressCase,
                "ready" => 100,
                _ => 0
            };

        }

        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Customer { get; set; }
        public int VisitId { get; set; }
        public string VisitArrivalDate { get; set; }
        public string VehicleMakeModel { get; set; }
        public string ServiceItem { get; set; }
        public string Status { get; set; }
        public int ProgressBarPercent { get; set; }
        public string ReadyDate { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }

        public string Action { get; set; }
        public string Context { get; set; }
    }
}
