﻿using EZGarage.Services.Dto;
using EZGarage.Services.Dto.Services;
using EZGarage.Web.Models.UserViewModels;
using System.Collections.Generic;
using System.Linq;

namespace EZGarage.Web.Models.MyServicesModels
{
    public class MyServicesIndexViewModel
    {
        public MyServicesIndexViewModel(CustomerViewModel customerVM, IEnumerable<ServiceViewModel> serviceVMs)
        {
            this.Customer = customerVM;
            this.Services = serviceVMs;
        }
        public CustomerViewModel Customer { get; set; }
        public IEnumerable<ServiceViewModel> Services { get; set; }
    }
}
