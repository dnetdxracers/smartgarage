﻿using EZGarage.Web.Models.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.MngCustomersModels
{
    public class MngCustomersIndexViewModel
    {
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="userViewModel"></param>
        /// <param name="usersViewModels"></param>
        public MngCustomersIndexViewModel(CustomerViewModel userViewModel, IEnumerable<CustomerViewModel> usersViewModels)
        {
            UserViewModel = userViewModel;
            UsersViewModels = usersViewModels;
        }

        /// <summary>
        /// Props
        /// </summary>
        public CustomerViewModel UserViewModel { get; set; }
        public IEnumerable<CustomerViewModel> UsersViewModels { get; set; }
    }
}
