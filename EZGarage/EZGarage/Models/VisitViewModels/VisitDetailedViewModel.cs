﻿using EZGarage.Services.Dto.Visits;
using EZGarage.Web.Models.MyServicesModels;
using EZGarage.Web.Models.VehicleViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.VisitViewModels
{
    public class VisitDetailedViewModel
    {
        //used by ASP.NET
        public VisitDetailedViewModel()
        {

        }
        public VisitDetailedViewModel(VisitDtoDetailed visitDto)
        {
            this.Id = visitDto.Id;
            this.CustomerId = visitDto.CustomerId;
            this.CustomerName = visitDto.CustomerName;
            this.ArrivalDate = visitDto.ArrivalDate;
            this.VehicleId = visitDto.VehicleId;
            this.VehicleMakeModel = visitDto.VehicleMakeModel;
            this.RegistrationPlate = visitDto.RegistrationPlate;

            //check if this crashes
            this.Services = visitDto.Services.Select(se => new ServiceViewModel(se)).ToList() ;
            
            this.ReadyDate = visitDto.ReadyDate;
            this.TotalPrice = visitDto.TotalPrice;
            this.Currency = visitDto.Currency;
        }

        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string ArrivalDate { get; set; }
        public int VehicleId { get; set; }
        public string VehicleMakeModel { get; set; }
        public string RegistrationPlate { get; set; }
        public IEnumerable<ServiceViewModel> Services { get; set; }
        public string ReadyDate { get; set; }
        public decimal TotalPrice { get; set; }
        public string Currency { get; set; }
        public string ReportCurrency { get; set; }
        public string ReportCurrencyPrice { get; set; }

        public string Action { get; set; }
        public string Context { get; set; }
    }
}
