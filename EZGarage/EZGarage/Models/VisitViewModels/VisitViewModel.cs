﻿using EZGarage.Services.Dto.Visits;
using EZGarage.Services.Dto.Services;
using EZGarage.Web.Models.MyServicesModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.VisitViewModels
{
    public class VisitViewModel
    {
        public VisitViewModel(VisitDto visitDto)
        {
            this.Id = visitDto.Id;
            this.CustomerId = visitDto.CustomerId;
            this.CustomerName = null;
            this.ArrivalDate = visitDto.ArrivalDate;
            this.VehicleId = visitDto.VehicleId;
            this.VehicleMakeModel = null;
            this.DateAllDone = visitDto.DateAllDone;
            this.RegistrationPlate = null;
            this.Services = visitDto.Services.Select(se=>new ServiceViewModel(se)).ToList();
            this.Currency = visitDto.Currency;
            this.Price = visitDto.Price;
        }

        public VisitViewModel(VisitDtoDetailed visitDtoDetailed)
        {
            this.Id = visitDtoDetailed.Id;
            this.CustomerId = visitDtoDetailed.CustomerId;
            this.CustomerName = visitDtoDetailed.CustomerName;
            this.ArrivalDate = visitDtoDetailed.ArrivalDate;
            this.VehicleId = visitDtoDetailed.VehicleId;
            this.VehicleMakeModel = visitDtoDetailed.VehicleMakeModel;
            this.RegistrationPlate = visitDtoDetailed.RegistrationPlate;
            this.DateAllDone = visitDtoDetailed.ReadyDate;
            this.Services = visitDtoDetailed.Services.Select(se => new ServiceViewModel(se)).ToList();
            this.Currency = visitDtoDetailed.Currency;
            this.Price = visitDtoDetailed.TotalPrice;
        }

        /// <summary>
        /// Visit Id property
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Customer Id property
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Customer name property
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Visit arrival date proeprty
        /// </summary>
        public string ArrivalDate { get; set; }

        /// <summary>
        /// Vehicle Id property
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// Vehicle make and model property
        /// </summary>
        public string VehicleMakeModel { get; set; }

        /// <summary>
        /// Registration plate property
        /// </summary>
        public string RegistrationPlate { get; set; }

        public string DateAllDone { get; set; }
        public IEnumerable<ServiceViewModel> Services { get; set; }
        public string Currency { get; set; }
        public decimal Price { get; set; }

    }
}
