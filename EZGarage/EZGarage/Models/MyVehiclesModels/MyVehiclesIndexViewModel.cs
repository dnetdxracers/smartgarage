﻿using EZGarage.Web.Models.UserViewModels;
using EZGarage.Web.Models.VehicleViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.MyVehiclesModels
{
    public class MyVehiclesIndexViewModel
    {
        public MyVehiclesIndexViewModel(CustomerViewModel customer, IEnumerable<VehicleViewModel> vehicles)
        {
            this.Customer = customer;
            this.Vehicles = vehicles;
        }

        public CustomerViewModel Customer { get; set; }
        public IEnumerable<VehicleViewModel> Vehicles { get; set; }
    }
    
}
