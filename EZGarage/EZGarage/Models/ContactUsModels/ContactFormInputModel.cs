﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.ContactUsModels
{
    public class ContactFormInputModel
    {
        /// <summary>
        /// Empty ctor
        /// </summary>
        public ContactFormInputModel()
        {

        }

        /// <summary>
        /// Ctor 
        /// </summary>
        /// <param name="names">Names</param>
        /// <param name="email">Email</param>
        /// <param name="phoneNumber">Phone number</param>
        /// <param name="body">Enquiry body</param>
        public ContactFormInputModel(string names,
            string email,
            string phoneNumber,
            string body)
        {
            this.Names = names;
            this.Email = email;
            this.PhoneNumber = phoneNumber;
            this.Body = body;
        }

        /// <summary>
        /// Name property
        /// </summary>
        [Required(ErrorMessage ="Names are required")]
        [MinLength(2)]
        public string Names { get; set; }

        /// <summary>
        /// Email property
        /// </summary>
        [Required(ErrorMessage ="Email is required")]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Phone number property
        /// </summary>
        [Required(ErrorMessage = "Phone number is required")]
        [StringLength(10, MinimumLength = 10, ErrorMessage ="Please enter a valid phone number, i.e. 0888123456")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Body property
        /// </summary>
        [Required(ErrorMessage = "Body of enquiry is required")]
        [StringLength(1000, MinimumLength = 20, ErrorMessage ="Please describe your enquiry in at least 20 symbols, thank you")]
        public string Body { get; set; }

        /// <summary>
        /// IsSent boolean property for alerts
        /// </summary>
        public bool IsSent { get; set; }
    }
}
