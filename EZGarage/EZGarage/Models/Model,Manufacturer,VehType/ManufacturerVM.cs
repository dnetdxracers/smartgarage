﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.Model_Manufacturer_VehType
{
    public class ManufacturerVM
    {
        public ManufacturerVM(Manufacturer manufacturer)
        {
            this.Id = manufacturer.Id;
            this.Name = manufacturer.Name;
        }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
