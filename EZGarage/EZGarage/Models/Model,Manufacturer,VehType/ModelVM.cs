﻿using EZGarage.Data.Models;
using EZGarage.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.Model_Manufacturer_VehType
{
    public class ModelVM
    {
        public ModelVM(ModelDto model)
        {
            this.Id = model.Id;
            this.Name = model.Name;
            this.ManufacturerId = model.ManufacturerId;
            this.ManufacturerName = model.Manufacturer;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
    }
}
