﻿using EZGarage.Services.Dto;

namespace EZGarage.Web.Models.Model_Manufacturer_VehType
{
    public class VehicleTypeVM
    {
        /// <summary>
        /// Ctor with dto injection
        /// </summary>
        /// <param name="vehicleType"></param>
        public VehicleTypeVM(VehicleTypeDto vehicleType)
        {
            this.Id = vehicleType.Id;
            this.Name = vehicleType.Name;
        }

        /// <summary>
        /// Id prop
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name prop
        /// </summary>
        public string Name { get; set; }
    }
}
