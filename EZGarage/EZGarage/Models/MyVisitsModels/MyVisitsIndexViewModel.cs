﻿using EZGarage.Web.Models.UserViewModels;
using EZGarage.Web.Models.VisitViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.MyVisitsModels
{
    public class MyVisitsIndexViewModel
    {
        private readonly CustomerViewModel customer;
        private readonly IEnumerable<VisitDetailedViewModel> visits;

        public MyVisitsIndexViewModel(CustomerViewModel customer, IEnumerable<VisitDetailedViewModel> visits)
        {
            this.Customer = customer;
            this.Visits = visits;
        }

        public CustomerViewModel Customer { get; set; }
        public IEnumerable<VisitDetailedViewModel> Visits { get; set; }


    }
}
