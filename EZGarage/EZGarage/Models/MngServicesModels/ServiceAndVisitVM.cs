﻿using EZGarage.Web.Models.MyServicesModels;
using EZGarage.Web.Models.VisitViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.MngServicesModels
{
    public class ServiceAndVisitVM
    {
        public ServiceAndVisitVM(ServiceViewModel serviceVM, VisitViewModel visitVM)
        {
            this.Service = serviceVM;
            this.Visit = visitVM;
        }
        public ServiceViewModel Service { get; set; }

        public VisitViewModel Visit { get; set; }
    }
}
