﻿using EZGarage.Web.Models.MyServicesModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.MngServicesModels
{
    public class MngServicesIndexVM
    {
        public MngServicesIndexVM(IEnumerable<ServiceViewModel> serviceVMs)
        {
            this.Services = serviceVMs;
        }
        public IEnumerable<ServiceViewModel> Services { get; set; }
    }
}
