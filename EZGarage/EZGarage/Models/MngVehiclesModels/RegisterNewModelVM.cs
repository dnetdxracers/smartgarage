﻿using EZGarage.Web.Models.Model_Manufacturer_VehType;
using EZGarage.Web.Models.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.MngVehiclesModels
{
    public class RegisterNewModelVM
    {
        public RegisterNewModelVM(IList<ModelVM> modelVMs, IList<ManufacturerVM> manufacturerVMs, string message)
        {
            this.Models = modelVMs;
            this.Manufacturers = manufacturerVMs;
            this.Message = message;
        }
        public IList<ModelVM> Models { get; set; }
        public IList<ManufacturerVM> Manufacturers { get; set; }
        public string Message { get; set; }

    }
}
