﻿using EZGarage.Web.Models.UserViewModels;
using EZGarage.Web.Models.VehicleViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.MngVehiclesModels
{
    public class MngVehiclesIndexVM
    {
        public MngVehiclesIndexVM(VehicleViewModel vehicleVM, IEnumerable<VehicleViewModel> vehicleVMs, string purpose,string context)
        {
            this.VehicleVM = vehicleVM;
            this.VehicleVMs = vehicleVMs;
            this.Purpose = purpose;
            this.Context = context;
        }
        public VehicleViewModel VehicleVM { get; set; }
        public IEnumerable<VehicleViewModel> VehicleVMs { get; set; }

        public string Purpose { get; set; }

        public string Context { get; set; }
    }
}
