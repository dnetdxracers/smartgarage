﻿using EZGarage.Services.Dto;
using EZGarage.Web.Models.VehicleViewModels;
using EZGarage.Web.Models.VisitViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.UserViewModels
{
    public class CustomerViewModel
    {
        /// <summary>
        /// Ctor with dto injection
        /// </summary>
        /// <param name="customerDto"></param>
        public CustomerViewModel(CustomerDtoDetailed customerDto)
        {
            this.Id = customerDto.Id;
            this.Name = customerDto.FirstName + " " + customerDto.LastName;
            this.Phone = customerDto.Phone;
            this.Email = customerDto.Email;
            this.Vehicles = customerDto.Vehicles.Select(ve => new VehicleViewModel(ve));
            this.Visits = customerDto.Visits.Select(vi => new VisitViewModel(vi));
            this.RoleId = 2;
        }

        public CustomerViewModel(CustomerDtoSmall customerDtoSmall)
        {
            this.Id = customerDtoSmall.Id;
            this.Name = customerDtoSmall.Name;
            this.Phone = customerDtoSmall.Phone;
            this.Email = customerDtoSmall.Email;
        }

        public int Id { get; set; }
        public string Name { get; }
        public string Phone { get; }
        public string Email { get; }
        public IEnumerable<VehicleViewModel> Vehicles { get; }
        public IEnumerable<VisitViewModel> Visits { get; }
        public int RoleId { get; }
    }
}
