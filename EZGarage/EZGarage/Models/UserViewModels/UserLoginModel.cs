﻿using EZGarage.Data.Models;
using EZGarage.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Web.Models.UserViewModels
{
    public class UserLoginModel
    {
        public UserLoginModel(UserLoginDto user)
        {
            this.Id = user.Id;
            this.Role = user.Role;
            this.Email = user.Email;
        }

        public int Id { get; }
        public string Role { get; }
        public string Email { get; }

    }

}
