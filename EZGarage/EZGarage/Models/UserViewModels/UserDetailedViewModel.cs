﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Models.UserViewModels
{
    public class UserDetailedViewModel
    {
        /// <summary>
        /// Empty ctor
        /// </summary>
        public UserDetailedViewModel()
        {

        }

        /// <summary>
        /// Ctor with props
        /// </summary>
        /// <param name="id"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="phone"></param>
        /// <param name="email"></param>
        /// <param name="roleId"></param>
        public UserDetailedViewModel(int id,
            string firstName,
            string lastName,
            string phone,
            string email,
            int roleId)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Phone = phone;
            Email = email;
            RoleId = roleId;
        }

        /// <summary>
        /// Id prop
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// First name prop
        /// </summary>
        [Required(ErrorMessage = "First name required")]
        public string FirstName { get; set; }
        /// <summary>
        /// Last name prop
        /// </summary>
        [Required(ErrorMessage = "Last name required")]
        public string LastName { get; set; }
        /// <summary>
        /// Phone number prop
        /// </summary>
        [Required(ErrorMessage = "Phone number is required")]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "Please enter a valid phone number, i.e. 0888123456")]
        public string Phone { get; set; }
        /// <summary>
        /// Email prop
        /// </summary>
        [Required(ErrorMessage = "Phone number is required")]
        [EmailAddress]
        public string Email { get; set; }
        /// <summary>
        /// Role id prop
        /// </summary>
        [Range(1, 2, ErrorMessage = "Please choose from 1 = Employee, 2 = Customer")]
        public int RoleId { get; set; } = 2;
    }
}
