﻿using EZGarage.Services.Dto.Visits;
using EZGarage.Web.PDFBuilder.ToSTringBuilder;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.Drawing;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using System;
using System.IO;

namespace EZGarage.Web.PDFBuilder
{
    public class PDFBuilder : IPDFBuilder
    {
        private readonly ToStringBuilder toStringBuilder;

        public PDFBuilder(ToStringBuilder toStringBuilder)
        {
            this.toStringBuilder = toStringBuilder;
        }
        public FileStreamResult GeneratePdfReport(VisitDtoDetailed visitData, string requiredFormat)
        {

            if (requiredFormat.ToLower() == "simple")
                return this.CreateSimplePdf(visitData);

            throw new NotImplementedException();

        }

        public FileStreamResult CreateSimplePdf(VisitDtoDetailed visitData)
        {
            //Create a new PDF document
            PdfDocument document = new PdfDocument();

            //Add a page to the document
            PdfPage page = document.Pages.Add();

            //Create PDF graphics for the page
            PdfGraphics graphics = page.Graphics;

            //Set the standard font
            PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 20);

            //Draw the text

            RectangleF bounds = new RectangleF(PointF.Empty, page.Graphics.ClientSize);

            graphics.DrawString($"{toStringBuilder.VisitDtoDetailedToString(visitData)}", font, PdfBrushes.Black, new PointF(0, 0));
            

            //Saving the PDF to the MemoryStream
            MemoryStream stream = new MemoryStream();

            document.Save(stream);

            //Set the position as '0'.
            stream.Position = 0;

            //Download the PDF document in the browser
            FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/pdf");

            fileStreamResult.FileDownloadName = "Sample.pdf";

            return fileStreamResult;
        }
    }
}
