﻿using EZGarage.Services.Dto.Visits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Web.PDFBuilder.ToSTringBuilder
{
    public class ToStringBuilder
    {
        public string VisitDtoDetailedToString(VisitDtoDetailed visitDtoDetailed)
        {
            var output = new StringBuilder();
            output.AppendLine($"Report for customer visit:");
            output.AppendLine($"CUSTOMER: {visitDtoDetailed.CustomerName}");
            output.AppendLine($"VEHICLE: {visitDtoDetailed.VehicleMakeModel}");
            output.AppendLine($"REGISTRATION PLATE: {visitDtoDetailed.RegistrationPlate}");
            output.AppendLine($"SERVICES:");
            output.AppendLine();

            var services = visitDtoDetailed.Services.ToList();
            for (var i = 0; i < services.Count(); i++)
            {
                var service = services[i];
                output.AppendLine($"{i+1}:");
                output.AppendLine($"{service.ServiceItem}");
                output.AppendLine($"{service.ReadyDate}");
                output.AppendLine($"{service.Price}");
                output.AppendLine($"{service.Currency}");
                output.AppendLine();

            }
            output.AppendLine($"READY DATE: {visitDtoDetailed.ReadyDate}");
            output.AppendLine($"TOTAL PRICE: {visitDtoDetailed.TotalPrice}");

            return output.ToString().Trim();
        }
    }
}
