﻿using EZGarage.Services.Dto.Visits;
using Microsoft.AspNetCore.Mvc;

namespace EZGarage.Web.PDFBuilder
{
    public interface IPDFBuilder
    {
        FileStreamResult CreateSimplePdf(VisitDtoDetailed visitData);
        FileStreamResult GeneratePdfReport(VisitDtoDetailed visitData, string requiredFormat);
    }
}