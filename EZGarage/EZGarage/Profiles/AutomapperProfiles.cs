﻿using AutoMapper;
using EZGarage.Data.Models;
using EZGarage.Services.Dto;
using EZGarage.Services.Dto.ServiceItems;
using EZGarage.Services.Dto.Services;
using EZGarage.Services.Dto.Visits;
using EZGarage.Web.Models.ServiceItemModels;
using EZGarage.Web.Models.UserViewModels;
using EZGarage.Web.Models.VehicleViewModels;
using EZGarage.Web.Models.VisitViewModels;

namespace EZGarage.Web.Profiles
{
    public class AutomapperProfiles : Profile
    {
        public AutomapperProfiles()
        {
            // Service maps
            // Service -> Web


            // Service item maps
            // Service -> Web
            CreateMap<ServiceItemDto, ServiceItemViewModel>().ReverseMap();
            CreateMap<ServiceItemDto, ServiceItemInputViewModel>().ReverseMap();
            CreateMap<PostServiceItemDto, ServiceItemViewModel>().ReverseMap();


            // Visit maps
            // Data -> Service
            CreateMap<Visit, VisitDto>().ReverseMap();
            CreateMap<Visit, VisitDtoDetailed>().ReverseMap();
            CreateMap<Visit, PostVisitDto>().ReverseMap();

            // Service -> Web
            CreateMap<VisitDto, VisitViewModel>().ReverseMap();
            CreateMap<VisitDtoDetailed, VisitViewModel>().ReverseMap();
            CreateMap<PostVisitDto, VisitViewModel>().ReverseMap();
            CreateMap<VisitDtoDetailed, VisitDetailedViewModel>().ReverseMap();

            // User
            // Service -> Web
            CreateMap<PostUserDtoRequest, UserDetailedViewModel>().ReverseMap();
            CreateMap<PostUserDtoResponse, UserDetailedViewModel>().ReverseMap();
            CreateMap<CustomerDtoDetailed, UserDetailedViewModel>().ReverseMap();

            // Vehicles
            CreateMap<VehicleDto, VehicleViewModel>().ReverseMap();
            CreateMap<PostVehicleDto, VehicleViewModel>().ReverseMap();
        }
    }
}
