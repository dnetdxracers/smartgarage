﻿using EZGarage.Services.Contracts;
using EZGarage.Services.Dto.Visits;
using EZGarage.Web.Helpers;
using EZGarage.Web.Models.MngVisitsModels;
using EZGarage.Web.Models.ServiceItemModels;
using EZGarage.Web.Models.VehicleViewModels;
using EZGarage.Web.Models.VisitViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("/MngVisits")]
    [EzAuhtorize(Role = "employee")]
    public class MngVisitsController : Controller
    {
        /// <summary>
        /// Fields for calling injected services
        /// </summary>
        private readonly IVisitService visitService;
        private readonly IVehicleService vehicleService;
        private readonly IServiceService serviceService;
        private readonly IServiceItemService serviceItemService;

        /// <summary>
        /// ctor of controller
        /// </summary>
        /// <param name="visitService"></param>
        /// <param name="vehicleService"></param>
        /// <param name="serviceService"></param>
        /// <param name="serviceItemService"></param>
        public MngVisitsController(IVisitService visitService, IVehicleService vehicleService, IServiceService serviceService, IServiceItemService serviceItemService)
        {
            this.visitService = visitService;
            this.vehicleService = vehicleService;
            this.serviceService = serviceService;
            this.serviceItemService = serviceItemService;
        }

        /// <summary>
        /// Index View with all visits
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var visits = await visitService.GetAllAsync();
            var visitVMs = visits.Select(vi => new VisitViewModel(vi));



            return View(visitVMs);
        }

        /// <summary>
        /// Visists by customer ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCustomerVisits(int id)
        {
            try
            {
                var visits = await visitService.GetAllAsync();

                var visitVMs = visits.Where(vi => vi.CustomerId == id)
                    .Select(vi => new VisitViewModel(vi));

                return View(visitVMs);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

        }

        /// <summary>
        /// Detailed visit view by Visit ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="action"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost("single/{id}")]
        public async Task<IActionResult> Single(int id, string action, string context)
        {
            try
            {
                var visit = await this.visitService.GetDetailedVisitAsync(id);
                var visitVM = new VisitDetailedViewModel(visit);
                visitVM.Action = action;
                visitVM.Context = context;

                return View(visitVM);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

        }

        /// <summary>
        /// Calls the delete view (request and confirm) depeding on the string message
        /// </summary>
        /// <param name="id"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        [HttpPost("delete/{id}")]
        public async Task<IActionResult> Delete(int id, string action)
        {
            try
            {
                if (action == "deleterequest")
                {
                    var visitVM = await this.GetVisitByIdAndAllServiceItems(id);

                    visitVM.Context = action;

                    return View(visitVM);
                }
                else
                {
                    await visitService.DeleteAsync(id);
                    var emptyVisitVM = new VisitAndServiceItemsVM();

                    emptyVisitVM.Context = action;

                    return View(emptyVisitVM);
                }
            }
            catch (Exception e)
            {
                var emptyVisitVMfail = new VisitAndServiceItemsVM();
                emptyVisitVMfail.Context = e.Message;

                return View(emptyVisitVMfail);
            }

        }

        /// <summary>
        /// Calls the view for adding services to a visit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("{id}/addservices")]
        public async Task<IActionResult> AddServices(int id)
        {
            try
            {
                var visit = await visitService.GetDetailedVisitAsync(id);
                var visitVM = new VisitDetailedViewModel(visit);

                var serviceItems = await serviceItemService.GetAllAsync();
                var serviceitemVMs = serviceItems.Select(si => new ServiceItemViewModel(si));

                var visitServiceItemVM = new VisitAndServiceItemsVM(visitVM, serviceitemVMs);

                return View(visitServiceItemVM);
            }
            catch (Exception e)
            {

                return NotFound(e.Message);
            }

        }

        /// <summary>
        /// Performs the adding of a service and returns back to the calling View
        /// </summary>
        /// <param name="id"></param>
        /// <param name="serviceItemId"></param>
        /// <param name="statusId"></param>
        /// <param name="readyDate"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> ServiceAdder(int id, int serviceItemId, int statusId, string readyDate)
        {

            try
            {
                await this.serviceService.PostAsync(new Services.Dto.Services.PostServiceDto
                {
                    VisitId = id,
                    ServiceItemId = serviceItemId,
                    StatusId = statusId,
                    ReadyDate = readyDate
                });
            }
            catch (Exception e)
            {
                var visitAndServiceItems = await GetVisitByIdAndAllServiceItems(id);
                visitAndServiceItems.Context = "fail";
                visitAndServiceItems.Message = e.Message;

                return View("addservices", visitAndServiceItems);
            }

            try
            {
                var visitVMAndServiceItemsVMs = await GetVisitByIdAndAllServiceItems(id);
                visitVMAndServiceItemsVMs.Context = "success";

                return View("addservices", visitVMAndServiceItemsVMs);
            }
            catch (Exception e)
            {

                return NotFound(e.Message);
            }


        }

        /// <summary>
        /// Calls the form view for creating a visit
        /// </summary>
        /// <returns></returns>
        [HttpPost("createnew")]
        public async Task<IActionResult> CreateNew()
        {
            var serviceItems = await serviceItemService.GetAllAsync();
            var serviceItemVMs = serviceItems.Select(si => new ServiceItemViewModel(si));
            var vehicles = await vehicleService.GetAllAsync();
            var vehicleVMs = vehicles.Select(ve => new VehicleViewModel(ve));

            var createVisitFormVM = new CreateNewVisitFormVM(vehicleVMs, serviceItemVMs);

            return View(createVisitFormVM);
        }

        /// <summary>
        /// Performs the creation of the visit and return to a view with the newly created service
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="arrivalDate"></param>
        /// <param name="serviceItemIds"></param>
        /// <param name="readyDates"></param>
        /// <returns></returns>
        [HttpPost("visitcreator")]
        public async Task<IActionResult> VisitCreator(int vehicleId, string arrivalDate, string serviceItemIds, string readyDates)
        {
            int id;
            try
            {
                var serviceItemsIds = serviceItemIds.Trim().Split(" ").Select(x => int.Parse(x)).ToArray();
                var servicesFinishDates = readyDates.Trim().Split(" ");

                var vehicle = await this.vehicleService.GetOneByIdAsync(vehicleId);
                var customerId = vehicle.CustomerId;

                var request = new PostVisitDto
                {
                    CustomerId = customerId,
                    VehicleId = vehicleId,
                    ArrivalDate = arrivalDate,
                    ServiceIteIds = serviceItemsIds,
                    ServiceItemFinishDates = servicesFinishDates,
                    //ServiceItemFinishDates = readyDates,
                };

                var response = await this.visitService.PostAsync(request);
                id = response.Id;
            }
            catch (Exception e)
            {
                return View("ConfirmCreated", new CreateVisitVM(null, "fail", e.Message));
            }


            try
            {
                var visit = await this.visitService.GetDetailedVisitAsync(id);
                var visitVM = new VisitDetailedViewModel(visit);

                var createVisitVM = new CreateVisitVM(visitVM, "success", null);

                return View("ConfirmCreated", createVisitVM);
            }
            catch (Exception e)
            {

                return NotFound(e.Message);
            }

        }


        /// <summary>
        /// Aux method
        /// </summary>
        /// <param name="visitId"></param>
        /// <returns></returns>
        private async Task<VisitAndServiceItemsVM> GetVisitByIdAndAllServiceItems(int visitId)
        {
            var visit = await visitService.GetDetailedVisitAsync(visitId);
            var visitVM = new VisitDetailedViewModel(visit);

            var serviceItems = await serviceItemService.GetAllAsync();
            var serviceitemVMs = serviceItems.Select(si => new ServiceItemViewModel(si));

            var visitServiceItemVM = new VisitAndServiceItemsVM(visitVM, serviceitemVMs);

            return visitServiceItemVM;
        }

    }
}
