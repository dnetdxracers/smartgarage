﻿using EZGarage.Services.Contracts;
using EZGarage.Web.Models.MyVisitsModels;
using EZGarage.Web.Models.UserViewModels;
using EZGarage.Web.Models.VisitViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.HtmlConverter;
using Syncfusion.Pdf;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("myvisits")]
    public class MyVisitsController : Controller
    {
        /// <summary>
        /// Fields
        /// </summary>
        private readonly IUserService userService;
        private readonly IVisitService visitService;
        private readonly IHostingEnvironment hostingEnvironment;

        /// <summary>
        /// Ctor with service injections
        /// </summary>
        /// <param name="userService">User service</param>
        /// <param name="visitService">Visit service</param>
        /// <param name="hostingEnvironment">Hosting environment</param>
        public MyVisitsController(IUserService userService, IVisitService visitService, IHostingEnvironment hostingEnvironment)
        {
            this.userService = userService;
            this.visitService = visitService;
            this.hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// Index endpoint
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Index(int id)
        {
            try
            {
                var customer = await this.userService.GetOneCustomerAsync(id);
                var customerVisits = await this.visitService.GetAllAsync(id);

                var customerViewModel = new CustomerViewModel(customer);
                var customerVisitsViewModel = customerVisits.Select(vi => new VisitDetailedViewModel(vi));

                var indexViewModel = new MyVisitsIndexViewModel(customerViewModel, customerVisitsViewModel);

                return View(indexViewModel);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Post method to view detailed visit report in base currency
        /// </summary>
        /// <param name="id">Visit Id</param>
        /// <returns></returns>
        [HttpPost("detailedreportbgn/{id}")]
        public async Task<IActionResult> DetailedReportBGN(int id)
        {
            try
            {
                var visit = await this.visitService.GetDetailedVisitAsync(id);

                var visitViewModel = new VisitDetailedViewModel(visit);

                return View(visitViewModel);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }


        /// <summary>
        /// Get method for downloading report without forex
        /// </summary>
        /// <param name="id">Visit Id</param>
        /// <returns></returns>
        [HttpGet("downloadvisitreportbgn/{id}")]
        public async Task<IActionResult> DownloadVisitReport(int id)
        {
            try
            {
                var visit = await this.visitService.GetDetailedVisitAsync(id);

                var visitViewModel = new VisitDetailedViewModel(visit);
                visitViewModel.ReportCurrency = visit.Currency;
                visitViewModel.ReportCurrencyPrice = visit.TotalPrice.ToString();

                return View(visitViewModel);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Post method to create pdf report for visit without currency
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("PDFproviderbgn/{id}")]
        public IActionResult PDFproviderBGN(int id)
        {
            try
            {
                HtmlToPdfConverter converter = new HtmlToPdfConverter();
                WebKitConverterSettings settings = new WebKitConverterSettings();
                settings.WebKitPath = Path.Combine(hostingEnvironment.ContentRootPath, @"Syncfusion\QtBinariesWindows");
                converter.ConverterSettings = settings;

                var stringRoute = "http://localhost:5000/myvisits/downloadvisitreportbgn/" + $"{id}";
                PdfDocument document = converter.Convert(stringRoute);

                MemoryStream ms = new MemoryStream();
                document.Save(ms);
                document.Close(true);
                ms.Position = 0;

                FileStreamResult fileStreamResult = new FileStreamResult(ms, "application/pdf");
                fileStreamResult.FileDownloadName = ("Report.pdf");

                return fileStreamResult;
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Post method to view detailed visit report
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("detailedreport/{id}")]
        public async Task<IActionResult> DetailedReport(int id)
        {
            try
            {
                var visit = await this.visitService.GetDetailedVisitAsync(id);

                var visitViewModel = new VisitDetailedViewModel(visit);

                return View(visitViewModel);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get method for downloading report with forex
        /// </summary>
        /// <param name="id">Visit Id</param>
        /// <param name="reportCurrency">Report selected currency</param>
        /// <param name="reportAmount">Report converted amount</param>
        /// <returns></returns>
        [HttpGet("downloadvisitreport/{id}/currency/{reportCurrency}/amount/{reportAmount}")]
        public async Task<IActionResult> DownloadVisitReport(int id, string reportCurrency, string reportAmount)
        {
            try
            {
                var visit = await this.visitService.GetDetailedVisitAsync(id);

                var visitViewModel = new VisitDetailedViewModel(visit);
                visitViewModel.ReportCurrency = reportCurrency;
                visitViewModel.ReportCurrencyPrice = reportAmount;

                return View(visitViewModel);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Post method to create pdf report for visit with currency
        /// </summary>
        /// <param name="id"></param>
        /// <param name="forexCurrency"></param>
        /// <param name="forexPrice"></param>
        /// <returns></returns>
        [HttpPost("PDFprovider/{id}")]
        public IActionResult PDFprovider(int id, string forexCurrency, string forexPrice)
        {
            //HTML to PDF conversion code.
            try
            {
                HtmlToPdfConverter converter = new HtmlToPdfConverter();
                WebKitConverterSettings settings = new WebKitConverterSettings();
                settings.WebKitPath = Path.Combine(hostingEnvironment.ContentRootPath, @"Syncfusion\QtBinariesWindows");
                converter.ConverterSettings = settings;

                var stringRoute = "http://localhost:5000/myvisits/downloadvisitreport/" + $"{id}" + "/currency/" + $"{forexCurrency}" + " /amount/" + $"{forexPrice}";
                PdfDocument document = converter.Convert(stringRoute);

                MemoryStream ms = new MemoryStream();
                document.Save(ms);
                document.Close(true);
                ms.Position = 0;

                FileStreamResult fileStreamResult = new FileStreamResult(ms, "application/pdf");
                fileStreamResult.FileDownloadName = ("Report.pdf");

                return fileStreamResult;
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

    }
}
