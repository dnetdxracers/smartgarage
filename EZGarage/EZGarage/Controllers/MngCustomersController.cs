﻿using AutoMapper;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using EZGarage.Web.Helpers;
using EZGarage.Web.Models.MngCustomersModels;
using EZGarage.Web.Models.UserViewModels;
using EZGarage.Web.Models.VehicleViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("mngcustomers")]
    [EzAuhtorize(Role = "employee")]
    public class MngCustomersController : Controller
    {
        /// <summary>
        /// Fields
        /// </summary>
        private readonly IUserService userService;
        private readonly IMapper mapper;
        private readonly IVehicleService vehicleService;
        private readonly IManufacturerService manufacturerService;
        private readonly IModelService modelService;
        private readonly IVehicleTypeService vehicleTypeService;

        /// <summary>
        /// Ctor with services injections
        /// </summary>
        /// <param name="userService">User service</param>
        /// <param name="mapper">Automapper</param>
        /// <param name="vehicleService">Vehicle service</param>
        /// <param name="manufacturerService">Manufacturer service</param>
        /// <param name="modelService">Model service</param>
        /// <param name="vehicleTypeService">Vehicle type service</param>
        public MngCustomersController(IUserService userService,
            IMapper mapper,
            IVehicleService vehicleService,
            IManufacturerService manufacturerService,
            IModelService modelService,
            IVehicleTypeService vehicleTypeService)
        {
            this.userService = userService;
            this.mapper = mapper;
            this.vehicleService = vehicleService;
            this.manufacturerService = manufacturerService;
            this.modelService = modelService;
            this.vehicleTypeService = vehicleTypeService;
        }

        /// <summary>
        /// Index endpoint with search/filtration params
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="name">Names</param>
        /// <param name="phone">Phone</param>
        /// <param name="email">Email</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(int? customerId = null,
            string name = null,
            string phone = null,
            string email = null)
        {
            try
            {
                var matchingCustomers = await this.userService.GetManyMngCustomersWebAsync(customerId,
                    name,
                    phone,
                    email);

                var customers = matchingCustomers.Select(c => new CustomerViewModel(c));

                var indexCustomers = new MngCustomersIndexViewModel(null, customers);

                return View("Index", indexCustomers);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get endpoint for creating new customers
        /// </summary>
        /// <returns></returns>
        [HttpGet("create")]
        public IActionResult Create()
        {
            try
            {
                ViewData["IsCreated"] = "User was created successfully!";

                return View();
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Post endpoint for creating new customers
        /// </summary>
        /// <param name="customer">Customer view model</param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<IActionResult> Create(UserDetailedViewModel customer)
        {
            var customerDto = this.mapper.Map<PostUserDtoRequest>(customer);
            try
            {
                var result = await this.userService.PostUserAsync(customerDto);
            }
            catch (Exception e)
            {

                return NotFound(e.Message);
            }

            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Get endpoint for customer details by id
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns></returns>
        [HttpGet("details/{id}")]
        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var userDto = await this.userService.GetOneCustomerAsync(id);

                var user = new CustomerViewModel(userDto);

                var result = new MngCustomersIndexViewModel(user, null);

                return View(result);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }


        /// <summary>
        /// Get method to edit customer by id
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns></returns>
        [HttpGet("edit/{id}")]
        public async Task<IActionResult> Edit([FromRoute] int id)
        {
            try
            {
                var customerDto = await this.userService.GetOneCustomerAsync(id);
                var customer = this.mapper.Map<UserDetailedViewModel>(customerDto);

                return View("Edit", customer);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Post method to edit customer by customer Id
        /// </summary>
        /// <param name="userViewModel">User view model</param>
        /// <returns></returns>
        [HttpPost("edit/{id}")]
        public async Task<IActionResult> Edit(UserDetailedViewModel userViewModel)
        {
            try
            {
                var userDto = await this.userService.PutUserAsync(userViewModel.Id,
                    userViewModel.FirstName,
                    userViewModel.LastName,
                    userViewModel.Phone,
                    userViewModel.Email);

                TempData["IsEdit"] = "User was updated successfully!";

                return View();
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get method to delete service item by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("delete/{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            try
            {
                var customerDto = await this.userService.GetOneCustomerAsync(id);

                var user = new CustomerViewModel(customerDto);

                var result = new MngCustomersIndexViewModel(user, null);

                return View(result);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Post method for deleting an existing item by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("delete/{id}")]
        public async Task<IActionResult> DeleteForm([FromRoute] int id)
        {
            try
            {
                await this.userService.DeleteCustomerAsync(id);
                TempData["IsDeleted"] = "User was deleted successfully!";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get endpoint to add vehicle to customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns></returns>
        [HttpGet("addvehicle/{customerId}")]
        public IActionResult AddVehicle([FromRoute] int customerId)
        {
            try
            {
                var manufacturers = this.manufacturerService.GetAllAsync().Result;

                var models = this.modelService.GetAllAsync().Result;

                VehicleViewModel v = new VehicleViewModel
                {
                    CustomerId = customerId,
                    Manufacturers = manufacturers.Select(m => new SelectListItem
                    {
                        Text = m.Name,
                        Value = m.Id.ToString()
                    }),
                    Models = models.Select(mo => new SelectListItem
                    {
                        Text = mo.Name,
                        Value = mo.Id.ToString()
                    })

                };

                return View(v);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Post endpoint to add vehicle to customer by id
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="vehicleViewModel">Vehicle view model</param>
        /// <returns></returns>
        [HttpPost("addvehicle/{customerId}")]
        public async Task<IActionResult> AddVehicle([FromRoute] int customerId, VehicleViewModel vehicleViewModel)
        {
            try
            {
                var modelId = vehicleViewModel.ModelId;

                var vehicles = await this.vehicleService.GetAllAsync();

                var vehicleTypeId = vehicles.FirstOrDefault(v => v.ModelId == modelId).VehicleTypeId;

                var customer = await this.userService.GetOneCustomerAsync(customerId);

                vehicleViewModel.VehicleTypeId = vehicleTypeId;

                var vehicleDto = this.mapper.Map<PostVehicleDto>(vehicleViewModel);

                var result = await this.vehicleService.PostAsync(vehicleDto);


                TempData["IsAdded"] = "Vehicle successfully added to customer!";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }


    }
}
