﻿using EZGarage.Services.Contracts;
using EZGarage.Services.Util.Models;
using EZGarage.Web.Models.ContactUsModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("contactus")]
    public class ContactUsController : Controller
    {
        /// <summary>
        /// Mail service field
        /// </summary>
        private readonly IMailService mailService;

        /// <summary>
        /// Ctor with mail service injection
        /// </summary>
        /// <param name="mailService">Mail service</param>
        public ContactUsController(IMailService mailService)
        {
            this.mailService = mailService;
        }

        /// <summary>
        /// Index get method
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                var input = new ContactFormInputModel();
                return View(input);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }



        /// <summary>
        /// Contact post method
        /// </summary>
        /// <param name="inputForm">Contact input form</param>
        /// <returns></returns>
        [HttpPost("send")]
        public async Task<IActionResult> Contact(ContactFormInputModel inputForm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var stringBuilder = new StringBuilder();

                    stringBuilder.Append(String.Format("Customer {0}, Contact info {1}, {2}", inputForm.Names, inputForm.PhoneNumber, inputForm.Email));
                    stringBuilder.Append(Environment.NewLine);
                    stringBuilder.Append(String.Format("Enquiry: {0}", inputForm.Body));

                    var mail = new MailRequest
                    {
                        Subject = String.Format("Enquiry from {0}, received at {1}", inputForm.Names, DateTime.Now),
                        Body = stringBuilder.ToString(),
                        ToEmail = "ezgaragecustomerservice@gmail.com"
                    };

                    await this.mailService.SendEmailAsync(mail);
                    TempData["IsSent"] = "Contact form successfully sent, an EZ pro will get back to you ASAP!";

                    return RedirectToAction(nameof(Index));
                }
                return View("Index", inputForm);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
