﻿using EZGarage.Services.Contracts;
using EZGarage.Web.Models.MyVehiclesModels;
using EZGarage.Web.Models.UserViewModels;
using EZGarage.Web.Models.VehicleViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("myvehicles")]
    public class MyVehiclesController : Controller
    {
        /// <summary>
        /// Fields
        /// </summary>
        private readonly IVehicleService vehicleService;
        private readonly IUserService userService;

        /// <summary>
        /// Ctor with service injections
        /// </summary>
        /// <param name="vehicleService"></param>
        /// <param name="userService"></param>
        public MyVehiclesController(IVehicleService vehicleService, IUserService userService)
        {
            this.vehicleService = vehicleService;
            this.userService = userService;
        }

        /// <summary>
        /// Index endpoint to show visits of customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> IndexAsync(int id)
        {
            try
            {
                var customer = await this.userService.GetOneCustomerAsync(id);
                var customerVehicles = await this.vehicleService.GetAllAsync(id);

                var customerVM = new CustomerViewModel(customer);
                var vehiclesVM = customerVehicles.Select(ve => new VehicleViewModel(ve));

                var vehiclesIndexModel = new MyVehiclesIndexViewModel(customerVM, vehiclesVM);

                return View(vehiclesIndexModel);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

        }
    }
}
