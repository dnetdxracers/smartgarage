﻿using EZGarage.Services.Contracts;
using EZGarage.Web.Helpers;
using EZGarage.Web.Models.MngServicesModels;
using EZGarage.Web.Models.MyServicesModels;
using EZGarage.Web.Models.VisitViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [EzAuhtorize(Role = "employee")]
    public class MngServicesController : Controller
    {
        /// <summary>
        /// Field
        /// </summary>
        private readonly IServiceService serviceService;
        private readonly IVisitService visitService;

        /// <summary>
        /// Ctor with service injection
        /// </summary>
        /// <param name="serviceService"></param>
        /// <param name="visitService"></param>

        public MngServicesController(IServiceService serviceService, IVisitService visitService)
        {
            this.serviceService = serviceService;
            this.visitService = visitService;
        }

        /// <summary>
        /// Index endpoint
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var services = await serviceService.GetManyAsync();
            var servicesVMs = services.Select(se => new ServiceViewModel(se));

            var mngServicesVM = new MngServicesIndexVM(servicesVMs);

            return View(mngServicesVM);
        }

        /// <summary>
        /// Post method for detailed view of single service
        /// </summary>
        /// <param name="id">Service Id</param>
        /// <param name="action"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        [HttpPost("single/{id}")]
        public async Task<IActionResult> Single(int id, string action, string context)
        {
            try
            {
                var service = await serviceService.GetOneByIdAsync(id);
                var visit = await visitService.GetOneByServiceId(id);
                var serviceVM = new ServiceViewModel(service);
                var visitVM = new VisitViewModel(visit);
                serviceVM.Action = action;
                var serviceAndVisitVM = new ServiceAndVisitVM(serviceVM, visitVM);


                return View(serviceAndVisitVM);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Post method to advance status of service item
        /// </summary>
        /// <param name="id">Service Id</param>
        /// <returns></returns>
        [HttpPost("advance/{id}")]
        public async Task<IActionResult> Advance(int id)
        {
            string action = "advance";
            try
            {
                var service = await serviceService.AdvanceServiceAsync(id);
            }
            catch (Exception)
            {

                var serviceViewModel = await GetServiceVMById(id);
                var visitViewModel = await GetVisitVMByServiceId(id);

                serviceViewModel.Action = action;
                serviceViewModel.Context = "fail";

                var serviceAndVisitViewModel = new ServiceAndVisitVM(serviceViewModel, visitViewModel);

                return View("single", serviceAndVisitViewModel);
            }

            var serviceVM = await GetServiceVMById(id);
            var visitVM = await GetVisitVMByServiceId(id);
            serviceVM.Action = action;
            serviceVM.Context = "success";

            var serviceAndVisitVM = new ServiceAndVisitVM(serviceVM, visitVM);

            return View("single", serviceAndVisitVM);
        }

        /// <summary>
        /// Post method to delete service
        /// </summary>
        /// <param name="id">Service Id</param>
        /// <returns></returns>
        [HttpPost("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var visitVM = await GetVisitVMByServiceId(id);

                await serviceService.DeleteAsync(id);

                ServiceViewModel serviceVM = new ServiceViewModel();
                serviceVM.Action = "delete";
                serviceVM.Context = "success";

                var serviceAndVisitVM = new ServiceAndVisitVM(serviceVM, visitVM);
                return View("single", serviceAndVisitVM);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

        }

        /// <summary>
        /// Aux method to get service view model by service id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private async Task<ServiceViewModel> GetServiceVMById(int id)
        {
            var service = await serviceService.GetOneByIdAsync(id);
            return new ServiceViewModel(service);
        }

        /// <summary>
        /// Aux method to get visit by service id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private async Task<VisitViewModel> GetVisitVMByServiceId(int id)
        {
            var visit = await visitService.GetOneByServiceId(id);
            return new VisitViewModel(visit);
        }
    }
}
