﻿using EZGarage.Services.Contracts;
using EZGarage.Web.Models.MyServicesModels;
using EZGarage.Web.Models.UserViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("myservices")]
    public class MyServicesController : Controller
    {
        /// <summary>
        /// Fields
        /// </summary>
        private readonly IServiceService serviceService;
        private readonly IUserService userService;

        /// <summary>
        /// Ctor with service injections
        /// </summary>
        /// <param name="serviceService"></param>
        /// <param name="userService"></param>
        public MyServicesController(IServiceService serviceService, IUserService userService)
        {
            this.serviceService = serviceService;
            this.userService = userService;
        }

        /// <summary>
        /// Index ednpoint to get services of customer 
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Index(int id)
        {
            try
            {
                var userServiceDtos = await this.serviceService.GetManyAsync(id);

                var customerDto = await this.userService.GetOneCustomerAsync(id);

                var customerVM = new CustomerViewModel(customerDto);
                var serviceVMs = userServiceDtos.Select(se => new ServiceViewModel(se));

                var indexViewModel = new MyServicesIndexViewModel(customerVM, serviceVMs);

                return View(indexViewModel);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Filter by date post endpoint
        /// </summary>
        /// <param name="datefrom"></param>
        /// <param name="dateto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> FilterByDate(string datefrom = null, string dateto = null)
        {
            try
            {
                var customerId = HttpContext.Session.GetInt32("id");
                var customerDto = await this.userService.GetOneCustomerAsync((int)customerId);
                var filteredServices = await this.serviceService.GetManyAsync(customerId, null, null, null, null, null, null, null, null, null, datefrom, dateto);

                var customerVM = new CustomerViewModel(customerDto);
                var serviceVMs = filteredServices.Select(se => new ServiceViewModel(se));

                var indexViewModel = new MyServicesIndexViewModel(customerVM, serviceVMs);

                return View("Index", indexViewModel);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
