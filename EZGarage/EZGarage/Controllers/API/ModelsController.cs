﻿using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ModelsController : ControllerBase
    {
        /// <summary>
        /// field for calling an injected instance
        /// </summary>
        private readonly IModelService modelService;

        
        public ModelsController(IModelService modelService)
        {
            this.modelService = modelService;
        }

        /// <summary>
        /// Get model by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> GetOneAsync(int id)
        {
            try
            {
                var model = await this.modelService.GetModelByIdAsync(id);
                return Ok(model);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get list of all models
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> GetAllAsync(int id)
        {

            var moidels = await this.modelService.GetAllAsync();
            return Ok(moidels);

        }

        /// <summary>
        /// Create new model
        /// </summary>
        /// <param name="modelName"></param>
        /// <param name="manufacturerId"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> PostAsync([FromQuery] string modelName, int manufacturerId)
        {
            try
            {
                var newModel = await modelService.PostAsync(modelName, manufacturerId);

                return Ok(newModel);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Edit properties of a model
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newName"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> PutAsync(int id, [FromQuery] string newName)
        {
            try
            {
                var editedModel = await modelService.PutAsync(id, newName);

                return Ok(editedModel);
            }
            catch (NonExistentItemWithId e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
