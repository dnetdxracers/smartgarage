﻿using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using EZGarage.Services.Util.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UsersController : ControllerBase
    {
        /// <summary>
        /// User service field
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// Ctor with user service 
        /// </summary>
        /// <param name="userService"></param>
        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        /// <summary>
        /// User Login 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] UserInputModel model)
        {
            var response = userService.Authenticate(model);

            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(response);
        }

        /// <summary>
        /// Get Customer by id (detailed, employee auth)
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> GetOneCustomerAsync(int id)
        {
            try
            {
                var customer = await userService.GetOneCustomerAsync(id);
                return Ok(customer);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
        /// <summary>
        /// Reguster user (Employee auth)
        /// </summary>
        /// <param name="userRequest"></param>
        /// <returns></returns>
        [Authorize(Roles = "1")]
        [HttpPost("registeruser")]
        //public async Task<IActionResult> PostUserAsync([FromQuery] string firstName, string lastName, string phone, string email, int roleId)
        public async Task<IActionResult> PostUserAsync([FromBody] PostUserDtoRequest userRequest)
        {
            try
            {
                var response = await userService.PostUserAsync(userRequest);

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        /// <summary>
        /// Method to send email to user when requesting to update password 
        /// </summary>
        /// <param name="id">User Id</param>
        /// <returns></returns>
        [Authorize(Roles = "1")]
        [HttpPut("resetpassword/{id}")]
        public async Task<IActionResult> ResetUserPasswordAsync(int id)
        {
            try
            {
                var response = await userService.SendEmailToResetPasswordAsync(id);
                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        /// <summary>
        /// Get authenticated user profile
        /// </summary>
        /// <returns></returns>
        [HttpGet("profile")]
        public async Task<IActionResult> GetProfileAsync()
        { 
            var userId = int.Parse(this.User.FindFirst("id").Value);

            try
            {
                var customer = await userService.GetOneUserAsync(userId);
                return Ok(customer);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get all customers (compact, emplyee auth)
        /// </summary>
        /// <returns>List of customers</returns>
        [HttpGet("")]
        [Authorize(Roles ="1")]
        public async Task<IActionResult> GetAllCustomersAsync()
        {
            var customers = await userService.GetAllCustomersAsync();

            return Ok(customers);
        }


        /// <summary>
        /// Filter customers by keyword (detailed, employyee auth)
        /// </summary>
        /// <param name="nameSeeker"></param>
        /// <param name="phoneSeeker"></param>
        /// <param name="emailSeeker"></param>
        /// <param name="vehicleId"></param>
        /// <param name="vehiModelSeeker"></param>
        /// <param name="vehiManufSeeker"></param>
        /// <param name="visitBefore"></param>
        /// <param name="visitAfter"></param>
        /// <returns></returns>
        [HttpGet("search")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> GetManyCustomersAsync([FromQuery] string nameSeeker = null, string phoneSeeker = null, string emailSeeker = null, int? vehicleId = null, string vehiModelSeeker = null, string vehiManufSeeker = null, string visitBefore = null, string visitAfter = null)
        {
            var foundCustomers = await userService.GetManyCustomersAsync(nameSeeker, phoneSeeker, emailSeeker, vehicleId, vehiModelSeeker, vehiManufSeeker, visitBefore, visitAfter);

            try
            {
                if (foundCustomers.Count() == 0)
                    return NotFound();

                return Ok(foundCustomers);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        
        /// <summary>
        /// Change user data (employee atuh)
        /// </summary>
        /// <param name="id">user id</param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="phone"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> PutUserAsync([FromQuery] int id,
            string firstName = null,
            string lastName = null,
            string phone = null,
            string email = null)
        {
            try
            {
                var customer = await userService.PutUserAsync(id, firstName, lastName, phone, email);
                return Ok(customer);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// criterias: firstname, lastname, visitdate ; orders: null, asc, desc, 
        /// </summary>
        /// <param name="criteria">firstname/lastname/visitdate</param>
        /// <param name="order">null/asc/desc</param>
        /// <returns>List of customers sorted by criteria or default sorted by id if no params are passed</returns>
        [HttpGet("sort")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> SortCustomersAsync(string criteria, string order)
        {
            var response = await userService.SortCustomersAsync(criteria, order);
            return Ok(response);
        }


        /// <summary>
        /// Delete customer (employee auth)
        /// </summary>
        /// <param name="id">customer id</param>
        /// <returns>string with result info</returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> DeleteCustomerAsync(int id)
        {
            try
            {
                var response = await userService.DeleteCustomerAsync(id);
                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
