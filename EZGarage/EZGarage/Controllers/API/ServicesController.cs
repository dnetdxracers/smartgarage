﻿using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ServicesController : ControllerBase
    {
        private readonly IServiceService serviceService;

        public ServicesController(IServiceService serviceService)
        {
            this.serviceService = serviceService;
        }

        /// <summary>
        /// Get Service by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> GetOneAsync(int id)
        {
            try
            {
                var service = await serviceService.GetOneByIdAsync(id);
                return Ok(service);
            }
            catch (NonExistentItemWithId e)
            {
                return NotFound(e.Message);
            }

        }

        /// <summary>
        /// Get all Services
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="customerNameSeeker"></param>
        /// <param name="serviceItemId"></param>
        /// <param name="serviceItemNameSeeker"></param>
        /// <param name="vehicleId"></param>
        /// <param name="vehicleRegPlateSeeker"></param>
        /// <param name="vehicleVINSeeker"></param>
        /// <param name="visitId"></param>
        /// <param name="statusId"></param>
        /// <param name="statusNameSeeker"></param>
        /// <param name="visitDateFromDdMMMyyy"></param>
        /// <param name="visitDateToDdMMMyyy"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> GetManyAsync(int? customerId, string customerNameSeeker, int? serviceItemId,
            string serviceItemNameSeeker, int? vehicleId, string vehicleRegPlateSeeker, string vehicleVINSeeker, int? visitId,
            int? statusId,
            string statusNameSeeker, string visitDateFromDdMMMyyy, string visitDateToDdMMMyyy )
        {
            try
            {
                var foundServices = await serviceService.GetManyAsync(customerId, customerNameSeeker, serviceItemId, serviceItemNameSeeker, vehicleId, vehicleRegPlateSeeker, vehicleVINSeeker, visitId, statusId, statusNameSeeker, visitDateFromDdMMMyyy, visitDateToDdMMMyyy);
                if (foundServices.Count()!=0)
                {
                    return Ok(foundServices);
                }
                return NotFound();
                
            }
            catch (NegativeIdSelected e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Create a Service
        /// </summary>
        /// <param name="serviceDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> Post(PostServiceDto serviceDto)
        {
            try
            {
                var service = await serviceService.PostAsync(serviceDto);
                return Ok(service);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
    }
}
