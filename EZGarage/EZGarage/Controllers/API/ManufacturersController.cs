﻿using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ManufacturersController : ControllerBase
    {
        /// <summary>
        /// field for calling an instance
        /// </summary>
        private readonly IManufacturerService manufacturerService;

       
        public ManufacturersController(IManufacturerService manufacturerService)
        {
            this.manufacturerService = manufacturerService;
        }

        /// <summary>
        /// Get manufacturer by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> GetOneAsync(int id)
        {
            try
            {
                var manufacturer = await this.manufacturerService.GetManufacturerByIdAsync(id);
                return Ok(manufacturer);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get list of all manufacturers
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> GetAllAsync()
        {

            var manufacturers = await this.manufacturerService.GetAllAsync();
            return Ok(manufacturers);

        }

        /// <summary>
        /// Create new manufacturer
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> PostAsync([FromQuery] string name)
        {
            try
            {
                var newmManufacturer = await manufacturerService.PostAsync(name);

                return Ok(newmManufacturer);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Edit properties of a manufacturer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newName"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> PutAsync(int id,[FromQuery] string newName)
        {
            try
            {
                var editedManufacturer = await manufacturerService.PutAsync(id, newName);

                return Ok(editedManufacturer);
            }
            catch (NonExistentItemWithId e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
