﻿using EZGarage.Services.Contracts;
using EZGarage.Services.Dto.Visits;
using EZGarage.Web.PDFBuilder;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;


namespace EZGarage.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class VisitsController : ControllerBase
    {
        /// <summary>
        /// Visit service field
        /// </summary>
        private readonly IVisitService visitService;
        /// <summary>
        /// pdf builder field
        /// </summary>
        private readonly IPDFBuilder pdfBuilder;

        /// <summary>
        /// Ctor with visit service injection
        /// </summary>
        /// <param name="visitService"></param>
        public VisitsController(IVisitService visitService, IPDFBuilder pdfBuilder)
        {
            this.visitService = visitService;
            this.pdfBuilder = pdfBuilder;
        }

        /// <summary>
        /// Get visit by id async method
        /// </summary>
        /// <param name="id">Visit Id</param>
        /// <returns></returns>
        [Authorize(Roles = "1")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneByIdAsync(int id)
        {
            try
            {
                var visit = await visitService.GetOneByIdAsync(id);

                if (visit == null)
                {
                    return NotFound();
                }

                return Ok(visit);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

        }

        /// <summary>
        /// Convert currency async method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="convertTo"></param>
        /// <returns></returns>
        [HttpGet("forex")]
        public async Task<IActionResult> ConvertCurrencyAsync(int id,
            string convertTo)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var visit = await visitService.GetOneByIdAsync(id);

                    if (visit == null)
                    {
                        return NotFound();
                    }

                    var baseCurrency = visit.Currency.ToUpper();
                    convertTo = convertTo.ToUpper();

                    client.BaseAddress = new Uri("https://free.currconv.com");
                    var response = await client.GetAsync($"/api/v7/convert?q={convertTo}_{baseCurrency}&compact=ultra&apiKey=cd6130618024045ff512");
                    var stringResult = await response.Content.ReadAsStringAsync();

                    var obj = JObject.Parse(stringResult).ToObject<Dictionary<string, decimal>>();

                    visit.Currency = convertTo;
                    var rate = obj.First().Value;
                    var price = visit.Price * rate;

                    return Ok(visit);

                }
                catch (HttpRequestException e)
                {
                    return NotFound(e.Message);
                }
            }
        }

        /// <summary>
        /// Get exchange rate async method
        /// </summary>
        /// <returns></returns>
        [HttpGet("currency")]
        public async Task<IActionResult> GetForex(string currencyFrom,
            string currencyTo)
        {
            var from = currencyFrom.ToUpper();
            var to = currencyTo.ToUpper();

            var client = new RestClient($"https://free.currconv.com/api/v7/convert?q={from}_{to}&compact=ultra&apiKey=cd6130618024045ff512");
            var request = new RestRequest(Method.GET);
            IRestResponse response = await client.ExecuteAsync(request);

            var data = response.Content;

            return Ok(data);
        }


        /// <summary>
        /// Get all visits with optional filtration async method
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="vehicleId">Vehicle Id</param>
        /// <param name="registrationPlate">Registration plate</param>
        /// <param name="vIN">Vehicle identification number</param>
        /// <param name="model">Model name</param>
        /// <param name="modelId">Model Id</param>
        /// <param name="manufacturer">Manufacturer name</param>
        /// <param name="manufacturerId">Manufacturer Id</param>
        /// <param name="dateAfter">Date after which to filter by</param>
        /// <param name="dateBefore">Date before which to filter by</param>
        /// <param name="serviceId">Service Id</param>
        /// <param name="serviceItemId">Service item Id</param>
        /// <param name="serviceItem">Service item name</param>
        /// <param name="statusId">Status Id</param>
        /// <param name="status">Status name</param>
        [HttpGet("search")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> GetAllAsync([FromQuery] int? customerId,
            int? vehicleId,
            string registrationPlate,
            string vIN,
            string model,
            int? modelId,
            string manufacturer,
            int? manufacturerId,
            string dateAfter,
            string dateBefore,
            int? serviceId,
            int? serviceItemId,
            string serviceItem,
            int? statusId,
            string status)
        {
            try
            {
                var visits = await this.visitService.GetAllAsync(customerId,
                                vehicleId,
                                registrationPlate,
                                vIN,
                                model,
                                modelId,
                                manufacturer,
                                manufacturerId,
                                dateAfter,
                                dateBefore,
                                serviceId,
                                serviceItemId,
                                serviceItem,
                                statusId,
                                status);

                return Ok(visits);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        /// <summary>
        /// Post async method to create a new visit 
        /// </summary>
        /// <param name="visitDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> PostAsync([FromBody] PostVisitDto visitDto)
        {
            try
            {

                var newVisit = await visitService.PostAsync(visitDto);
                return Ok(newVisit);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
    }
}
