﻿using AutoMapper;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto.ServiceItems;
using EZGarage.Web.Models.ServiceItemModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ServiceItemsController : ControllerBase
    {
        private readonly IServiceItemService serviceItemService;
        private readonly IMapper mapper;


        public ServiceItemsController(IServiceItemService serviceItemService,
            IMapper mapper)
        {
            this.serviceItemService = serviceItemService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Get service item by id
        /// </summary>
        /// <param name="id">Service item Id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetOneByIdAsync(int id)
        {
            try
            {
                var serviceItem = await serviceItemService.GetOneByIdAsync(id);

                return Ok(serviceItem);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get list of all service items
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAllAsync()
        {
            try
            {
                var serviceItems = await serviceItemService.GetAllAsync();

                return Ok(serviceItems);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get list of service items based on search criteria
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="minPrice">Minimum price</param>
        /// <param name="maxPrice">Maximum price</param>
        /// <returns></returns>
        [HttpGet("search")]
        [AllowAnonymous]
        public async Task<IActionResult> GetManyAsync([FromQuery] string name,
            decimal? minPrice,
            decimal? maxPrice)
        {
            try
            {
                var foundSerivceItems = await serviceItemService.GetManyAsync(name, minPrice, maxPrice);

                if (foundSerivceItems.Count() != 0)
                    return Ok(foundSerivceItems);
                else
                {
                    return NotFound("No match");
                }
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Create service item
        /// </summary>
        /// <param name="itemDto">Post service item Dto</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> PostAsync([FromBody] PostServiceItemDto itemDto)
        {
            try
            {
                var newServiceItem = await serviceItemService.PostAsync(itemDto);

                return Ok(newServiceItem);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Edit properties of a service item
        /// </summary>
        /// <param name="serviceItemInputViewModel">Service item input view model</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> PutAsync(ServiceItemInputViewModel serviceItemInputViewModel)
        {
            try
            {
                var item = this.mapper.Map<ServiceItemDto>(serviceItemInputViewModel);

                var serviceItem = await this.serviceItemService.PutAsync(item);

                return Ok(serviceItem);
            }
            catch (NonExistentItemWithId e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Delete service item
        /// </summary>
        /// <param name="id">Service item Id</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                await serviceItemService.DeleteAsync(id);
                return Ok();
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
