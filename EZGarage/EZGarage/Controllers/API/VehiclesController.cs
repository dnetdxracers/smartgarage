﻿using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class VehiclesController : ControllerBase
    {
        /// <summary>
        /// Field
        /// </summary>
        private readonly IVehicleService vehicleService;

        /// <summary>
        /// Ctor with service injection
        /// </summary>
        /// <param name="vehicleService"></param>
        public VehiclesController(IVehicleService vehicleService)
        {
            this.vehicleService = vehicleService;
        }

        /// <summary>
        /// Get vehicle by id async method
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> GetOneAsync(int id)
        {
            var vehicleDto = await this.vehicleService.GetOneByIdAsync(id);
            if (vehicleDto == null)
                return NotFound();

            return Ok(vehicleDto);
        }


        /// <summary>
        /// Get many vehicles async method
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="customer"></param>
        /// <param name="vehicleTypeId"></param>
        /// <param name="vehicleType"></param>
        /// <param name="modelId"></param>
        /// <param name="model"></param>
        /// <param name="manufacturerId"></param>
        /// <param name="manufacturer"></param>
        /// <param name="year"></param>
        /// <param name="regPlate"></param>
        /// <returns></returns>
        [HttpGet("search")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> GetManyAsync([FromQuery] int customerId,
            string customer,
            int vehicleTypeId,
            string vehicleType,
            int modelId,
            string model,
            int manufacturerId,
            string manufacturer,
            int year,
            string regPlate)
        {
            
            var foundVehicleDtos = await this.vehicleService.GetAllAsync(customerId, customer, vehicleTypeId, vehicleType, modelId, model, manufacturerId, manufacturer, year, regPlate);
            
            if (foundVehicleDtos.Count() != 0)
                return Ok(foundVehicleDtos);
            
            return NotFound();

        }

        /// <summary>
        /// Post method to create a vehicle
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> PostAsync([FromBody] PostVehicleDto model)
        { 
            try
            {
                var vehicle = await vehicleService.PostAsync(model);
                return Ok(vehicle);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Put method to update vehicle registration plate by id
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="newRegistrationPlate"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> PutAsync([FromQuery] int vehicleId, string newRegistrationPlate)
        {
            try
            {
                var vehicle = await vehicleService.PutAsync(vehicleId, newRegistrationPlate);
                return Ok(vehicle);
            }
            catch (Exception e) 
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Delete vehicle by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                var response = await vehicleService.DeleteAsync(id);
                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
