﻿using EZGarage.Services.Contracts;
using EZGarage.Services.Util.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MailController : ControllerBase
    {
        /// <summary>
        /// Mail service field
        /// </summary>
        private readonly IMailService mailService;
        
        /// <summary>
        /// Ctor with mail service injection
        /// </summary>
        /// <param name="mailService"></param>
        public MailController(IMailService mailService)
        {
            this.mailService = mailService;
        }

        /// <summary>
        /// Post method to send email
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("send")]
        public async Task<IActionResult> SendMail([FromForm] MailRequest request)
        {
            try
            {
                await mailService.SendEmailAsync(request);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
    }
}
