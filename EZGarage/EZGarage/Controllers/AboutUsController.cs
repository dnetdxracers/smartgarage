﻿
using Microsoft.AspNetCore.Mvc;

namespace EZGarage.Web.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AboutUsController : Controller
    {
        /// <summary>
        /// Index endpoint
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {

            return View();
        }
    }
}
