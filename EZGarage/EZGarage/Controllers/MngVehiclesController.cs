﻿using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using EZGarage.Web.Helpers;
using EZGarage.Web.Models.MngVehiclesModels;
using EZGarage.Web.Models.Model_Manufacturer_VehType;
using EZGarage.Web.Models.VehicleViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("mngvehicles")]
    public class MngVehiclesController : Controller
    {
        /// <summary>
        /// Fields
        /// </summary>
        private readonly IVehicleService vehicleService;
        private readonly IManufacturerService manufacturerService;
        private readonly IModelService modelService;
        private readonly IVehicleTypeService vehicleTypeService;

        /// <summary>
        /// Ctor with services injections
        /// </summary>
        /// <param name="vehicleService"></param>
        /// <param name="manufacturerService"></param>
        /// <param name="modelService"></param>
        /// <param name="vehicleTypeService"></param>
        public MngVehiclesController(IVehicleService vehicleService,
            IManufacturerService manufacturerService,
            IModelService modelService,
            IVehicleTypeService vehicleTypeService)
        {
            this.vehicleService = vehicleService;
            this.manufacturerService = manufacturerService;
            this.modelService = modelService;
            this.vehicleTypeService = vehicleTypeService;
        }

        /// <summary>
        /// Index endpoint
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="customerName">Customer Name</param>
        /// <param name="vehicleTypeId">Vehicle type Id</param>
        /// <param name="vehicleType">Vehicle type</param>
        /// <param name="modelId">Model Id</param>
        /// <param name="model">Model</param>
        /// <param name="manufacturerId">Manufacturer Id</param>
        /// <param name="manufacturer">Manufacturer</param>
        /// <param name="year">Year</param>
        /// <param name="regPlate">Registration plate</param>
        /// <returns></returns>
        [EzAuhtorize(Role = "employee")]
        [Route("index")]
        [HttpGet]
        public async Task<IActionResult> Index(int? customerId = null,
            string customerName = null,
            int? vehicleTypeId = null,
            string vehicleType = null,
            int? modelId = null,
            string model = null,
            int? manufacturerId = null,
            string manufacturer = null,
            int? year = null,
            string regPlate = null)
        {
            var matchingVehicles = await vehicleService.GetAllAsync(customerId, customerName, vehicleTypeId, vehicleType,
                modelId, model, manufacturerId, manufacturer, year, regPlate);

            var vehiclesVM = matchingVehicles.Select(ve => new VehicleViewModel(ve));

            var indexVM = new MngVehiclesIndexVM(null, vehiclesVM, null, null);

            return View(indexVM);
        }

        /// <summary>
        /// Get vehicle details method
        /// </summary>
        /// <param name="id">Vehicle Id</param>
        /// <returns></returns>
        [Route("vehicle/details/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetVehicleDetails(int id)
        {
            var vehicle = await vehicleService.GetOneByIdAsync(id);
            var vehicleVM = new VehicleViewModel(vehicle);

            var indexVM = new MngVehiclesIndexVM(vehicleVM, null, "details", null);

            return View("index", indexVM);
        }

        /// <summary>
        /// Edit vehicle endpoint
        /// </summary>
        /// <param name="id"></param>
        /// <param name="regPlate"></param>
        /// <returns></returns>
        [Route("vehicle/edit/{id}")]
        [HttpPost]
        public async Task<IActionResult> EditVehicle(int id, string regPlate)
        {
            if (regPlate == null)
            {
                var vehicle = await vehicleService.GetOneByIdAsync(id);
                var vehicleVM = new VehicleViewModel(vehicle);

                var indexVM = new MngVehiclesIndexVM(vehicleVM, null, "edit", null);

                return View("index", indexVM);
            }
            else
            {
                var vehicle = await vehicleService.PutAsync(id, regPlate);
                var vehicleVM = new VehicleViewModel(vehicle);

                var indexVM = new MngVehiclesIndexVM(vehicleVM, null, "details", "editsuccess");

                return View("index", indexVM);
            }

        }

        /// <summary>
        /// Delete vehicle endpoint
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("vehicle/deletevehicleview/{id}")]
        [HttpGet]
        public async Task<IActionResult> DeleteVehicleView(int id)
        {
            var vehicle = await vehicleService.GetOneByIdAsync(id);
            var vehicleVM = new VehicleViewModel(vehicle);

            var indexVM = new MngVehiclesIndexVM(vehicleVM, null, "delete", "confirmdelete");

            return View("index", indexVM);
        }

        /// <summary>
        /// Delete vehicle confirmation endpoint
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("/DeleteVehicleConfirmed")]
        public async Task<IActionResult> DeleteVehicleConfirmed(int id)
        {
            var result = await vehicleService.DeleteAsync(id);

            var vehicles = await vehicleService.GetAllAsync();

            var vehiclesVM = vehicles.Select(ve => new VehicleViewModel(ve));
            var indexVM = new MngVehiclesIndexVM(null, vehiclesVM, null, "deletesuccess");

            return View("index", indexVM);
        }

        /// <summary>
        /// Create new vehicle model endpoint
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> CreateNewModel()
        {
            var registerVehicleNewVM = await this.GetRegisterNewModelVMAsync(null);

            return View("RegisterNewModel", registerVehicleNewVM);
        }

        /// <summary>
        /// Register new model endpoint
        /// </summary>
        /// <param name="modelName"></param>
        /// <param name="manufacturerId"></param>
        /// <returns></returns>
        [HttpPost("RegisterNewModel")]
        public async Task<IActionResult> RegisterNewModel(string modelName, int manufacturerId)
        {
            ModelDto model;
            try
            {
                model = await modelService.PostAsync(modelName, manufacturerId);
            }
            catch (Exception e)
            {
                var registerNewModelVM = await this.GetRegisterNewModelVMAsync(e.Message);

                return View("RegisterNewModel", registerNewModelVM);
            }

            var vehicles = await vehicleService.GetAllAsync();
            var vehicleVMs = vehicles.Select(ve => new VehicleViewModel(ve));

            var indexVM = new MngVehiclesIndexVM(null, vehicleVMs, $"{model.Name} {model.Manufacturer}", "modelRegistered");

            return View("index", indexVM);
        }

        /// <summary>
        /// Register new manufacturer endpoint
        /// </summary>
        /// <param name="manufacturerName"></param>
        /// <returns></returns>
        [HttpPost("RegisterNewManufacturer")]
        public async Task<IActionResult> RegisterNewManufacturer(string manufacturerName)
        {
            var manufacturer = await manufacturerService.PostAsync(manufacturerName);

            if (manufacturer == null)
            {
                var registerNewModelVMFail = await this.GetRegisterNewModelVMAsync("Already existing manufacturer");

                return View("RegisterNewModel", registerNewModelVMFail);
            }

            var registerNewModelVMsucc = await this.GetRegisterNewModelVMAsync("manufSuccess");

            return View("RegisterNewModel", registerNewModelVMsucc);
        }

        /// <summary>
        /// Private method to assist with registering new model
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [HttpGet]
        private async Task<RegisterNewModelVM> GetRegisterNewModelVMAsync(string message)
        {
            var models = await modelService.GetAllAsync();
            var manufacturers = await manufacturerService.GetAllAsync();

            var registerNewModelVM = new RegisterNewModelVM(models.Select(mo => new ModelVM(mo)).ToList(), manufacturers.Select(ma => new ManufacturerVM(ma)).ToList(), message);

            return registerNewModelVM;
        }

        /// <summary>
        /// Get vehicles for given customer endpoint
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns></returns>
        [HttpGet("vehicles/{customerId}")]
        [EzAuhtorize(Role = "employee")]
        public async Task<IActionResult> GetCustomerVehicles(int customerId)
        {
            try
            {
                var matchingVehicles = await this.vehicleService.GetVehiclesByCustomerIdAsync(customerId);

                var vehiclesVM = matchingVehicles.Select(ve => new VehicleViewModel(ve));
                var result = new MngVehiclesIndexVM(null, vehiclesVM, null, null);

                return View("Index", result);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get manufacturer models endpoint
        /// </summary>
        /// <param name="id">Manufacturer Id</param>
        /// <returns></returns>
        [HttpGet("manufacturermodels/{id}")]
        public async Task<IActionResult> ManufacturerModels(int? id)
        {
            var models = await this.modelService.GetAllAsync();

            //return Json(models.Where(m => m.ManufacturerId == id).Select(m => new SelectListItem { Text = m.Name, Value = m.Id.ToString()}));
            return Json(models.Where(m => m.ManufacturerId == id).Select(m => new SelectListItem { Text = m.Id.ToString(), Value = m.Name }));
        }

        /// <summary>
        /// Get method to obtain vehicle types by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getvehicletypes/{id}")]
        public async Task<IActionResult> GetVehicleTypes(int id)
        {
            var vehicleTypes = await this.vehicleTypeService.GetVehicleTypeByIdAsync(id);

            return Json(vehicleTypes);
        }
    }
}


