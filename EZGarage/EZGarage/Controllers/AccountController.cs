﻿using AutoMapper;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using EZGarage.Web.Models.UserViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AccountController : Controller
    {
        /// <summary>
        /// Fields
        /// </summary>
        private readonly IUserService userService;
        private readonly IMapper mapper;

        /// <summary>
        /// Ctor with service injections
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="mapper"></param>
        public AccountController(IUserService userService,
            IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Index endpoint
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Login async method
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        [HttpPost()]
        public async Task<IActionResult> LoginAsync(string email, string password)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await this.userService.GetUserByEmailPassAsync(email, password);
                    if (user == null)
                    {
                        this.HttpContext.Session.SetString("logintry", "unsuccessful");
                        return View("Index");
                    }
                    this.HttpContext.Session.Clear();

                    var userModel = new UserLoginModel(user);

                    this.HttpContext.Session.SetString("user", userModel.Email);
                    this.HttpContext.Session.SetInt32("id", userModel.Id);
                    this.HttpContext.Session.SetString("role", userModel.Role);


                    //emplyees login with employee view mode by default
                    if (userModel.Role == "employee")
                        this.HttpContext.Session.SetString("viewMode", "employee");

                    return RedirectToAction("Index", "Home");
                }
                return View();
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Fogotten password get method
        /// </summary>
        /// <returns></returns>
        [HttpGet("forgottenpassword")]
        public IActionResult ForgottenPassword()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Forgotten password post method to send email to user with option to change password
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost("forgottenpassword")]
        public async Task<IActionResult> ForgottenPassword(string email)
        {
            try
            {
                var id = this.userService.GetUserIdByEmailAsync(email).Result;

                var user = await this.userService.SendEmailToResetPasswordAsync(id);

                TempData["IsSent"] = "An EZMail with reset password instructions will be in your mail shortly!";

                return RedirectToAction(nameof(ForgottenPassword));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Reset password get method
        /// </summary>
        /// <returns></returns>
        [HttpGet("resetpassword")]
        public IActionResult ResetPassword()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Reset password post method
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="activationCode">Activation code as per recovery email</param>
        /// <param name="newPassword">New password</param>
        /// <returns></returns>
        [HttpPost("resetpassword")]
        public async Task<IActionResult> ResetPassword(string email, string activationCode, string newPassword)
        {
            try
            {
                var userId = await this.userService.GetUserIdByEmailAsync(email);

                //var createUserReset = this.userService.CreateNewUserResetAsync(userId);

                var userResetDto = new UserResetDto()
                {
                    Email = email,
                    TemporaryPassword = activationCode,
                    UserId = userId,
                    ExpiryDate = DateTime.Now.AddHours(3)
                };

                var verify = await this.userService.VerifyUserResetAsync(userResetDto);

                this.userService.UpdatePassword(userId, newPassword);

                TempData["IsReset"] = "Your EZPassword has been successfully updated!";

                return RedirectToAction(nameof(ResetPassword));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Logout method
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Logout()
        {
            this.HttpContext.Session.Clear();
            return View();
        }

        /// <summary>
        /// Register method
        /// </summary>
        /// <returns></returns>
        [HttpGet("register")]
        public IActionResult Register()
        {
            return View();
        }

    }
}

