﻿using AutoMapper;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto.ServiceItems;
using EZGarage.Web.Helpers;
using EZGarage.Web.Models.ServiceItemModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("services")]
    public class ServicesController : Controller
    {
        /// <summary>
        /// Service item service and mapper fields
        /// </summary>
        private readonly IServiceItemService serviceItemService;
        private readonly IMapper mapper;

        /// <summary>
        /// Ctor with injections
        /// </summary>
        /// <param name="serviceItemService">Service item service</param>
        /// <param name="mapper">Automapper</param>
        public ServicesController(IServiceItemService serviceItemService,
            IMapper mapper)
        {
            this.serviceItemService = serviceItemService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Main index page get method
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            try
            {
                var services = await this.serviceItemService.GetAllAsync();

                var result = new ServiceItemsViewModel(services);

                return View(result);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Service item details get method
        /// </summary>
        /// <param name="id">Service item Id</param>
        /// <returns></returns>
        [HttpGet("details/{id}")]
        [EzAuhtorize(Role = "employee")]
        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var serviceItem = await serviceItemService.GetOneByIdAsync(id);

                var result = new ServiceItemViewModel(serviceItem);

                return View(result);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Search method
        /// </summary>
        /// <param name="name">Service item name</param>
        /// <returns></returns>
        //[Authorize(Roles = "1")]
        [HttpGet("search")]
        public async Task<IActionResult> Search([FromQuery] string name)
        {
            try
            {
                var foundSerivceItems = await serviceItemService.GetManyAsync(name, null, null);

                if (foundSerivceItems.Count() != 0)
                {
                    ICollection<ServiceItemViewModel> result = new List<ServiceItemViewModel>();
                    foreach (var item in foundSerivceItems)
                    {
                        result.Add(new ServiceItemViewModel(item));
                    }
                    return View(result);
                }
                else
                {
                    return NotFound("No match");
                }
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get method to create service item
        /// </summary>
        /// <returns></returns>
        [HttpGet("create")]
        [EzAuhtorize(Role = "employee")]
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Post method for creating a new service item
        /// </summary>
        /// <param name="serviceItem"></param>
        /// <returns></returns>
        [HttpPost("create")]
        [EzAuhtorize(Role = "employee")]
        public async Task<IActionResult> Create(ServiceItemViewModel serviceItem)
        {
            try
            {
                var serviceItemDto = this.mapper.Map<PostServiceItemDto>(serviceItem);
                var result = await this.serviceItemService.PostAsync(serviceItemDto);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get method to edit an existing service item
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("edit/{id}")]
        [EzAuhtorize(Role = "employee")]
        public async Task<IActionResult> Edit([FromRoute] int id)
        {
            try
            {
                var serviceItem = await this.serviceItemService.GetOneByIdAsync(id);

                var item = this.mapper.Map<ServiceItemViewModel>(serviceItem);

                return View("Edit", item);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Post item for editing an existing service item
        /// </summary>
        /// <param name="serviceItemInputViewModel"></param>
        /// <returns></returns>
        [HttpPost("edit/{id}")]
        [EzAuhtorize(Role = "employee")]
        public async Task<IActionResult> Edit(ServiceItemInputViewModel serviceItemInputViewModel)
        {
            try
            {
                var item = this.mapper.Map<ServiceItemDto>(serviceItemInputViewModel);

                var serviceItem = await this.serviceItemService.PutAsync(item);

                var result = new ServiceItemViewModel(serviceItem);
                result.IsSuccessful = true;

                return View(result);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get method to delete service item by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("delete/{id}")]
        [EzAuhtorize(Role = "employee")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            try
            {
                var serviceItem = await this.serviceItemService.GetOneByIdAsync(id);

                var item = this.mapper.Map<ServiceItemViewModel>(serviceItem);

                return View(item);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Post method for deleting an existing item by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("delete/{id}")]
        [EzAuhtorize(Role = "employee")]
        public async Task<IActionResult> DeleteForm([FromRoute] int id)
        {
            try
            {
                await this.serviceItemService.DeleteAsync(id);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
