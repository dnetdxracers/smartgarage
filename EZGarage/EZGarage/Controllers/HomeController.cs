﻿using EZGarage.Services.Contracts;
using EZGarage.Web.PDFBuilder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EZGarage.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : Controller
    {
        /// <summary>
        /// Fields
        /// </summary>
        private readonly ILogger<HomeController> _logger;
        private readonly IVisitService visitService;
        private readonly IPDFBuilder pdfBuilder;

        /// <summary>
        /// Ctor with service injections
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="visitService"></param>
        /// <param name="pdfBuilder"></param>
        public HomeController(ILogger<HomeController> logger, IVisitService visitService,
            IPDFBuilder pdfBuilder)
        {
            _logger = logger;
            this.visitService = visitService;
            this.pdfBuilder = pdfBuilder;
        }

        /// <summary>
        /// Post method to update view depending on role of logged user
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ChangeView()
        {
            if (this.HttpContext.Session.GetString("viewMode") != "employee")
                this.HttpContext.Session.SetString("viewMode", "employee");
            else
                this.HttpContext.Session.SetString("viewMode", "normal");

            return View("index");
        }

        /// <summary>
        /// Error page view controller
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/Home/HandleError/{code:int}")]
        [HttpGet]
        public IActionResult HandleError(int code)
        {
            ViewData["ErrorCode"] = $"{code}";
            return View("~/Views/Shared/Error.cshtml");
        }

        /// <summary>
        /// Index endpoint
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Privacy endpoint
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Privacy()
        {
            return View();
        }

    }
}
