﻿
function myFunction() {
    var input, filter, tableBody, tr, td, i, t;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    tableBody = document.getElementById("myTableBody");
    tr = tableBody.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        var filtered = false;
        var tds = tr[i].getElementsByTagName("td");
        for (t = 0; t < tds.length; t++) {
            var td = tds[t];
            if (td) {
                if (td.innerText.toUpperCase().indexOf(filter) > -1) {
                    filtered = true;
                }
            }
        }
        if (filtered === true) {
            tr[i].style.display = '';
        }
        else {
            tr[i].style.display = 'none';
        }
    }
}

function sortTable(n) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("myTableBody");
    switching = true;
    dir = "asc";
    while (switching) {
        switching = false;
        rows = table.getElementsByTagName("TR");
        for (i = 0; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            var cmpX = isNaN(parseInt(x.innerHTML)) ? x.innerHTML.toLowerCase() : parseInt(x.innerHTML);
            var cmpY = isNaN(parseInt(y.innerHTML)) ? y.innerHTML.toLowerCase() : parseInt(y.innerHTML);
            cmpX = (cmpX == '-') ? 0 : cmpX;
            cmpY = (cmpY == '-') ? 0 : cmpY;
            if (dir == "asc") {
                if (cmpX > cmpY) {
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (cmpX < cmpY) {
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}
