﻿function myFunction() {
    var input, filter, tableBody, tr, td, i, t;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    tableBody = document.getElementById("myTableBody");
    tr = tableBody.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        var filtered = false;
        var tds = tr[i].getElementsByTagName("td");
        for (t = 0; t < tds.length; t++) {
            var td = tds[t];
            if (td) {
                if (td.innerText.toUpperCase().indexOf(filter) > -1) {
                    filtered = true;
                }
            }
        }
        if (filtered === true) {
            tr[i].style.display = '';
        }
        else {
            tr[i].style.display = 'none';
        }
    }
}