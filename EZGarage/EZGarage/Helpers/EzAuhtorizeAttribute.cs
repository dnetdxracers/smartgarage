﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Web.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class EzAuhtorizeAttribute :  Attribute, IAuthorizationFilter
    {
        public string Role { get; set; }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var sessionUsername = context.HttpContext.Session.GetString("user");
            var sessionRole = context.HttpContext.Session.GetString("role");

            if (sessionRole != this.Role || string.IsNullOrEmpty("user"))
                context.Result = new UnauthorizedResult();
        }
        
    }
}
