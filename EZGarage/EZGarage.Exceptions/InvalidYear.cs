﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class InvalidYear : Exception
    {
        public InvalidYear()
        {

        }

        public InvalidYear(int year)
            :base(String.Format("Vehicle year {0} cannot be in the future", year))
        {

        }
    }
}
