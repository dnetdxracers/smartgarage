﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class UnsupportedCurrency : Exception
    {
        public UnsupportedCurrency()
        {

        }

        public UnsupportedCurrency(string currency)
            : base(String.Format("Unsupported currency: {0}", currency))
        {
        }
    }
}
