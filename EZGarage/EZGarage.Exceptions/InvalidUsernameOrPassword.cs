﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class InvalidUsernameOrPassword : Exception
    {
        public InvalidUsernameOrPassword()
            : base("Invalid username or password")
        {

        }

    }
}
