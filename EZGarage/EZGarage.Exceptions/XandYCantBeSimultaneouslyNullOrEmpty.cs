﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class XandYCantBeSimultaneouslyNullOrEmpty : Exception
    {
        public XandYCantBeSimultaneouslyNullOrEmpty()
        {

        }

        public XandYCantBeSimultaneouslyNullOrEmpty(string item1, string item2)
            : base(String.Format("At leas one of the parameters for {0} and {1} must be valid", item1, item2))
        {

        }
    }
}
