﻿using System;

namespace EZGarage.Exceptions
{
    public class InvalidStringLength : Exception
    {
        public InvalidStringLength()
        {

        }

        public InvalidStringLength(string item, int min, int max)
            : base(String.Format("{0} length must be between {1} and {2} symbols", item, min, max))
        {

        }
    }
}
