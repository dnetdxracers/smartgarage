﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class NonExistentItemWithProperty : Exception
    {
        public NonExistentItemWithProperty()
        {

        }

        public NonExistentItemWithProperty(string item, string property, string input)
            : base(String.Format("{0} with {1} {2} does not exist", item, property, input))
        {

        }
    }
}
