﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class NameNullOrEmpty : Exception
    {
        public NameNullOrEmpty()
        {

        }

        public NameNullOrEmpty(string targetObject)
            : base($"{targetObject} name cannot be null or empty")
        {

        }
    }
}

