﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class NonExistentItemWithId : Exception
    {
        public NonExistentItemWithId()
        {

        }

        public NonExistentItemWithId(string classObject, int id)
            : base(String.Format("{0} with id {1} does not exist", classObject, id))
        {

        }

        public NonExistentItemWithId(string classObject)
            : base(String.Format("Invalid id fur {}"))
        {

        }
    }
}
