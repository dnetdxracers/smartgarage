﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class InvalidRegPlate : Exception
    {
        public InvalidRegPlate()
        {

        }

        public InvalidRegPlate(string registration)
            : base(String.Format("Invalid registration plate {0}, format must be: A(A)DDDDAA, where A is a character of the Latin alphabet and D is a digit", registration))
        {

        }
    }
}
