﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class InvalidEmailFormat : Exception
    {
        public InvalidEmailFormat()
        {

        }

        public InvalidEmailFormat(string email)
            : base($"{email} has invalid email format")
        {

        }
    }
}
