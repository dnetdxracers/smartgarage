﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class NegativeIdSelected : Exception
    {
        public NegativeIdSelected()
            : base("Selected Id can't be negative")
        {

        }

    }
}

