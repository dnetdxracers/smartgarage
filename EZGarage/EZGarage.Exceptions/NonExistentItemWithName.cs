﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class NonExistentItemWithName : Exception
    {
        public NonExistentItemWithName()
        {
                
        }

        public NonExistentItemWithName(string classObject, string name)
            : base(String.Format("{0} with name {1} does not exist", classObject, name))
        {

        }
    }
}
