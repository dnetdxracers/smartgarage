﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class NegativePriceException : Exception
    {
        public NegativePriceException()
        {

        }

        public NegativePriceException(string targetObject)
            : base($"{targetObject} price cannot be negative")
        {

        }
    }
}

