﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class InvalidIntSet : Exception
    {
        public InvalidIntSet()
        {

        }

        public InvalidIntSet(IEnumerable<int> set)
            : base($"The set of items provided {string.Join(", ", set)} contains unrecognized members")
        {

        }
    }
}
