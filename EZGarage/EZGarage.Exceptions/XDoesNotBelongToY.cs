﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class XDoesNotBelongToY : Exception
    {
        public XDoesNotBelongToY()
        {

        }

        public XDoesNotBelongToY(string child, int childId, string parent, int parentId)
            : base(String.Format("{0} with id {1} does not belong to {2} with id {3}", child, childId, parent, parentId))
        {

        }
    } 
}
