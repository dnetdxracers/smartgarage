﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class NonExistentItemContaining : Exception
    {
        public NonExistentItemContaining()
        {

        }

        public NonExistentItemContaining(string classObject, string objectPropertyName, string keyWord)
            : base(String.Format("Nonexistent {0} containing {2} in {1} property", classObject, objectPropertyName, keyWord))
        {
        }
    }
}
