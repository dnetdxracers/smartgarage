﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class AlreadyExisting : Exception
    {
        public AlreadyExisting(string className, string item)
            : base($"{className} {item} already exists in Database")
        {
        }

    }
}
