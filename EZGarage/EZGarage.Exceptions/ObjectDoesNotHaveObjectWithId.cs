﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Exceptions
{
    public class ObjectDoesNotHaveObjectWithId : Exception
    {
        public ObjectDoesNotHaveObjectWithId()
        {

        }

        public ObjectDoesNotHaveObjectWithId(string objectDoesNotHave, string objectWith, int id)
            : base(String.Format($"{0} does not have {1} with Id {2}", objectDoesNotHave, objectWith, id))
        {

        }
    }
}
