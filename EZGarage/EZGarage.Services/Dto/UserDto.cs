﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EZGarage.Services.Dto
{
    public class UserDto
    {
        public UserDto(User user)
        {
            this.Id = user.Id;

            if (user.RoleId == 1)
                this.Role = "Employee";
            else if (user.RoleId == 2)
                this.Role = "Customer";
            else
                this.Role = "Uncategorized";

            this.Name = user.FirstName + " " + user.LastName;
            this.Phone = user.Phone;
            this.Email = user.Email;
        }

        public int Id { get; set; }
        public string Role { get; }
        public string Name { get; }
        public string Phone { get; }
        public string Email { get; }
    }
}
