﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EZGarage.Services.Dto
{
    public class CustomerDto
    {
        public CustomerDto(User customer)
        {
            this.Id = customer.Id;
            this.Name = customer.FirstName + " " + customer.LastName;
            this.Phone = customer.Phone;
            this.Email = customer.Email;
            this.Vehicles = customer.Vehicles.Count != 0 
                ? customer.Vehicles.Select(ve => new VehicleDto(ve)).ToList() 
                : new List<VehicleDto>();
        }

        public int Id { get; }
        public string Name { get; }
        public string Phone { get; }
        public string Email { get; }
        public List<VehicleDto> Vehicles { get; }

    }
}
