﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Services.Dto
{
    public class PostUserDtoResponse
    {
        public PostUserDtoResponse(User user)
        {
            this.Id = user.Id;

            if (user.RoleId == 1)
                this.Role = "Employee";
            else if (user.RoleId == 2)
                this.Role = "Customer";
            else
                this.Role = "Uncategorized";

            this.Name =$"{user.Id} {user.FirstName} {user.LastName}";
            this.Phone = user.Phone;
            this.Email = user.Email;
        }

        public int Id { get; set; }
        public string Role { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
