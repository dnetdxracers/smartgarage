﻿using EZGarage.Data.Models;
using EZGarage.Services.Dto.Services;
using System.Collections.Generic;
using System.Linq;

namespace EZGarage.Services.Dto.Visits
{
    public class VisitDtoDetailed
    {
        
        /// <summary>
        /// Visit ctor with visit injection
        /// </summary>
        /// <param name="visit"></param>
        public VisitDtoDetailed(Visit visit)
        {
            this.Id = visit.Id;
            this.CustomerId = visit.CustomerId;
            this.CustomerName = $"{visit.Customer.FirstName} {visit.Customer.LastName}";
            this.VehicleId = visit.VehicleId;
            this.VehicleMakeModel = $"{visit.Vehicle.Model.Manufacturer.Name} {visit.Vehicle.Model.Name}";
            this.ArrivalDate = visit.ArrivalDate.ToString("dd-MMM-yyyy");
            this.RegistrationPlate = visit.Vehicle.RegistrationPlate;

            this.Services = visit.Services.Select(se => new ServiceDto(se)).ToList();
            this.ReadyDate = visit.Services.Select(se => se.ReadyDate).Max().ToString("dd-MMM-yyyy");

            this.TotalPrice = visit.TotalPrice;
            this.Currency = visit.Currency;
        }
        /// <summary>
        /// Visit Id property
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Customer Id property
        /// </summary>
        public int CustomerId { get; set; }
        
        /// <summary>
        /// Customer name property
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Visit arrival date proeprty
        /// </summary>
        public string ArrivalDate { get; set; }

        /// <summary>
        /// Vehicle Id property
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// Vehicle make and model property
        /// </summary>
        public string VehicleMakeModel { get; set; }

        /// <summary>
        /// Registration plate property
        /// </summary>
        public string RegistrationPlate { get; set; }

        /// <summary>
        /// Services property
        /// </summary>
        public IEnumerable<ServiceDto> Services { get; set; }

        /// <summary>
        /// Total price property
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// Currency property
        /// </summary>
        public string Currency { get; set; }

        public string ReadyDate { get; set; }

    }

   

}
