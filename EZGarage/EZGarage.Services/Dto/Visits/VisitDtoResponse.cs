﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Services.Dto.Visits
{
    public class VisitDtoResponse
    {
        
        public VisitDtoResponse(Visit visit)
        {
            this.Id = visit.Id;
            this.VehicleId = visit.VehicleId;
            this.CustomerId = visit.CustomerId;
            this.ArrivalDate = visit.ArrivalDate.ToString("dd-MMM-yyy");
            this.ReadyDate = visit.ReadyDate.ToString("dd-MMM-yyy");
            this.TotalPrice = visit.TotalPrice;
            this.Currency = visit.Currency;
        }

        public int Id { get; set; }
        /// <summary>
        /// Vehicle Id property
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// Customer Id property
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Arrival date property
        /// </summary>
        public string ArrivalDate { get; set; }

        public string ReadyDate { get; set; }

        /// <summary>
        /// Total price of the visit
        /// </summary>
        public decimal TotalPrice { get; set; }

        public string Currency { get; set; }
    }
}
