﻿using System;

namespace EZGarage.Services.Dto.Visits
{
    public class PostVisitDto
    {
        /// <summary>
        /// Empty ctor
        /// </summary>
        public PostVisitDto()
        {

        }
        /// <summary>
        /// Post visit Dto
        /// </summary>
        /// <param name="vehicleId">Vehicle Id</param>
        /// <param name="date">Arrival date</param>
        /// <param name="serviceItemIds">Service item Ids</param>
        /// <param name="customerId">Customer Id</param>
        /// <param name="serviceItemFinishDates">Finish dates for corresponding service items</param>
        public PostVisitDto(int vehicleId,
            int customerId,
            string date,
            int[]serviceItemIds,
            string[]serviceItemFinishDates)
        {
            this.VehicleId = vehicleId;
            this.CustomerId = customerId;
            this.ArrivalDate = date;
            this.ServiceIteIds = serviceItemIds;
            this.ServiceItemFinishDates = serviceItemFinishDates;
        }
        
        /// <summary>
        /// Vehicle Id property
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// Customer Id property
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Arrival date property
        /// </summary>
        public string ArrivalDate { get; set; }

        /// <summary>
        /// Array of service item ids property
        /// </summary>
        public int[] ServiceIteIds { get; set; }

        /// <summary>
        /// Array of service item finish dates property
        /// </summary>
        public string[] ServiceItemFinishDates { get; set; }

    }
}
