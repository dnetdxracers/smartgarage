﻿using EZGarage.Data.Models;
using EZGarage.Services.Dto.Services;
using System.Collections.Generic;
using System.Linq;

namespace EZGarage.Services.Dto.Visits
{
    public class VisitDto
    {
        
        /// <summary>
        /// Ctor with visit entity input
        /// </summary>
        /// <param name="visit"></param>
        public VisitDto(Visit visit)
        {
            this.Id = visit.Id;
            this.VehicleId = visit.VehicleId;
            this.ArrivalDate = visit.ArrivalDate.ToString("dd-MM-yyyy");
            this.DateAllDone = visit.Services.Select(se=>se.ReadyDate).Max().ToString("dd-MMM-yyyy");
            this.CustomerId = visit.CustomerId;
            this.Services = visit.Services.Select(se=>new ServiceDto(se)).ToList();
            this.Currency = visit.Currency;
            this.Price = visit.TotalPrice * visit.Vehicle.PriceFactor;

        }

        /// <summary>
        /// Visit Id property
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Vehicle Id property
        /// </summary>
        public int VehicleId { get; }

        /// <summary>
        /// Arrival date property
        /// </summary>
        public string ArrivalDate { get; }

        /// <summary>
        /// Date all done property
        /// </summary>
        public string DateAllDone { get; }

        /// <summary>
        /// Customer Id property
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Services property
        /// </summary>
        public List<ServiceDto> Services { get; set; } = new List<ServiceDto>();

        /// <summary>
        /// Currency property
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Price property
        /// </summary>
        public decimal Price { get; set; }
    }
}
