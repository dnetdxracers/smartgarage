﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Services.Dto
{
    public class ModelDto
    {
        public ModelDto(Model model)
        {
            this.Id = model.Id;
            this.Name = model.Name;
            this.Manufacturer = model.Manufacturer.Name;
            this.ManufacturerId = model.ManufacturerId;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public int ManufacturerId { get; set; }
    }
}
