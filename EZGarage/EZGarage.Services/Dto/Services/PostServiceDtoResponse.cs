﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Services.Dto.Services
{
    public class PostServiceDtoResponse
    {
        /// <summary>
        /// Empty ctor
        /// </summary>
        public PostServiceDtoResponse()
        {

        }
        /// <summary>
        /// Ctor with service injection
        /// </summary>
        /// <param name="service">Service</param>
        public PostServiceDtoResponse(Service service)
        {
            this.Id = service.Id;
            this.ServiceItemId = service.ServiceItemId;
            this.StatusId = service.StatusId;
            this.VisitId = service.VisitId;
            this.ReadyDate = service.ReadyDate.ToString("dd-MMM-yyyy");
        }

        /// <summary>
        /// Id property
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Service item Id property
        /// </summary>
        public int ServiceItemId { get; set; }

        /// <summary>
        /// Status Id property
        /// </summary>
        public int StatusId { get; set; }

        /// <summary>
        /// Visit Id property
        /// </summary>
        public int VisitId { get; set; }

        /// <summary>
        /// Ready date property
        /// </summary>
        public string ReadyDate { get; set; }
    }
}
