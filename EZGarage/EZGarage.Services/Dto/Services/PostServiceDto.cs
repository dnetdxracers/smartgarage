﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Services.Dto.Services
{
    public class PostServiceDto
    {
        /// <summary>
        /// Empty ctor
        /// </summary>
        public PostServiceDto()
        {

        }
        /// <summary>
        /// Ctor with properties injections
        /// </summary>
        /// <param name="serviceItemId">Service item Id</param>
        /// <param name="statusId">Status Id</param>
        /// <param name="visitId">Visit Id</param>
        /// <param name="readyDate"></param>
        public PostServiceDto(int serviceItemId,
            int statusId,
            int visitId,
            string readyDate)
        {
            this.ServiceItemId = serviceItemId;
            this.StatusId = statusId;
            this.VisitId = visitId;
            this.ReadyDate = readyDate;
        }

        /// <summary>
        /// Service Item id property
        /// </summary>
        public int ServiceItemId { get; set; }

        /// <summary>
        /// Status Id property
        /// </summary>
        public int StatusId { get; set; } = 1;

        /// <summary>
        /// Visit Id property
        /// </summary>
        public int VisitId { get; set; }

        /// <summary>
        /// Ready date property
        /// </summary>
        public string ReadyDate { get; set; }
    }
}
