﻿using EZGarage.Data.Models;

namespace EZGarage.Services.Dto.Services
{
    public class ServiceDto
    {
        public ServiceDto(Service service)
        {
            this.Id = service.Id;

            this.CustomerId = service.Visit.CustomerId;
            this.Customer = $"{service.Visit.Customer.FirstName} {service.Visit.Customer.LastName}";
            this.ServiceItem = service.ServiceItem.Name;
            this.Status = service.Status.Name;
            this.ReadyDate = service.ReadyDate.ToString("dd-MMM-yyyy");
            this.Price = service.ServiceItem.Price;
            this.Currency = service.ServiceItem.Currency;
            this.VehicleId = service.Visit.VehicleId;
            this.VehicleMakeModel = $"{service.Visit.Vehicle.Model.Manufacturer.Name} {service.Visit.Vehicle.Model.Name}";
            this.VehicleRegPlate = service.Visit.Vehicle.RegistrationPlate;
            this.VIN= service.Visit.Vehicle.VIN;
            this.VisitId = service.VisitId;
            this.VisitArrivalDate = service.Visit.ArrivalDate.ToString("dd-MMM-yyy");
        }

        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Customer { get; set; }
        public string ServiceItem { get; set; }
        public string Status { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
        public int VisitId { get; set; }
        public int VehicleId { get; set; }
        public string VehicleMakeModel { get; set; }
        public string VehicleRegPlate { get; set; }
        public string VIN { get; set; }
        public string VisitArrivalDate { get; set; }
        public string ReadyDate { get; set; }
    }
}
