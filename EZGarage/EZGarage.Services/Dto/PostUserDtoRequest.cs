﻿using EZGarage.Data.Models.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Services.Dto
{
    public class PostUserDtoRequest
    {
        public PostUserDtoRequest()
        {

        }
        //VALIDATE ME
        public PostUserDtoRequest(string firstName, string lastName, string phone, string email, int roleId)
        {
            this.RoleId = roleId;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Phone = phone;
            this.Email = email;
        }

        //public PostUserDtoRequest(string firstName, string lastName, string password, string phone, string email)
        //{
        //    this.RoleId = 2;
        //    this.FirstName = firstName;
        //    this.LastName = lastName;
        //    this.Password = password;
        //    this.Phone = phone;
        //    this.Email = email;
        //}

        public int RoleId { get; set; }

        [NameMinMaxLength]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }



    }
}
