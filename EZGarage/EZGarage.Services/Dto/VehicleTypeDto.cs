﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Services.Dto
{
    public class VehicleTypeDto
    {
        public VehicleTypeDto()
        {

        }

        public VehicleTypeDto(VehicleType vehicleType)
        {
            this.Id = vehicleType.Id;
            this.Name = vehicleType.Name;
        }

        public int Id { get; set; }

        public string Name { get; set; }
    }
}
