﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EZGarage.Services.Dto.Visits;

namespace EZGarage.Services.Dto
{
    public class CustomerDtoDetailed
    {
        public CustomerDtoDetailed(User customer)
        {
            this.Id = customer.Id;
            this.FirstName = customer.FirstName;
            this.LastName= customer.LastName;
            this.Phone = customer.Phone;
            this.Email = customer.Email;
            this.Vehicles = customer.Vehicles.Count != 0
                ? customer.Vehicles.Select(ve => new VehicleDto(ve)).ToList()
                : new List<VehicleDto>();
            this.Visits = customer.Visits.Count != 0
                ? customer.Visits.Select(vi => new VisitDto(vi)).ToList()
                : new List<VisitDto>();
        }

        public int Id { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public string Phone { get; }
        public string Email { get; }
        public List<VehicleDto> Vehicles { get; }
        public List<VisitDto> Visits { get; }
    }
}
