﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Services.Dto
{
    public class ManufacturerDto
    {
        /// <summary>
        /// Ctor with id and name params
        /// </summary>
        /// <param name="id">Manufacturer Id</param>
        /// <param name="name">Name</param>
        public ManufacturerDto(int id, string name)
        {
            Id = id;
            Name = name;
        }

        /// <summary>
        /// Id prop
        /// </summary>
        public int Id { get; }
        
        /// <summary>
        /// Name prop
        /// </summary>
        public string Name { get; }
    }
}
