﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Services.Dto
{
    public class VehicleDto
    {
        public VehicleDto(Vehicle vehicle)
        {
            this.Id = vehicle.Id;
            this.CustomerId = vehicle.CustomerId;
            this.CustomerName = vehicle.Customer.FirstName + " " + vehicle.Customer.LastName;
            this.VehicleTypeId = vehicle.VehicleTypeId;
            this.VehicleType = vehicle.VehicleType.Name;
            this.ManufacturerId = vehicle.Model.ManufacturerId;
            this.Manufacturer = vehicle.Model.Manufacturer.Name;
            this.ModelId = vehicle.ModelId;
            this.Model = vehicle.Model.Name;
            this.Year = vehicle.Year;
            this.RegistrationPlate = vehicle.RegistrationPlate;
            this.VIN = vehicle.VIN;
        }

        public int Id { get; }
        public int CustomerId { get; }
        public string CustomerName { get; }
        public int VehicleTypeId { get; }
        public string VehicleType { get; }
        public int ManufacturerId { get; }
        public string Manufacturer { get; }
        public int ModelId { get; }
        public string Model { get; }
        public int Year { get; }
        public string RegistrationPlate { get; }
        public string VIN { get; }
    }
}
