﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Services.Dto
{
    public class UserResetDto
    {
        /// <summary>
        /// Empty ctor
        /// </summary>
        public UserResetDto()
        {

        }

        /// <summary>
        /// Ctor with user reset injection
        /// </summary>
        /// <param name="userReset">User reset</param>
        public UserResetDto(UserReset userReset)
        {
            this.UserId = userReset.UserId;
            this.TemporaryPassword = userReset.TemporaryPassword;
            this.ExpiryDate = userReset.ExpiryDate;
            this.Email = userReset.Email;
        }

        /// <summary>
        /// Id prop
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Temp pass prop
        /// </summary>
        public string TemporaryPassword { get; set; }

        /// <summary>
        /// Expiry date prop
        /// </summary>
        public DateTime ExpiryDate { get; set; }

        /// <summary>
        /// Email prop
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Bool prop for expiration
        /// </summary>
        public bool IsExpired { get; set; }
    }
}
