﻿using EZGarage.Data.Models;

namespace EZGarage.Services.Dto.ServiceItems
{
    public class ServiceItemDto
    {
        /// <summary>
        /// Empty ctor
        /// </summary>
        public ServiceItemDto()
        {

        }
        /// <summary>
        /// Ctor with service item entity input
        /// </summary>
        /// <param name="serviceItem">Service item</param>
        public ServiceItemDto(ServiceItem serviceItem)
        {
            this.Name = serviceItem.Name;
            this.Price = serviceItem.Price;
            this.Id = serviceItem.Id;
        }

        /// <summary>
        /// Name property
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Price property
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Service item Id property
        /// </summary>
        public int Id { get; set; }
    }
}
