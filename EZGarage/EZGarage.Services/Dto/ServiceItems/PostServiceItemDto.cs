﻿namespace EZGarage.Services.Dto.ServiceItems
{
    public class PostServiceItemDto
    {
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="name">Service item name</param>
        /// <param name="price">Service item price</param>
        public PostServiceItemDto(string name, decimal price)
        {
            this.Name = name;
            this.Price = price;
        }

        /// <summary>
        /// Name property
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Price property
        /// </summary>
        public decimal Price { get; set; }
    }
}
