﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EZGarage.Services.Dto
{
    public class CustomerDtoSmall
    {

        public CustomerDtoSmall(User customer)
        {
            this.Id = customer.Id;
            this.Role = "Customer";
            this.Name = customer.FirstName + " " + customer.LastName;
            this.Phone = customer.Phone;
            this.Email = customer.Email;
            this.VehicleIds = customer.Vehicles.Select(ve => ve.Id).ToList();

        }


        public int Id { get; }
        public string Role { get; }
        public string Name { get; }
        public string Phone { get; }
        public string Email { get; }
        public List<int> VehicleIds { get; }
    }
}
