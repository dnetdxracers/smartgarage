﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Services.Dto
{
    public class UserLoginDto
    {
        public UserLoginDto(User user)
        {
            this.Id = user.Id;

            
            if (user.RoleId == 1)
                this.Role = "employee";
            else if (user.RoleId == 2)
                this.Role = "customer";
            else
                this.Role = "uncategorized";

            this.Name = user.FirstName + " " + user.LastName;
            this.Email = user.Email;
        }

        public int Id { get; set; }
        public int RoleId { get; set; }
        public string Role { get; }
        public string Name { get; }
        public string Email { get; }

    }

}
