﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Services.Dto
{
    public class PostVehicleDto
    {
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="vehicleTypeId">Vehicle type Id</param>
        /// <param name="modelId">Model Id</param>
        /// <param name="year">Year</param>
        /// <param name="registrationPlate">Registration plate</param>
        /// <param name="VIN">Vehicle identification number</param>
        public PostVehicleDto(int customerId,
            int vehicleTypeId,
            int modelId,
            int year,
            string registrationPlate,
            string VIN)
        {
            this.CustomerId = customerId;
            this.VehicleTypeId = vehicleTypeId;
            this.ModelId = modelId;
            this.Year = year;
            this.RegistrationPlate = registrationPlate;
            this.VIN = VIN;
        }

        /// <summary>
        /// Customer Id property
        /// </summary>
        public int CustomerId { get; set; }
        /// <summary>
        /// Vehicle type Id property
        /// </summary>
        public int VehicleTypeId { get; set; }
        /// <summary>
        /// Model Id property
        /// </summary>
        public int ModelId { get; set; }
        /// <summary>
        /// Year property
        /// </summary>
        public int Year { get; set; }
        /// <summary>
        /// Registration plate property
        /// </summary>
        public string RegistrationPlate { get; set; }
        /// <summary>
        /// Vehicle identification number property
        /// </summary>
        public string VIN { get; set; }
    }
}
