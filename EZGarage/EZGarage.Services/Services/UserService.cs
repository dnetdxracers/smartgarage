﻿using EZGarage.Data;
using EZGarage.Data.Models;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using EZGarage.Services.Util.Helpers;
using EZGarage.Services.Util.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Services.Services
{
    public class UserService : IUserService
    {
        /// <summary>
        /// Database, app settings and context accessor fields
        /// </summary>
        private readonly EZGarageContext context;
        private readonly IMailService mailService;
        private readonly AppSettings appSettings;
        private readonly IHttpContextAccessor contextAccessor;

        /// <summary>
        /// Ctor with db and app settings injections
        /// </summary>
        /// <param name="context">Db</param>
        /// <param name="appSettings">App settings</param>
        public UserService(EZGarageContext context, IOptions<AppSettings> appSettings, IMailService mailService)
        {
            this.context = context;
            this.mailService = mailService;
            this.appSettings = appSettings.Value;
        }

        /// <summary>
        /// Authentication method
        /// </summary>
        /// <param name="model">User input model</param>
        /// <returns></returns>
        public UserModel Authenticate(UserInputModel model)
        {
            var user = this.context.Users.SingleOrDefault(x => x.Email == model.Email && x.Password == model.Password);

            // fix this, exception needed
            if (user == null) return null;

            return new UserModel()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Id = user.Id,
                Username = user.Email,
                RoleId = user.RoleId,
                Token = GenerateJwtToken(user)
            };
        }

        /// <summary>
        /// Method to generate JWT
        /// </summary>
        /// <param name="user">Input user</param>
        /// <returns></returns>
        private string GenerateJwtToken(User user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("id", user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.RoleId.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }



        /// <summary>
        /// Put method to change the role of a customer to employee role
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns></returns>
        public async Task<UserDto> PromoteToEmployeeAsync(int customerId)
        {
            var user = await this.DbGetOneUserAsync(customerId);

            if (user == null || user.RoleId != 2)
                throw new NonExistentItemWithId("Customer", customerId);

            user.RoleId = 1;

            await this.context.SaveChangesAsync();

            return new UserDto(user);
        }

        /// <summary>
        /// Put method to update properties of user
        /// </summary>
        /// <param name="id">User Id</param>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Last name</param>
        /// <param name="phone">Phone</param>
        /// <param name="email">Email</param>
        /// <param name="removedVehicleId"></param>
        /// <returns></returns>
        public async Task<UserDto> PutUserAsync(int id,
            string firstName,
            string lastName,
            string phone,
            string email)
        {
            var user = await this.DbGetOneUserAsync(id)
                ?? throw new NonExistentItemWithId("User", id);

            if (firstName != null)
            {
                user.FirstName = firstName;
            }
            if (lastName != null)
            {
                user.LastName = lastName;
            }
            if (phone != null)
            {
                user.Phone = phone;
            }
            if (email != null)
            {
                user.Email = email;
            }

            await context.SaveChangesAsync();

            return new UserDto(user);
        }



        /// <summary>
        /// Post method to create new user
        /// </summary>
        /// <param name="postUserRequest">User</param>
        /// <returns></returns>
        public async Task<(string, PostUserDtoResponse)> PostUserAsync(PostUserDtoRequest postUserRequest)
        {
            var user = new User
            {
                FirstName = postUserRequest.FirstName,
                LastName = postUserRequest.LastName,
                RoleId = postUserRequest.RoleId,
                Password = Guid.NewGuid().ToString().Substring(0, 15),
                Phone = postUserRequest.Phone,
                Email = postUserRequest.Email
            };
            var password = user.Password;

            if (context.Users.Where(us => !us.IsDeleted).Select(us => us.Email).Any(em => em == user.Email))
                throw new AlreadyExisting("Email", user.Email);

            var result = await this.mailService.SendEmailToUserInitialPassAsync(user);

            await this.context.Users.AddAsync(user);

            await context.SaveChangesAsync();

            return (result, new PostUserDtoResponse(user));
        }

        /// <summary>
        /// Get user id by email async method
        /// </summary>
        /// <param name="email">Email input</param>
        /// <returns></returns>
        public async Task<int> GetUserIdByEmailAsync(string email)
        {
            var user = await this.DbGetAllUsers().FirstOrDefaultAsync(u => u.Email.ToLower() == email.ToLower());
            var userId = user.Id;

            return userId;
        }

        /// <summary>
        /// Send email with password async method
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns></returns>
        public async Task<string> SendEmailToResetPasswordAsync(int userId)
        {
            if (userId < 0)
                throw new NegativeIdSelected();

            var user = await this.DbGetOneUserAsync(userId);
            if (user == null)
                throw new NonExistentItemWithId("User", userId);

            var userReset = await this.CreateNewUserResetAsync(userId);


            var response = await mailService.SendEmailTOUserResetPassowrdAsync(userReset);

            return response;
        }

        public async Task<UserResetDto> CreateNewUserResetAsync(int id)
        {
            var user = await this.DbGetOneUserAsync(id);

            var newPass = Guid.NewGuid().ToString();

            var userReset = new UserReset()
            {
                UserId = user.Id,
                Email = user.Email,
                ExpiryDate = DateTime.Now.AddHours(3),
                TemporaryPassword = newPass,
                IsExpired = false
            };

            await this.context.UserResets.AddAsync(userReset);
            
            await this.context.SaveChangesAsync();

            var userResetDto = new UserResetDto(userReset);

            return userResetDto;
        }

        public void UpdatePassword(int userId, string newPass)
        {
            var user = this.DbGetOneUserAsync(userId).Result;
            user.Password = newPass;

                        //await this.context.UserResets.AddAsync(user);
            context.SaveChanges();
        }

        public async Task<UserReset> VerifyUserResetAsync(UserResetDto userReset)
        {

            var result = await this.context.UserResets.FirstOrDefaultAsync(u => u.Email == userReset.Email && u.IsExpired == false)
                ?? throw new NonExistentItemWithProperty("User reset", "email", userReset.Email);

            if (userReset.TemporaryPassword == result.TemporaryPassword)
            {
                return result;
            }

            throw new NonExistentItemWithProperty("User reset", "email", userReset.Email);
        }



        /// <summary>
        /// Get users by filtration criteria
        /// </summary>
        /// <param name="nameSeeker">Name</param>
        /// <param name="phoneSeeker">Phone</param>
        /// <param name="emailSeeker">Email</param>
        /// <param name="vehicleId">Vehicle Id</param>
        /// <param name="vehiModelSeeker">Vehicle model</param>
        /// <param name="vehiManufSeeker">Vehicle manufacturer</param>
        /// <param name="visitBefore">Date before which to filter by</param>
        /// <param name="visitAfter">Date after which to filter by</param>
        /// <returns></returns>
        public async Task<IEnumerable<CustomerDtoDetailed>> GetManyCustomersAsync(string nameSeeker,
            string phoneSeeker,
            string emailSeeker,
            int? vehicleId,
            string vehiModelSeeker,
            string vehiManufSeeker,
            string visitBefore,
            string visitAfter)
        {
            var customers = this.DbGetAllCustomers();

            if (nameSeeker != null)
            {
                customers = customers.Where(cu => cu.FirstName.ToLower().Contains(nameSeeker.ToLower())
                                                || cu.LastName.ToLower().Contains(nameSeeker.ToLower()));
            }
            if (phoneSeeker != null)
            {
                customers = customers.Where(cu => cu.Phone.Contains(phoneSeeker));
            }
            if (emailSeeker != null)
            {
                customers = customers.Where(cu => cu.Email.Contains(emailSeeker));
            }
            if (vehicleId != null)
            {
                customers = customers.Where(cu => cu.Vehicles
                                     .Any(ve => ve.Id == vehicleId));
            }
            if (vehiModelSeeker != null)
            {
                customers = customers.Where(cu => cu.Vehicles.Any(ve => ve.Model.Name.Contains(vehiModelSeeker)));
            }
            if (vehiManufSeeker != null)
            {
                customers = customers.Where(cu => cu.Vehicles.Any(ve => ve.Model.Manufacturer.Name.Contains(vehiManufSeeker)));
            }
            if (visitAfter != null)
            {
                customers = customers.Where(cu => this.context.Visits
                    .Any(vi => cu.Vehicles
                    .Select(ve => ve.Id)
                    .Contains(vi.VehicleId)
                    && vi.ArrivalDate >= Convert.ToDateTime(visitAfter)));
            }
            if (visitBefore != null)
            {
                customers = customers.Where(cu => this.context.Visits
                    .Any(vi => cu.Vehicles.Select(ve => ve.Id)
                    .Contains(vi.VehicleId)
                    && vi.ArrivalDate <= Convert.ToDateTime(visitBefore)));
            }


            return await customers.Select(u => new CustomerDtoDetailed(u)).ToListAsync();
        }

        /// <summary>
        /// Aux method to get many customers based on filtration for web
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="phone"></param>
        /// <param name="email"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CustomerDtoDetailed>> GetManyMngCustomersWebAsync(int? customerId,
            string name,
            string phone,
            string email)
        {
            var customers = this.DbGetAllCustomers();

            if (customerId != default)
            {
                customers = customers.Where(c => c.Id == customerId);
            }

            if (name != null)
            {
                customers = customers.Where(cu => cu.FirstName.ToLower().Contains(name.ToLower())
                                                || cu.LastName.ToLower().Contains(name.ToLower()));
            }

            if (phone != default)
            {
                customers = customers.Where(c => c.Phone.Contains(phone));
            }

            if (email != default)
            {
                customers = customers.Where(c => c.Email.Contains(email));
            }

            return await customers.Select(u => new CustomerDtoDetailed(u)).ToListAsync();
        }

        /// <summary>
        /// Get all customers async method
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<CustomerDtoSmall>> GetAllCustomersAsync()
        {
            var customerDtos = await this.DbGetAllCustomers()
                .Select(cu => new CustomerDtoSmall(cu))
                .ToListAsync();

            return customerDtos;
        }

        /// <summary>
        /// Get one customer by id async mehtod
        /// </summary>
        /// <param name="id">Customer id</param>
        /// <returns></returns>
        public async Task<CustomerDtoDetailed> GetOneCustomerAsync(int id)
        {
            var user = await this.DbGetOneCustomerAsync(id);
            if (user == null)
                throw new NonExistentItemWithId("Customer", id);

            var userDto = new CustomerDtoDetailed(user);
            return userDto;
        }

        /// <summary>
        /// Get one user by id async method
        /// </summary>
        /// <param name="id">User Id</param>
        /// <returns></returns>
        public async Task<CustomerDtoDetailed> GetOneUserAsync(int id)
        {

            var user = await this.DbGetOneUserAsync(id);

            if (user == null)
            {
                throw new NonExistentItemWithId("User", id);
            }

            var userDto = new CustomerDtoDetailed(user);

            return userDto;
        }

        /// <summary>
        /// Get user by email and password async method
        /// </summary>
        /// <param name="email">Email</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        public async Task<UserLoginDto> GetUserByEmailPassAsync(string email, string password)
        {

            var user = await this.context.Users.Where(us => !us.IsDeleted).FirstOrDefaultAsync(us => us.Email == email && us.Password == password);

            return user == null ? null : new UserLoginDto(user);
        }

        /// <summary>
        /// Get profile of authenticated user async method
        /// </summary>
        /// <returns></returns>
        public async Task<CustomerDtoDetailed> GetProfileAsync()
        {
            var username = this.contextAccessor.HttpContext.User.Claims.First(i => i.Type == ClaimTypes.NameIdentifier).Value;

            var user = await this.DbGetOneUserByUsernameAsync(username);

            var userDto = new CustomerDtoDetailed(user);
            return userDto;
        }

        /// <summary>
        /// Filters all customers by criteria and order
        /// </summary>
        /// <param name="criteria">firstname, lastname, visitdate</param>
        /// <param name="order">asc, desc, null(default)</param>
        /// <returns>Customers fildered by criteria and order or defaulty by id asc</returns>
        public async Task<(string, IEnumerable<UserDto>)> SortCustomersAsync(string criteria, string order)
        {
            //based on the received tuple will return tuple with the sorted customers and as extra, string with the report how the method sorted them
            criteria = criteria ?? "";
            order = order ?? "";

            var users = this.DbGetAllCustomers();
            string sortedByReport;

            var visits = await users.SelectMany(cu => cu.Visits).ToListAsync();


            (sortedByReport, users) = (criteria.ToLower(), order.ToLower()) switch
            {
                ("firstname", "asc") => ("First Name Asc", users.OrderBy(cu => cu.FirstName)),
                ("firstname", null) => ("First Name Asc (default)", users.OrderBy(cu => cu.FirstName)),
                ("firstname", "desc") => ("First Name Desc", users.OrderByDescending(cu => cu.FirstName)),

                ("lastname", "asc") => ("Last Name Asc ", users.OrderBy(cu => cu.LastName)),
                ("lastname", null) => ("Last Name Asc (default)", users.OrderBy(cu => cu.LastName)),
                ("lastname", "desc") => ("Last Name Desc", users.OrderByDescending(cu => cu.LastName)),

                ("visitdate", "asc") => ("Visit Date Asc", users.OrderBy(cu => cu.Visits.Select(vi => vi.ArrivalDate).Max())),
                ("visitdate", null) => ("Visit Date Asc (default)", users.OrderBy(cu => cu.Visits.Select(vi => vi.ArrivalDate).Max())),
                ("visitdate", "desc") => ("Visit Date Desc", users.OrderByDescending(cu => cu.Visits.Select(vi => vi.ArrivalDate).Max())),

                _ => ("Id Asc", users.OrderBy(cu => cu.Id))
            };


            return (sortedByReport, users.Select(cu => new UserDto(cu)));
        }


        /// <summary>
        /// Deletes an User of type Customer
        /// </summary>
        /// <param name="id">id of customer</param>
        /// <returns>String with result message</returns>
        public async Task<String> DeleteCustomerAsync(int id)
        {
            var user = await this.DbGetOneUserAsync(id);
            if (user == null || user.IsDeleted || user.RoleId != 2)
                throw new NonExistentItemWithId("Customer", id);

            user.Vehicles.ForEach(ve => ve.IsDeleted = true);

            user.IsDeleted = true;

            await context.SaveChangesAsync();

            return $"Customer with Id {id}: {user.FirstName} {user.LastName} deleted";
        }

        /// <summary>
        /// Gets one customer async, full data.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<User> DbGetOneCustomerAsync(int id)
        {
            var user = await this.DbGetAllCustomers()
                .FirstOrDefaultAsync(cu => cu.Id == id) ??
                throw new NonExistentItemWithId("Customer", id);

            return user;
        }

        /// <summary>
        /// Fast method for getting a user. Primary data only.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<User> DbGetOneUserAsync(int id)
        {
            return await this.context.Users.FirstOrDefaultAsync(cu => cu.Id == id);
        }

        //DBGETALL-CAN RETURN NULL--------------------------------------------------------

        //--------------------------------------------------------------------------------

        /// <summary>
        /// Query db for all users
        /// </summary>
        /// <returns></returns>
        private IQueryable<User> DbGetAllUsers()
        {
            var users = this.context.Users.Where(us => !us.IsDeleted)
                .Include(us => us.Role)
                .Include(us => us.Vehicles)
                .Include(us => us.Visits);


            return users;
        }

        /// <summary>
        /// Query db for all customers, full data
        /// </summary>
        /// <returns></returns>
        private IQueryable<User> DbGetAllCustomers()
        {
            var customers = this.context.Users.Where(cu => !cu.IsDeleted)
                .Include(cu => cu.Role)
                .Include(cu => cu.Vehicles)
                    .ThenInclude(ve => ve.Model)
                        .ThenInclude(mo => mo.Manufacturer)
                .Include(cu => cu.Vehicles)
                    .ThenInclude(ve => ve.VehicleType)
                .Include(cu => cu.Visits)
                    .ThenInclude(vi => vi.Services)
                        .ThenInclude(se => se.ServiceItem)
                .Include(cu => cu.Visits)
                    .ThenInclude(vi => vi.Services)
                        .ThenInclude(se => se.Status);

            return customers;
        }

        /// <summary>
        /// Query db for all emplyees
        /// </summary>
        /// <returns></returns>
        private IQueryable<User> DbGetAllEmployees()
        {
            var employees = this.DbGetAllUsers().Where(em => em.RoleId == 1 && em.IsDeleted == false);

            return employees;
        }

        /// <summary>
        /// Get user by username async method
        /// </summary>
        /// <param name="email">Username email</param>
        /// <returns></returns>
        private async Task<User> DbGetOneUserByUsernameAsync(string email)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.Email.ToLower() == email.ToLower())
                ?? throw new NonExistentItemWithProperty("User", "username", email);

            return user;
        }
    }
}
