﻿using EZGarage.Data;
using EZGarage.Data.Models;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Services.Services
{
    public class VehicleTypeService : IVehicleTypeService
    {
        /// <summary>
        /// Database field
        /// </summary>
        private readonly EZGarageContext context;

        /// <summary>
        /// Ctor with db injection
        /// </summary>
        /// <param name="context">Db</param>
        public VehicleTypeService(EZGarageContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get all async
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<VehicleTypeDto>> GetAllAsync()
        {
            var vehTypes = await this.DbGetAll().OrderBy(vt=>vt.Id).ToListAsync();

            return vehTypes.Select(v => new VehicleTypeDto(v));
        }

        /// <summary>
        /// Get vehicle type by id async method
        /// </summary>
        /// <param name="id">Vehicle type Id</param>
        /// <returns></returns>
        public async Task<VehicleTypeDto> GetVehicleTypeByIdAsync(int id)
        {
            var vehicleType = await this.DbGetAll().FirstOrDefaultAsync(vt => vt.Id == id);

            var result = new VehicleTypeDto(vehicleType);

            return result;
        }

        /// <summary>
        /// Get vehicle type by name async method
        /// </summary>
        /// <param name="type">Vehicle type name</param>
        /// <returns></returns>
        public async Task<VehicleTypeDto> GetVehicleTypeByNameAsync(string type)
        {
            var vehicleType = await this.DbGetAll().FirstOrDefaultAsync(vt => vt.Name.ToLower() == type.ToLower());

            var result = new VehicleTypeDto(vehicleType);

            return result;
        }

        /// <summary>
        /// Auxiliary method to query db for all vehicle types
        /// </summary>
        /// <returns></returns>
        private IQueryable<VehicleType> DbGetAll()
        {
            return this.context.VehicleTypes;
        }
    }
}
