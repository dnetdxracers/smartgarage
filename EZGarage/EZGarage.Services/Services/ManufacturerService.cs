﻿using EZGarage.Data;
using EZGarage.Data.Models;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Services.Services
{
    public class ManufacturerService : IManufacturerService
    {
        /// <summary>
        /// Database field
        /// </summary>
        private readonly EZGarageContext context;

        /// <summary>
        /// Ctor with db injection
        /// </summary>
        /// <param name="context">Db</param>
        public ManufacturerService(EZGarageContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Method to get manufacturer by id
        /// </summary>
        /// <param name="id">Manufacturer id</param>
        /// <returns></returns>
        public async Task<Manufacturer> GetManufacturerByIdAsync(int id)
        {
            var manufacturers = await this.GetAllAsync();
            var manufacturer = manufacturers
                .FirstOrDefault(m => m.Id == id)
                ?? throw new NonExistentItemWithId("Manufacturer", id);

            return manufacturer;
        }

        /// <summary>
        /// Method to get manufacturer by name
        /// </summary>
        /// <param name="name">Manufacturer name</param>
        /// <returns></returns>
        public async Task<Manufacturer> GetManufacturerByNameAsync(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new NonExistentItemWithName("Manufacturer", name);

            var manufacturers = await this.GetAllAsync();
            var manufacturer = manufacturers
                .FirstOrDefault(m => m.Name.ToLower() == name.ToLower())
                ?? throw new NonExistentItemWithName("Manufacturer", name);

            return manufacturer;
        }

        /// <summary>
        /// Method to get a collection of all manufacturers
        /// </summary>
        /// <returns></returns>
        /// Potentially needs a .ToList() after DbGetAll()
        public async Task<ICollection<Manufacturer>> GetAllAsync()
        {
            var manufacturers = await context.Manufacturers.OrderBy(ma => ma.Id).ToListAsync();

            return manufacturers;
        }


        /// <summary>
        /// Create new manufacturer
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Newly created manufacturer</returns>
        public async Task<Manufacturer> PostAsync(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new NameNullOrEmpty("Manufacturer");

            var manufacturer = new Manufacturer
            {
                Name = name
            };

            var isExisiting = await context.Manufacturers.AnyAsync(ma => ma.Name.ToLower() == name.ToLower());

            if (isExisiting)
                return null;

            await context.Manufacturers.AddAsync(manufacturer);

            await context.SaveChangesAsync();

            return manufacturer;
        }

        /// <summary>
        /// Change manufacturer property
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<Manufacturer> PutAsync(int id, string newName)
        {
            if (id < 0)
                throw new NegativeIdSelected();

            if (string.IsNullOrEmpty(newName))
                throw new NameNullOrEmpty("Manufacturer");

            var manufacturer = await this.GetManufacturerByIdAsync(id);

            manufacturer.Name = newName;

            await context.SaveChangesAsync();

            return manufacturer;
        }

        /// <summary>
        /// Get manufacturers for select list for drop downs
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetManufacturersSelectList()
        {

            List<SelectListItem> manufacturers = this.GetAllAsync().Result
                .OrderBy(m => m.Name)
                    .Select(n =>
                    new SelectListItem
                    {
                        Value = n.Name,
                        Text = n.Name
                    }).ToList();
            var manufTip = new SelectListItem()
            {
                Value = null,
                Text = "--- Select Manufacturer ---"
            };
            manufacturers.Insert(0, manufTip);
            return new SelectList(manufacturers, "Value", "Text");

        }

    }
}
