﻿using EZGarage.Data.Models;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using EZGarage.Services.Util.Helpers;
using EZGarage.Services.Util.Models;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Services.Services
{
    public class MailService : IMailService
    {
        /// <summary>
        /// Mail settings field
        /// </summary>
        private readonly MailSettings _mailSettings;

        /// <summary>
        /// Ctor with mail settings injection
        /// </summary>
        /// <param name="mailSettings"></param>
        public MailService(IOptions<MailSettings> mailSettings)
        {
            _mailSettings = mailSettings.Value;
        }

        /// <summary>
        /// Send email async method
        /// </summary>
        /// <param name="mailRequest">Mail request entity</param>
        /// <returns></returns>
        public async Task SendEmailAsync(MailRequest mailRequest)
        {
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
            email.To.Add(MailboxAddress.Parse(mailRequest.ToEmail));
            email.Subject = mailRequest.Subject;
            var builder = new BodyBuilder();
            if (mailRequest.Attachments != null)
            {
                byte[] fileBytes;
                foreach (var file in mailRequest.Attachments)
                {
                    if (file.Length > 0)
                    {
                        using (var ms = new MemoryStream())
                        {
                            file.CopyTo(ms);
                            fileBytes = ms.ToArray();
                        }
                        builder.Attachments.Add(file.FileName, fileBytes, ContentType.Parse(file.ContentType));
                    }
                }
            }
            builder.HtmlBody = mailRequest.Body;
            email.Body = builder.ToMessageBody();
            using var smtp = new SmtpClient();
            smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);
            smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
            await smtp.SendAsync(email);
            smtp.Disconnect(true);
        }


        /// <summary>
        /// Send email to user on successful registration async method
        /// </summary>
        /// <param name="user">User</param>
        /// <returns></returns>
        public async Task<string> SendEmailToUserInitialPassAsync(User user)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Greetings,");
            stringBuilder.AppendLine(Environment.NewLine);
            stringBuilder.AppendLine("Your registration at EZGarage was successful!");
            stringBuilder.AppendLine(Environment.NewLine);
            stringBuilder.AppendLine($"Here is your password, keep it safe: {user.Password}");

            var mailRequest = new MailRequest
            {
                Subject = "EZGarage: Successful Registration",
                Body = stringBuilder.ToString(),
                ToEmail = user.Email
            };

            await SendEmailAsync(mailRequest);

            return $"New user email sent succesfully to {user.Email} with their password.";
        }


        /// <summary>
        /// Send email to recover password async method
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="newPassword">New password</param>
        /// <returns></returns>
        public async Task<string> SendEmailTOUserResetPassowrdAsync(UserResetDto user)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Greetings,");
            stringBuilder.AppendLine(Environment.NewLine);
            stringBuilder.AppendLine("This is an automated response from EZGarage.");
            stringBuilder.AppendLine(Environment.NewLine);
            stringBuilder.AppendLine($"Your activation code is: {user.TemporaryPassword}. It will expire in 3 hours!");
            stringBuilder.AppendLine(Environment.NewLine);
            stringBuilder.AppendLine("Copy this link in your browser to reset your password: ");
            stringBuilder.AppendLine(Environment.NewLine);
            stringBuilder.AppendLine("http://localhost:5000/resetpassword/");

            var mailRequest = new MailRequest
            {
                Subject = "Password reset EZGarage",
                Body = stringBuilder.ToString(),
                ToEmail = user.Email
            };

            await SendEmailAsync(mailRequest);

            return $"Passowrd for user with {user.UserId} reset. Email sent succesfully to {user.Email} with an activation code.";

        }


    }
}
