﻿using EZGarage.Data;
using EZGarage.Data.Models;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Services.Services
{
    public class ModelService : IModelService
    {
        /// <summary>
        /// Database field
        /// </summary>
        private readonly EZGarageContext context;

        /// <summary>
        /// Ctor with db injection
        /// </summary>
        /// <param name="context">Db</param>
        public ModelService(EZGarageContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Method to get model by id
        /// </summary>
        /// <param name="id">Model id</param>
        /// <returns></returns>
        public async Task<ModelDto> GetModelByIdAsync(int id)
        {
            var model = await this.DbGetAll()
                .FirstOrDefaultAsync(mo => mo.Id == id) ??
                throw new NonExistentItemWithId("Model", id);

            return new ModelDto(model);
        }

        /// <summary>
        /// Method to get model by name
        /// </summary>
        /// <param name="name">Model name</param>
        /// <returns></returns>
        public async Task<ModelDto> GetModelByNameAsync(string name)
        {
            var model = await this.DbGetAll()
                .FirstOrDefaultAsync(m => m.Name.ToLower() == name.ToLower()) ??
                throw new NonExistentItemWithName("Model", name);

            return new ModelDto(model);
        }

        /// <summary>
        /// Get list of all models
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ModelDto>> GetAllAsync()
        {
            var models = await this.DbGetAll().OrderBy(mo=>mo.Id).ToListAsync();

            return models.Select(mo => new ModelDto(mo));
        }

        /// <summary>
        /// Get method to obtain select list items of models
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetModelsSelectList()
        {

            List<SelectListItem> models = this.GetAllAsync().Result
                .OrderBy(m => m.Name)
                    .Select(n =>
                    new SelectListItem
                    {
                        Value = n.Name,
                        Text = n.Name
                    }).ToList();
            var manufTip = new SelectListItem()
            {
                Value = null,
                Text = "--- Select Model ---"
            };
            models.Insert(0, manufTip);
            return new SelectList(models, "Value", "Text");

        }

        /// <summary>
        /// Create a new model
        /// </summary>
        /// <param name="name"></param>
        /// <param name="manufacturerId"></param>
        /// <param name="manufacturerName"></param>
        /// <returns></returns>
        public async Task<ModelDto> PostAsync(string name, int manufacturerId)
        {
            if (manufacturerId < 0)
                throw new NegativeIdSelected();

            if (string.IsNullOrEmpty(name))
                throw new NameNullOrEmpty("Model");

            var manufacturer = await context.Manufacturers.FirstOrDefaultAsync(ma => ma.Id == manufacturerId) ??
                throw new NonExistentItemWithId("Manufacturer", manufacturerId);

            var model = new Model
            {
                Name = name,
                ManufacturerId = manufacturer.Id
            };

            var models = await this.GetAllAsync();
            var isExisting = models.Any(mo => mo.Name.ToLower() == name.ToLower());
            if (isExisting)
                throw new AlreadyExisting("Model", name);

            context.Models.Add(model);
            await context.SaveChangesAsync();

            return new ModelDto(model);
        }

        /// <summary>
        /// Change model properties
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newName"></param>
        /// <returns></returns>
        public async Task<ModelDto> PutAsync(int id, string newName)
        {
            if (id < 0)
                throw new NegativeIdSelected();

            if(string.IsNullOrEmpty(newName))
                throw new NameNullOrEmpty("Model");

            var model = await this.DbGetAll().FirstOrDefaultAsync(mo => mo.Id == id) ??
                throw new NonExistentItemWithId("Model", id);

            model.Name = newName;

            await context.SaveChangesAsync();

            return new ModelDto(model);
        }



        /// <summary>
        /// Auxiliary method to get all models from db
        /// </summary>
        /// <returns></returns>
        private IQueryable<Model> DbGetAll()
        {
            return this.context.Models.Include(mo => mo.Manufacturer);
        }
    }
}
