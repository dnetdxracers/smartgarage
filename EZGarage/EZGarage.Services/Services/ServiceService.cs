﻿using EZGarage.Data;
using EZGarage.Data.Models;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using EZGarage.Services.Dto.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZGarage.Services.Services
{
    public class ServiceService : IServiceService
    {
        /// <summary>
        /// Database field
        /// </summary>
        private readonly EZGarageContext context;

        /// <summary>
        /// Ctor with db injection
        /// </summary>
        /// <param name="context">Db</param>
        public ServiceService(EZGarageContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get service by id async method
        /// </summary>
        /// <param name="id">Service Id</param>
        /// <returns></returns>
        public async Task<ServiceDto> GetOneByIdAsync(int id)
        {
            var service = await this.DbGetOneAsync(id)
                ?? throw new NonExistentItemWithId("Service", id);

            var serviceDto = new ServiceDto(service);

            return serviceDto;
        }

        /// <summary>
        /// Filter services based on myltiple criteria
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="customerNameSeeker"></param>
        /// <param name="serviceItemId"></param>
        /// <param name="serviceItemNameSeeker"></param>
        /// <param name="vehicleId"></param>
        /// <param name="vehicleRegPlateSeeker"></param>
        /// <param name="vehicleVINSeeker"></param>
        /// <param name="visitId"></param>
        /// <param name="statusId"></param>
        /// <param name="statusNameSeeker"></param>
        /// <param name="visitDateFromDdMMMyyy"></param>
        /// <param name="visitDateToDdMMMyyy"></param>
        /// <returns></returns>
        public async Task<ICollection<ServiceDto>> GetManyAsync(
            int? customerId = null,
            string customerNameSeeker = null,
            int? serviceItemId = null,
            string serviceItemNameSeeker = null,
            int? vehicleId = null,
            string vehicleRegPlateSeeker = null,
            string vehicleVINSeeker = null,
            int? visitId = null,
            int? statusId = null,
            string statusNameSeeker = null,
            string visitDateFromDdMMMyyy = null,
            string visitDateToDdMMMyyy = null
            )
        {
            if (customerId < 0 || serviceItemId < 0 || vehicleId < 0 || visitId < 0 || statusId < 0)
                throw new NegativeIdSelected();

            var services = this.DbGetAll();

            if (customerId != default)
                services = services.Where(se => se.Visit.CustomerId == (int)customerId);

            if (string.IsNullOrEmpty(customerNameSeeker) == false)
                services = services.Where(se => se.Visit.Customer.FirstName.ToLower().Contains(customerNameSeeker.ToLower())
                || se.Visit.Customer.LastName.ToLower().Contains(customerNameSeeker.ToLower()));

            if (serviceItemId != default)
                services = services.Where(se => se.ServiceItemId == (int)serviceItemId);

            if (string.IsNullOrEmpty(serviceItemNameSeeker) == false)
                services = services.Where(se => se.ServiceItem.Name.ToLower().Contains(serviceItemNameSeeker.ToLower()));

            if (vehicleId != default)
                services = services.Where(se => se.Visit.VehicleId == vehicleId);

            if (string.IsNullOrEmpty(vehicleRegPlateSeeker) == false)
                services = services.Where(se => se.Visit.Vehicle.RegistrationPlate.ToLower().Contains(vehicleRegPlateSeeker.ToLower()));

            if (string.IsNullOrEmpty(vehicleVINSeeker) == false)
                services = services.Where(se => se.Visit.Vehicle.VIN.ToLower().Contains(vehicleVINSeeker.ToLower()));

            if (visitId != default)
                services = services.Where(se => se.VisitId == (int)visitId);

            if (statusId != default)
                services = services.Where(se => se.StatusId == (int)statusId);

            if (string.IsNullOrEmpty(statusNameSeeker) == false)
                services = services.Where(se => se.Visit.Vehicle.VIN.ToLower().Contains(statusNameSeeker.ToLower()));

            if (string.IsNullOrEmpty(visitDateFromDdMMMyyy) == false)
                services = services.Where(se => se.Visit.ArrivalDate.Date >= Convert.ToDateTime(visitDateFromDdMMMyyy).Date);

            if (string.IsNullOrEmpty(visitDateToDdMMMyyy) == false)
                services = services.Where(se => se.Visit.ArrivalDate.Date <= Convert.ToDateTime(visitDateToDdMMMyyy).Date);

            services = services.OrderBy(se => se.StatusId).ThenBy(se => se.VisitId);

            var serviceDtos = await services.Select(s => new ServiceDto(s)).ToListAsync();

            return serviceDtos;
        }

        /// <summary>
        /// Post service async method
        /// </summary>
        /// <param name="postServiceDto">Post service Dto</param>
        /// <returns></returns>
        public async Task<PostServiceDtoResponse> PostAsync(PostServiceDto serviceDto)
        {
            var visitId = serviceDto.VisitId;
            var statusId = serviceDto.StatusId;
            var serviceItemId = serviceDto.ServiceItemId;
            string readyDate = serviceDto.ReadyDate;

            if (visitId < 0 || statusId < 0 || serviceItemId < 0)
                throw new NegativeIdSelected();

            var visit = await this.context.Visits.FirstOrDefaultAsync(v => v.Id == visitId)??
                throw new NonExistentItemWithId("Visit", visitId);

            if (context.Statuses.Select(s => s.Id).Contains(statusId) == false)
                throw new NonExistentItemWithId("Status", statusId);

            if (context.ServiceItems.Select(s => s.Id).Contains(serviceItemId) == false)
                throw new NonExistentItemWithId("Service item", serviceItemId);

            var service = new Service
            {
                VisitId = visitId,
                StatusId = statusId,
                ServiceItemId = serviceItemId,
                ReadyDate = Convert.ToDateTime(readyDate)
            };

            await context.Services.AddAsync(service);

            //visit.Services.Add(service);

            await this.context.SaveChangesAsync();

            return new PostServiceDtoResponse(service);
        }

        /// <summary>
        /// Soft delete service by id method
        /// </summary>
        /// <param name="id">Service Id</param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int id)
        {
            var service = await this.DbGetOneAsync(id)
                ?? throw new NonExistentItemWithId("Service", id);


            service.IsDeleted = true;
            context.Visits.FirstOrDefault(vi => vi.Services
                    .Any(se => se.Id == id)).Services
                    .Remove(service);

            await this.context.SaveChangesAsync();

            return true;
        }


        /// <summary>
        /// When service is finished use this. Will update visit if all done.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task FinallizeServiceAsync(int id)
        {
            if (id < 0)
                throw new NegativeIdSelected();

            var service = await this.DbGetOneAsync(id);
            var visit = service.Visit;

            if (service == null)
                throw new NonExistentItemWithId("Service", id);

            service.StatusId = 3;
            service.ReadyDate = DateTime.Now;

            if (visit.Services.Select(se => se.StatusId).All(sid => sid == 3))
                visit.ReadyDate = service.ReadyDate;

            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Advance Service Status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ServiceDto> AdvanceServiceAsync(int id)
        {
            if (id < 0)
                throw new NegativeIdSelected();

            var service = await this.DbGetOneAsync(id);
            var visit = service.Visit;

            if (service == null)
                throw new NonExistentItemWithId("Service", id);

            if (service.StatusId > 2)
                throw new ArgumentException("Can't advance further");

            service.StatusId++;
            if (service.StatusId == 3)
                service.ReadyDate = DateTime.Now;

            if (visit.Services.Select(se => se.StatusId).All(sid => sid == 3))
                visit.ReadyDate = service.ReadyDate;

            await context.SaveChangesAsync();
            var newService = await this.DbGetOneAsync(id);

            return new ServiceDto(newService);
        }

        /// <summary>
        /// Auxiliary method to query db for service by id
        /// </summary>
        /// <param name="id">Service Id</param>
        /// <returns></returns>
        private async Task<Service> DbGetOneAsync(int id)
        {
            var service = await this.DbGetAll().FirstOrDefaultAsync(s => s.Id == id);

            return service;
        }

        /// <summary>
        /// Auxiliary method to query db for all services
        /// </summary>
        /// <returns></returns>
        private IQueryable<Service> DbGetAll()
        {
            var services = this.context.Services.Where(se => se.IsDeleted == false)
                .Include(se => se.ServiceItem)
                .Include(se => se.Status)
                .Include(se => se.Visit)
                    .ThenInclude(vi => vi.Vehicle)
                        .ThenInclude(ve => ve.Model)
                            .ThenInclude(mo => mo.Manufacturer)
                .Include(se => se.Visit)
                    .ThenInclude(vi => vi.Services)
                .Include(se => se.Visit)
                    .ThenInclude(vi => vi.Customer);

            return services;
        }
    }
}
