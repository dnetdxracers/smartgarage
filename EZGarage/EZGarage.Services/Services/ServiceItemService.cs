﻿using AutoMapper;
using EZGarage.Data;
using EZGarage.Data.Models;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto.ServiceItems;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Services.Services
{
    public class ServiceItemService : IServiceItemService
    {
        /// <summary>
        /// Database field
        /// </summary>
        private readonly EZGarageContext context;

        /// <summary>
        /// ServiceItem constructor
        /// </summary>
        /// <param name="context">database injection</param>
        public ServiceItemService(EZGarageContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get service item by id async method
        /// </summary>
        /// <param name="id">Service item Id</param>
        /// <returns></returns>
        public async Task<ServiceItemDto> GetOneByIdAsync(int id)
        {
            var serviceItem = await this.DbGetOneByIdAsync(id);

            if (serviceItem == null || serviceItem.IsDeleted)
            {
                throw new NonExistentItemWithId("Service item", id);
            }

            var result = new ServiceItemDto(serviceItem);

            return result;
        }

        /// <summary>
        /// Get service item by id even if deleted async method for reporting
        /// </summary>
        /// <param name="id">Service item Id</param>
        /// <returns></returns>
        public async Task<ServiceItemDto> GetOneByIdWithDeletedAsync(int id)
        {
            var serviceItem = await this.DbGetOneByIdAsync(id)
                ?? throw new NonExistentItemWithId("Service item", id);

            var result = new ServiceItemDto(serviceItem);

            return result;
        }

        /// <summary>
        /// Get service item by name even if deleted async method for reporting
        /// </summary>
        /// <param name="name">Service item name</param>
        /// <returns></returns>
        public async Task<ServiceItemDto> GetOneByNameAsync(string name)
        {
            var serviceItem = await this.DbGetOneByNameAsync(name);

            if (serviceItem == null || serviceItem.IsDeleted)
            {
                throw new NonExistentItemWithName("Service item", name);
            }

            var result = new ServiceItemDto(serviceItem);

            return result;
        }

        /// <summary>
        /// Get service item by name even if deleted async method for reporting
        /// </summary>
        /// <param name="name">Service item name</param>
        /// <returns></returns>
        public async Task<ServiceItemDto> GetOneByNameWithDeletedAsync(string name)
        {
            var serviceItem = await this.DbGetOneByNameAsync(name)
                ?? throw new NonExistentItemWithName("Service item", name);

            var result = new ServiceItemDto(serviceItem);

            return result;
        }

        /// <summary>
        /// Get all service items async method
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<ServiceItemDto>> GetAllAsync()
        {
            var serviceItems = this.DbGetAll().Where(s => !s.IsDeleted);

            var itemDtos = await serviceItems.Select(s => new ServiceItemDto(s)).ToListAsync();

            return itemDtos;
        }

        /// <summary>
        /// Get all service items deleted included async method
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<ServiceItemDto>> GetAllWithDeletedAsync()
        {
            var serviceItems = this.DbGetAll();

            var itemDtos = await serviceItems.Select(s => new ServiceItemDto(s)).ToListAsync();

            return itemDtos;
        }

        /// <summary>
        /// Get collection of service items based on search criteria
        /// </summary>
        /// <param name="name">Name criteria</param>
        /// <param name="minPrice">Minimum price criteria</param>
        /// <param name="maxPrice">Maximum price criteria</param>
        /// <returns></returns>
        public async Task<ICollection<ServiceItemDto>> GetManyAsync(string name,
            decimal? minPrice,
            decimal? maxPrice)
        {
            var serviceItems = this.DbGetAll().Where(s => !s.IsDeleted).AsQueryable();

            if (name != default)
            {
                serviceItems = serviceItems.Where(si => si.Name.ToLower().Contains(name));
            }
            if (minPrice != default)
            {
                serviceItems = serviceItems.Where(si => si.Price >= (decimal)minPrice);
            }
            if (maxPrice != default)
            {
                serviceItems = serviceItems.Where(si => si.Price <= (decimal)maxPrice);
            }

            var itemDtos = await serviceItems.Select(s => new ServiceItemDto(s)).ToListAsync();

            return itemDtos;
        }

        /// <summary>
        /// Get collection of service items based on search criteria
        /// </summary>
        /// <param name="name">Name criteria</param>
        /// <param name="minPrice">Minimum price criteria</param>
        /// <param name="maxPrice">Maximum price criteria</param>
        /// <returns></returns>
        public async Task<ICollection<ServiceItemDto>> GetManyWithDeletedAsync(string name,
            decimal? minPrice,
            decimal? maxPrice)
        {
            var serviceItems = this.DbGetAll().AsQueryable();

            if (name != default)
            {
                serviceItems = serviceItems.Where(si => si.Name.ToLower().Contains(name));
            }
            if (minPrice != default)
            {
                serviceItems = serviceItems.Where(si => si.Price >= (decimal)minPrice);
            }
            if (maxPrice != default)
            {
                serviceItems = serviceItems.Where(si => si.Price <= (decimal)maxPrice);
            }

            var itemDtos = await serviceItems.Select(s => new ServiceItemDto(s)).ToListAsync();

            return itemDtos;
        }

        /// <summary>
        /// Post method to create new service item
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="priceDefaultCurrency">Price in BGN</param>
        /// <returns></returns>
        public async Task<PostServiceItemDto> PostAsync(PostServiceItemDto serviceItemDto)
        {

            if (serviceItemDto.Price < 0)
            {
                throw new NegativePriceException("Service item");
            }

            if (string.IsNullOrEmpty(serviceItemDto.Name))
            {
                throw new NameNullOrEmpty("Service item");
            }
            var serviceItem = new ServiceItem 
            { 
                Name = serviceItemDto.Name,
                Price = serviceItemDto.Price
            };

            context.ServiceItems.Add(serviceItem);
            await context.SaveChangesAsync();

            return serviceItemDto;
        }

        /// <summary>
        /// Put method to modify existing service item
        /// </summary>
        /// <param name="serviceItemId">Service item Id</param>
        /// <param name="newPrice">Price in BGN</param>
        /// <returns></returns>
        public async Task<ServiceItemDto> PutAsync(ServiceItemDto serviceItemDto)
        {
            if (serviceItemDto.Id < 0)
                throw new NegativeIdSelected();

            var serviceItem = await this.DbGetOneByIdAsync(serviceItemDto.Id);

            // If price is different from current, create new service item
            if (serviceItem.Price != serviceItemDto.Price)
            {
                var newServiceItem = new PostServiceItemDto(serviceItemDto.Name, serviceItemDto.Price);
                await this.PostAsync(newServiceItem);
                await this.DeleteAsync(serviceItem.Id);
                await this.context.SaveChangesAsync();
            }
            else
            {
                serviceItem.Name = serviceItemDto.Name;
                context.ServiceItems.Update(serviceItem);
                await this.context.SaveChangesAsync();
            }

            var result = new ServiceItemDto
            {
                Id = serviceItemDto.Id,
                Name = serviceItemDto.Name,
                Price = serviceItemDto.Price
            };

            await context.SaveChangesAsync();

            return result;
        }

        /// <summary>
        /// Delete service item by id async method
        /// </summary>
        /// <param name="serviceItemId">Service item Id</param>
        /// <returns></returns>
        public async Task DeleteAsync(int serviceItemId)
        {
            var serviceItem = await this.DbGetOneByIdAsync(serviceItemId);

            if (serviceItem.IsDeleted == false)
            {
                serviceItem.IsDeleted = true;
            }
            else
            {
                throw new NonExistentItemWithId("Service item", serviceItemId);
            }

            await context.SaveChangesAsync();
        }

        // --------------- Private auxiliary methods ---------------
        /// <summary>
        /// Auxiliary method to query db for service item by name
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private async Task<ServiceItem> DbGetOneByNameAsync(string name)
        {
            var serviceItem = await this.DbGetAll()
                .FirstOrDefaultAsync(s => s.Name.ToLower().Contains(name.ToLower()));

            return serviceItem;
        }

        /// <summary>
        /// Auxiliary method to query db for service item by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private async Task<ServiceItem> DbGetOneByIdAsync(int id)
        {
            var serviceItem = await this.DbGetAll().FirstOrDefaultAsync(s => s.Id == id && s.IsDeleted == false);

            return serviceItem;
        }

        /// <summary>
        /// Auxiliary method to query db for all service items
        /// </summary>
        /// <returns></returns>
        private IQueryable<ServiceItem> DbGetAll()
        {
            var serviceItems = this.context.ServiceItems;

            return serviceItems;
        }
    }
}
