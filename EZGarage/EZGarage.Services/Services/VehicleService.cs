﻿using EZGarage.Data;
using EZGarage.Data.Models;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using EZGarage.Services.Util.Validators;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EZGarage.Services.Services
{
    public class VehicleService : IVehicleService
    {
        /// <summary>
        /// Database and services fields
        /// </summary>
        private readonly EZGarageContext context;

        /// <summary>
        /// Ctor with db, vehicle type, model and manufacturer service injections
        /// </summary>
        /// <param name="context"></param>
        public VehicleService(EZGarageContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get vehicle by id async method
        /// </summary>
        /// <param name="id">Vehicle Id</param>
        /// <returns></returns>
        public async Task<VehicleDto> GetOneByIdAsync(int id)
        {
            var vehicle = await DbGetOneAsync(id)
                ?? throw new NonExistentItemWithId("Vehicle", id);

            var result = new VehicleDto(vehicle);

            return result;
        }

        /// <summary>
        /// Get vehicle by registration plate async method
        /// </summary>
        /// <param name="registration">Registration plate</param>
        /// <returns></returns>
        public async Task<VehicleDto> GetOneByRegPlateAsync(string registration)
        {
            var vehicle = await this.DbGetAll()
                .FirstOrDefaultAsync(v => v.RegistrationPlate.ToLower() == registration.ToLower())
                ?? throw new NonExistentItemWithProperty("Vehicle", "registration plate", registration);

            var result = new VehicleDto(vehicle);

            return result;
        }


        /// <summary>
        /// Get vehicles belonging to customer with Id get async method 
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns></returns>
        public async Task<IEnumerable<VehicleDto>> GetVehiclesByCustomerIdAsync(int customerId)
        {
            var vehicles = this.DbGetAll()
                .Where(v => v.CustomerId == customerId)
                ?? throw new ObjectDoesNotHaveObjectWithId("Vehicle", "customer Id", customerId);

            var result = await vehicles.Select(ve => new VehicleDto(ve)).ToListAsync();

            return result;
        }

        /// <summary>
        /// Get all vehicles async method with optional params
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="customer">Customer name</param>
        /// <param name="vehicleTypeId">Vehicle type Id</param>
        /// <param name="vehicleType">Vehicle type name</param>
        /// <param name="modelId">Model Id</param>
        /// <param name="model">Model name</param>
        /// <param name="year">Year</param>
        /// <param name="regPlate">Vehicle reg plate number</param>
        /// <returns></returns>
        public async Task<ICollection<VehicleDto>> GetAllAsync(int? customerId=null,
            string customerName = null,
            int? vehicleTypeId =null,
            string vehicleType = null,
            int? modelId = null,
            string model = null,
            int? manufacturerId = null,
            string manufacturer = null,
            int? year = null,
            string regPlate = null)
        {
            var vehicles = this.DbGetAll();

            if (customerId != default)
            {
                vehicles = vehicles.Where(v => v.CustomerId == customerId);
            }

            if (customerName != default)
            {
                vehicles = vehicles.Where(v => v.Customer.FirstName.ToLower().Contains(customerName.ToLower()) 
                                    || v.Customer.LastName.ToLower().Contains(customerName.ToLower()));
            }

            if (vehicleTypeId != default)
            {
                vehicles = vehicles.Where(v => v.VehicleType.Id == vehicleTypeId);
            }

            if (vehicleType != default)
            {
                vehicles = vehicles.Where(v => v.VehicleType.Name.ToLower().Contains(vehicleType.ToLower()));
            }

            if (modelId != default)
            {
                vehicles = vehicles.Where(v => v.Model.Id == modelId);
            }

            if (model != default)
            {
                vehicles = vehicles.Where(v => v.Model.Name.ToLower().Contains(model.ToLower()));
            }

            if (manufacturerId != default)
            {
                vehicles = vehicles.Where(v => v.Model.ManufacturerId == manufacturerId);
            }

            if (manufacturer != default)
            {
                vehicles = vehicles.Where(v => v.Model.Manufacturer.Name.ToLower().Contains(manufacturer.ToLower()));
            }

            if (year != default)
            {
                vehicles = vehicles.Where(v => v.Year == year);
            }

            if (regPlate != default)
            {
                vehicles = vehicles.Where(v => v.VIN.ToLower().Contains(regPlate.ToLower()));
            }

            vehicles = vehicles.OrderBy(ve => ve.CustomerId).ThenBy(ve => ve.VehicleTypeId).ThenBy(ve => ve.ModelId).ThenBy(ve => ve.Model.ManufacturerId);

            var result = await vehicles.Select(ve => new VehicleDto(ve)).ToListAsync();

            return result;
        }


        /// <summary>
        /// Post vehicle async method
        /// </summary>
        /// <param name="postVehicleDto">Post vehicle Dto</param>
        /// <returns></returns>
        public async Task<VehicleDto> PostAsync(PostVehicleDto postVehicleDto)
        {
            var vehicle = new Vehicle
            {
                CustomerId = postVehicleDto.CustomerId,
                VehicleTypeId = postVehicleDto.VehicleTypeId,
                ModelId = postVehicleDto.ModelId,
                Year = postVehicleDto.Year,
                RegistrationPlate = postVehicleDto.RegistrationPlate,
                VIN = postVehicleDto.VIN
            };

            if (this.context.Users.FirstOrDefault(cu => cu.Id == postVehicleDto.CustomerId) == null)
                throw new NonExistentItemWithId("Customer", postVehicleDto.CustomerId);

            if (this.context.VehicleTypes.FirstOrDefault(vt => vt.Id == postVehicleDto.VehicleTypeId) == null)
                throw new NonExistentItemWithId("Vehicle type", postVehicleDto.VehicleTypeId);

            if (this.context.Models.FirstOrDefault(mo => mo.Id == postVehicleDto.ModelId) == null)
                throw new NonExistentItemWithId("Model", postVehicleDto.ModelId); 

            if (postVehicleDto.Year > DateTime.Now.Year)
                throw new InvalidYear(postVehicleDto.Year);

            if (!RegPlateValidator.IsRegPlateValid(postVehicleDto.RegistrationPlate))
                throw new InvalidRegPlate(postVehicleDto.RegistrationPlate);

            await context.Vehicles.AddAsync(vehicle);

            this.context.Users
                .FirstOrDefaultAsync(cu => cu.Id == postVehicleDto.CustomerId).Result
                .Vehicles.Add(vehicle);

            await this.context.SaveChangesAsync();

            return new VehicleDto(vehicle);
        }

        /// <summary>
        /// Put method to update vehicle param reg plate
        /// </summary>
        /// <param name="id">Vehicle Id</param>
        /// <param name="registrationPlate">Registration plate</param>
        /// <returns></returns>
        public async Task<VehicleDto> PutAsync(int id, 
            string registrationPlate)
        {
            var vehicle = await this.DbGetOneAsync(id);
            if (vehicle == null)
            {
                throw new NonExistentItemWithId("Vehicle", id);
            }

            if (registrationPlate != null)
            {
                vehicle.RegistrationPlate = registrationPlate;
            }

            await this.context.SaveChangesAsync();

            return new VehicleDto(vehicle);
        }

        /// <summary>
        /// Soft delete vehicle by id method
        /// </summary>
        /// <param name="id">Vehicle Id</param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int id)
        {
            var vehicle = await this.DbGetOneAsync(id)
                ?? throw new NonExistentItemWithId("Vehicle", id);


            var user = await context.Users.FirstOrDefaultAsync(u => u.Vehicles.Contains(vehicle));
            

            vehicle.IsDeleted = true;

            await this.context.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Auxiliary method to query db for a vehicle by id
        /// </summary>
        /// <param name="id">Vehicle Id</param>
        /// <returns></returns>
        private async Task<Vehicle> DbGetOneAsync(int id)
        {
            var vehicle = await this.DbGetAll()
                .FirstOrDefaultAsync(ve => ve.Id == id);

            return vehicle;
        }

        /// <summary>
        /// Auxiliary method to query db for all vehicles
        /// </summary>
        /// <returns></returns>
        private IQueryable<Vehicle> DbGetAll()
        {
            var vehicles = this.context.Vehicles
                .Where(ve => ve.IsDeleted==false)
                .Include(ve => ve.Customer)
                    .ThenInclude(cu => cu.Role)
                .Include(ve => ve.Customer)
                    .ThenInclude(cu => cu.Vehicles)
                        .ThenInclude(ve => ve.Model)
                .Include(ve => ve.Customer)
                    .ThenInclude(cu => cu.Vehicles)
                        .ThenInclude(ve => ve.VehicleType)
                .Include(ve => ve.VehicleType)
                .Include(ve => ve.Model)
                    .ThenInclude(ve => ve.Manufacturer)
                .Where(ve=>ve.Customer.IsDeleted==false);

            return vehicles;
        }


    }
}
