﻿using AutoMapper;
using EZGarage.Data;
using EZGarage.Data.Models;
using EZGarage.Exceptions;
using EZGarage.Services.Contracts;
using EZGarage.Services.Dto;
using EZGarage.Services.Dto.Services;
using EZGarage.Services.Dto.Visits;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Services.Services
{
    public class VisitService : IVisitService
    {
        /// <summary>
        /// Database and service service field
        /// </summary>
        private readonly EZGarageContext context;
        private readonly IServiceService serviceService;

        /// <summary>
        /// Ctor with db injection
        /// </summary>
        /// <param name="context"></param>
        public VisitService(EZGarageContext context, IServiceService serviceService)
        {
            this.context = context;
            this.serviceService = serviceService;
        }

        /// <summary>
        /// Get visit by id async method
        /// </summary>
        /// <param name="id">Visit Id</param>
        /// <returns></returns>
        public async Task<VisitDto> GetOneByIdAsync(int id)
        {
            var visit = await this.DbGetOneAsync(id)
                ?? throw new NonExistentItemWithId("Visit", id);

            var visitDto = new VisitDto(visit);

            return visitDto;
        }

        public async Task<VisitDto> GetOneByServiceId(int serviceId)
        {
            var visit = await this.DbGetAll()
                .FirstOrDefaultAsync(vi => vi.Services.Any(se => se.Id == serviceId)) ??
                throw new NonExistentItemWithId("Service", serviceId);

            return new VisitDto(visit);
        }

        /// <summary>
        /// Get detailed report of a visit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<VisitDtoDetailed> GetDetailedVisitAsync(int id)
        {
            if (id < 0)
                throw new NegativeIdSelected();

            var visit = await this.DbGetOneAsync(id) ??
                throw new NonExistentItemWithId("Visit", id);

            return new VisitDtoDetailed(visit);
        }

        /// <summary>
        /// Get all visits with optional filtration async method
        /// </summary>
        /// <param name="vehicleId">Vehicle Id</param>
        /// <param name="registrationPlate">Registration plate</param>
        /// <param name="vIN">Vehicle identification number</param>
        /// <param name="model">Model name</param>
        /// <param name="modelId">Model Id</param>
        /// <param name="manufacturer">Manufacturer name</param>
        /// <param name="manufacturerId">Manufacturer Id</param>
        /// <param name="dateAfter">Date after which to filter by</param>
        /// <param name="dateBefore">Date before which to filter by</param>
        /// <param name="serviceId">Service Id</param>
        /// <param name="serviceItemId">Service item Id</param>
        /// <param name="serviceItem">Service item name</param>
        /// <param name="statusId">Status Id</param>
        /// <param name="status">Status name</param>
        /// <returns></returns>
        public async Task<ICollection<VisitDtoDetailed>> GetAllAsync(int? customerId = null,
            int? vehicleId = null,
            string registrationPlate = null,
            string vIN = null,
            string model = null,
            int? modelId = null,
            string manufacturer = null,
            int? manufacturerId = null,
            string dateAfter = null,
            string dateBefore = null,
            int? serviceId = null,
            int? serviceItemId = null,
            string serviceItem = null,
            int? statusId = null,
            string status = null)
        {
            var visits = this.DbGetAll();

            if (customerId != default)
            {
                visits = visits.Where(vi => vi.CustomerId == customerId);
            }
            if (vehicleId != default)
            {
                visits = visits.Where(v => v.Vehicle.Id == vehicleId);
            }
            if (registrationPlate != default)
            {
                visits = visits.Where(v => v.Vehicle.RegistrationPlate.ToLower().Contains(registrationPlate.ToLower()));
            }
            if (vIN != default)
            {
                visits = visits.Where(v => v.Vehicle.VIN.ToLower().Contains(vIN.ToLower()));
            }
            if (model != default)
            {
                visits = visits.Where(v => v.Vehicle.Model.Name.ToLower().Contains(model.ToLower()));
            }
            if (modelId != default)
            {
                visits = visits.Where(v => v.Vehicle.ModelId == modelId);
            }
            if (manufacturer != default)
            {
                visits = visits.Where(v => v.Vehicle.Model.Manufacturer.Name.ToLower().Contains(manufacturer.ToLower()));
            }
            if (manufacturerId != default)
            {
                visits = visits.Where(v => v.Vehicle.Model.ManufacturerId == manufacturerId);
            }
            if (dateAfter != default)
            {
                var dateSelector = Convert.ToDateTime(dateAfter);
                visits = visits.Where(v => v.ArrivalDate >= dateSelector);
            }
            if (dateBefore != default)
            {
                var dateSelector = Convert.ToDateTime(dateBefore);
                visits = visits.Where(v => v.ArrivalDate <= dateSelector);
            }
            if (serviceId != default)
            {
                visits = visits.Where(v => v.Services.Any(s => s.Id == serviceId));
            }
            if (serviceItemId != default)
            {
                visits = visits.Where(v => v.Services.Any(s => s.ServiceItemId == serviceItemId));
            }
            if (serviceItem != default)
            {
                visits = visits.Where(v => v.Services.Any(s => s.ServiceItem.Name.ToLower().Contains(serviceItem.ToLower())));
            }
            if (statusId != default)
            {
                visits = visits.Where(v => v.Services.Any(s => s.StatusId == statusId));
            }
            if (status != default)
            {
                visits = visits.Where(v => v.Services.Any(s => s.Status.Name.ToLower().Contains(status.ToLower())));
            }

            visits = visits.OrderByDescending(vi => vi.ArrivalDate);

            var visitDtos = await visits.Select(vi => new VisitDtoDetailed(vi)).ToListAsync();

            return visitDtos;
        }

        /// <summary>
        /// Post async method to create a new visit 
        /// </summary>
        /// <param name="postVisitDto">Post visit Dto</param>
        /// <returns></returns>
        public async Task<VisitDtoResponse> PostAsync(PostVisitDto postVisitDto)
        {
            var customerId = postVisitDto.CustomerId;
            var vehicleId = postVisitDto.VehicleId;
            var arrivalDate = Convert.ToDateTime(postVisitDto.ArrivalDate);
            var serItemIds = postVisitDto.ServiceIteIds;
            var serItemDates = postVisitDto.ServiceItemFinishDates;


            if (customerId <= 0 || vehicleId <= 0)
                throw new NegativeIdSelected();

            if (serItemIds.Length != serItemDates.Length)
                throw new ArgumentException("Ids and Dates for services must be equal in number");

            var vehicle = await context.Vehicles.FirstOrDefaultAsync(ve => ve.Id == vehicleId);

            if (vehicle == null)
                throw new NonExistentItemWithId("Vehicle", vehicleId);

            if (vehicle.CustomerId != customerId)
                throw new XDoesNotBelongToY("Vehicle", vehicleId, "customer", customerId);

            if (context.Users.Select(us => us.Id).Contains(customerId) == false)
                throw new NonExistentItemWithId("Customer", customerId);

            //if (context.ServiceItems.Select(si => si.Id).Any(si => serItemIds.Contains(si) == false))
            //    throw new NonExistentItemWithId("Service item");

            var visit = new Visit
            {
                CustomerId = customerId,
                VehicleId = vehicleId,
                ArrivalDate = arrivalDate,
                TotalPrice = this.context.ServiceItems.Where(si => serItemIds.Contains(si.Id)).Select(si => si.Price).Sum(), //MULTIPLY BY API CURRENT RATE
                ReadyDate = serItemDates.Select(date => Convert.ToDateTime(date)).Max()
            };

            await this.context.Visits.AddAsync(visit);
            await this.context.SaveChangesAsync();

            for (var i = 0; i < serItemIds.Length; i++)
            {
                await serviceService.PostAsync(new PostServiceDto(serItemIds[i], 1, visit.Id, serItemDates[i]));
            }

            var visitDto = new VisitDtoResponse(visit);

            return visitDto;
        }

        /// <summary>
        /// Method for deleting a visit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteAsync(int id)
        {
            var visit = await context.Visits.FirstOrDefaultAsync(vi => vi.Id == id) ??
                throw new NonExistentItemWithId("Visit", id);

            context.Visits.Remove(visit);
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Auxiliary method to query db for visit by id
        /// </summary>
        /// <param name="id">Visit Id</param>
        /// <returns></returns>
        private async Task<Visit> DbGetOneAsync(int id)
        {
            var visit = await this.DbGetAll().FirstOrDefaultAsync(vi => vi.Id == id);

            return visit;
        }

        /// <summary>
        /// Auxiliary method to query db for all visits
        /// </summary>
        /// <returns></returns>
        private IQueryable<Visit> DbGetAll()
        {
            var visits = this.context.Visits
                .Include(vi => vi.Customer)
                    .ThenInclude(cu => cu.Role)
                .Include(vi => vi.Vehicle)
                        .ThenInclude(ve => ve.Model)
                            .ThenInclude(mo => mo.Manufacturer)
                .Include(vi => vi.Vehicle)
                        .ThenInclude(ve => ve.VehicleType)
                .Include(vi => vi.Services)
                    .ThenInclude(se => se.ServiceItem)
                .Include(vi => vi.Services)
                    .ThenInclude(se => se.Status);

            return visits;
        }
    }
}
