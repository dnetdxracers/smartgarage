﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace EZGarage.Services.Util.Validators
{
    public class RegPlateValidator
    {
        public static bool IsRegPlateValid(string regPlate)
        {
            if (regPlate.Length < 7 || regPlate.Length > 8 || Regex.Match(regPlate, @"[A - Z]{ 1,2}\d{ 4}
                [A-Z]{ 2}").Success)
            {
                return false;
            }
            return true;
        }
    }
}
