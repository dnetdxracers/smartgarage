﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Services.Util.Helpers
{
    public class MailSettings
    {
        /// <summary>
        /// Mail address of sender property
        /// </summary>
        public string Mail { get; set; }

        /// <summary>
        /// Display name of sender property
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Password for authentication property
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Host property
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Port property
        /// </summary>
        public int Port { get; set; }
    }
}
