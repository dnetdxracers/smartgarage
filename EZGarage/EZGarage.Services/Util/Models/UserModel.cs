﻿using EZGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EZGarage.Services.Util.Models
{
    public class UserModel
    {

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public int RoleId { get; set; }
        public string Token { get; set; }

    }
}
