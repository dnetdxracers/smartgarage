﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Services.Util.Models
{
    public class MailRequest
    {
        /// <summary>
        /// Email address of receiver property
        /// </summary>
        public string ToEmail { get; set; }

        /// <summary>
        /// Subject of email property
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Body of email property
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Attachments property
        /// </summary>
        public List<IFormFile> Attachments { get; set; }
    }
}
