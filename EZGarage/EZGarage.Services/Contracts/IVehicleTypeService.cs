﻿using EZGarage.Data.Models;
using EZGarage.Services.Dto;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Services.Contracts
{
    public interface IVehicleTypeService
    {
        Task<IEnumerable<VehicleTypeDto>> GetAllAsync();
        Task<VehicleTypeDto> GetVehicleTypeByIdAsync(int id);
        Task<VehicleTypeDto> GetVehicleTypeByNameAsync(string type);
    }
}