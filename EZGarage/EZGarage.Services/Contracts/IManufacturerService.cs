﻿using EZGarage.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZGarage.Services.Contracts
{
    public interface IManufacturerService
    {
        Task<ICollection<Manufacturer>> GetAllAsync();
        Task<Manufacturer> GetManufacturerByIdAsync(int id);
        Task<Manufacturer> GetManufacturerByNameAsync(string name);
        Task<Manufacturer> PostAsync(string name);
        Task<Manufacturer> PutAsync(int id, string newName);

        IEnumerable<SelectListItem> GetManufacturersSelectList();
    }
}