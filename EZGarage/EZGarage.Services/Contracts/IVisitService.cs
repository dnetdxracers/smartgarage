﻿using EZGarage.Services.Dto.Visits;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZGarage.Services.Contracts
{
    public interface IVisitService
    {
        Task<ICollection<VisitDtoDetailed>> GetAllAsync(int? customerId = null,
            int? vehicleId = null,
            string registrationPlate = null,
            string vIN = null,
            string model = null,
            int? modelId = null,
            string manufacturer = null,
            int? manufacturerId = null,
            string dateAfter = null,
            string dateBefore = null,
            int? serviceId = null,
            int? serviceItemId = null,
            string serviceItem = null,
            int? statusId = null,
            string status = null);
        Task<VisitDtoDetailed> GetDetailedVisitAsync(int id);
        Task<VisitDto> GetOneByIdAsync(int id);
        Task DeleteAsync(int id);
        Task<VisitDtoResponse> PostAsync(PostVisitDto postVisitDto);
        Task<VisitDto> GetOneByServiceId(int serviceId);
    }
}