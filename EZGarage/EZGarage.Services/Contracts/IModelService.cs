﻿using EZGarage.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZGarage.Services.Contracts
{
    public interface IModelService
    {
        Task<IEnumerable<ModelDto>> GetAllAsync();
        Task<ModelDto> GetModelByIdAsync(int id);
        Task<ModelDto> GetModelByNameAsync(string name);
        Task<ModelDto> PostAsync(string name, int manufacturerId);
        Task<ModelDto> PutAsync(int id, string newName);

        IEnumerable<SelectListItem> GetModelsSelectList();
    }
}