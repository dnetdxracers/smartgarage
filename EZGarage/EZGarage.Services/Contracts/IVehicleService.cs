﻿using EZGarage.Data.Models;
using EZGarage.Services.Dto;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZGarage.Services.Contracts
{
    public interface IVehicleService
    {
        Task<VehicleDto> GetOneByIdAsync(int id);
        Task<VehicleDto> GetOneByRegPlateAsync(string registration);
        Task<ICollection<VehicleDto>> GetAllAsync(int? customerId=null,
            string customerName = null,
            int? vehicleTypeId = null,
            string vehicleType = null,
            int? modelId = null,
            string model = null,
            int? manufacturerId = null,
            string manufacturer = null,
            int? year = null,
            string regPlate = null);
        Task<VehicleDto> PostAsync(PostVehicleDto postVehicleDto);
        Task<VehicleDto> PutAsync(int id,
            string registrationPlate);
        Task<bool> DeleteAsync(int id);
        Task<IEnumerable<VehicleDto>> GetVehiclesByCustomerIdAsync(int customerId);
    }
}