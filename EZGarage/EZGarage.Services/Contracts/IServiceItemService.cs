﻿using EZGarage.Services.Dto.ServiceItems;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZGarage.Services.Contracts
{
    public interface IServiceItemService
    {
        Task<ServiceItemDto> GetOneByIdAsync(int id);
        Task<ServiceItemDto> GetOneByIdWithDeletedAsync(int id);
        Task<ServiceItemDto> GetOneByNameAsync(string name);
        Task<ServiceItemDto> GetOneByNameWithDeletedAsync(string name);
        Task<ICollection<ServiceItemDto>> GetAllAsync();
        Task<ICollection<ServiceItemDto>> GetAllWithDeletedAsync();
        Task<ICollection<ServiceItemDto>> GetManyAsync(string name,
            decimal? minPrice,
            decimal? maxPrice);
        Task<ICollection<ServiceItemDto>> GetManyWithDeletedAsync(string name,
            decimal? minPrice,
            decimal? maxPrice);
        Task<PostServiceItemDto> PostAsync(PostServiceItemDto itemDto);
        Task<ServiceItemDto> PutAsync(ServiceItemDto serviceItemDto);
        Task DeleteAsync(int serviceItemId);
    }
}