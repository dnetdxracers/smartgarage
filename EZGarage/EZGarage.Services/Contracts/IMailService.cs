﻿using EZGarage.Data.Models;
using EZGarage.Services.Dto;
using EZGarage.Services.Util.Models;
using System.Threading.Tasks;

namespace EZGarage.Services.Contracts
{
    public interface IMailService
    {
        Task SendEmailAsync(MailRequest mailRequest);
        Task<string> SendEmailToUserInitialPassAsync(User user);
        Task<string> SendEmailTOUserResetPassowrdAsync(UserResetDto userReset);
    }
}