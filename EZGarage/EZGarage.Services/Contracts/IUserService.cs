﻿using EZGarage.Data.Models;
using EZGarage.Services.Dto;
using EZGarage.Services.Util.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZGarage.Services.Contracts
{
    public interface IUserService
    {
        UserModel Authenticate(UserInputModel model);
        Task<User> DbGetOneCustomerAsync(int id);
        Task<User> DbGetOneUserAsync(int id);
        Task<string> DeleteCustomerAsync(int id);
        Task<IEnumerable<CustomerDtoSmall>> GetAllCustomersAsync();
        Task<IEnumerable<CustomerDtoDetailed>> GetManyCustomersAsync(string nameSeeker, string phoneSeeker, string emailSeeker, int? vehicleId, string vehiModelSeeker, string vehiManufSeeker, string visitBefore, string visitAfter);
        Task<CustomerDtoDetailed> GetOneCustomerAsync(int id);
        Task<UserLoginDto> GetUserByEmailPassAsync(string email, string password);
        Task<CustomerDtoDetailed> GetOneUserAsync(int id);
        Task<CustomerDtoDetailed> GetProfileAsync();
        //Task<PostUserDtoResponse> PostUserAsync(PostUserDtoRequest postUserRequest);
        Task<UserDto> PromoteToEmployeeAsync(int customerId);
        Task<UserDto> PutUserAsync(int id, string firstName, string lastName, string phone, string email);
        Task<string> SendEmailToResetPasswordAsync(int userId);
        Task<(string, IEnumerable<UserDto>)> SortCustomersAsync(string criteria, string order);

        Task<(string, PostUserDtoResponse)> PostUserAsync(PostUserDtoRequest postUserRequest);

        Task<int> GetUserIdByEmailAsync(string email);

        Task<IEnumerable<CustomerDtoDetailed>> GetManyMngCustomersWebAsync(int? customerId,
            string name,
            string phone,
            string email);

        void UpdatePassword(int userId, string newPass);

        Task<UserReset> VerifyUserResetAsync(UserResetDto userReset);

        Task<UserResetDto> CreateNewUserResetAsync(int id);
    }
}