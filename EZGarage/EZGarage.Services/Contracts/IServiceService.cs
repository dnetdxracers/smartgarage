﻿using EZGarage.Services.Dto;
using EZGarage.Services.Dto.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZGarage.Services.Contracts
{
    public interface IServiceService
    {
        Task<bool> DeleteAsync(int id);
        Task<ICollection<ServiceDto>> GetManyAsync(int? customerId = null, string customerNameSeeker = null, int? serviceItemId = null, string serviceItemNameSeeker = null, int? vehicleId = null, string vehicleRegPlateSeeker = null, string vehicleVINSeeker = null, int? visitId = null, int? statusId = null, string statusNameSeeker = null, string visitDateFromDdMMMyyy = null, string visitDateToDdMMMyyy = null);
        Task<ServiceDto> GetOneByIdAsync(int id);
        Task<PostServiceDtoResponse> PostAsync(PostServiceDto serviceDto);
        Task<ServiceDto> AdvanceServiceAsync(int id);
    }
}