<img src="https://ezgarage.blob.core.windows.net/logotrasnparent/logo_verticalslim_normal.png" alt="logo-ezgarage" width="300px" style="margin-top: 20px;">

### EZGarage

EZGarage: An ASP.NET Core Web Application by dNet.dXracers

<img src="https://i.ibb.co/3pDD8dX/dnetdxracerslogo.png" alt="logo-dnetdxracers" width="100px" style="margin-top: 20px;"/>

Team members: <a href="https://gitlab.com/cinderglacier">Stoyan Kuklev</a>, <a href="https://gitlab.com/yanastaneva8">Yana Staneva</a>

# Overview
EZGarage is developed to serve the day-to-day management needs of a high-end luxury auto-repair shop. There are 2 main components: API and MVC web applications. Both are built with ASP.NET Core 3.1. 

Required tools to setup EZGarage:

* <a href="https://visualstudio.microsoft.com/">Microsoft Visual Studio</a> with <a href="https://dotnet.microsoft.com/download/dotnet/3.1">ASP.NET Core 3.1</a>
* <a href = "https://www.sqlshack.com/how-to-install-sql-server-express-edition/">SQL Server</a> and <a href="https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15">SQL Server Management Studio</a>

# Getting started
1. Open <a href="https://visualstudio.microsoft.com/">Microsoft Visual Studio</a> and follow these <a href="https://docs.microsoft.com/en-us/visualstudio/get-started/tutorial-open-project-from-repo-visual-studio-2019?view=vs-2019&tabs=vs168later">Instructions for Cloning a Repo</a>
2. Paste the link of the EZGarage repo: https://gitlab.com/dnetdxracers/smartgarage 
3. When the cloning is ready, you should be able to see the following items in the Solution Explorer <img src="https://i.ibb.co/25TphJ5/solution-Explorer.png" alt="solution-explorer" width="300px" style="margin-top: 20px;"/> 
4. In order to setup the database locally, first make sure that the SQL Server is <a href=https://www.mssqltips.com/sqlservertip/6307/how-to-stop-and-start-sql-server-services/>running</a>. 
Then, please open your **Package Manager Console** <a href=https://docs.microsoft.com/en-us/nuget/consume-packages/install-use-packages-powershell>(Tools => NuGet Package Manager => Package Manager Console)</a> and set the _Default Project_ to **EZGarage.Data** as shown below:

<img src="https://i.ibb.co/Cwg8TfH/pm-Console.png" width="600px">. 

Then enter the following commands in the console:

* <i>add-migration initial</i>
* <i>update-database</i>

4. At the top of the screen, your main menu should look like this: <img src="https://i.ibb.co/Ln7GHX3/mainMenu.png" alt="main-menu" style="margin-top: 20px;" />. 
Make sure to have the **EZGarage.Web** Startup Project selected. Then hit the green triangle button <img src="https://i.ibb.co/vLpZjK5/button.png" alt="button" width="80px" style="margin-top: 20px;"/>.
5. If everything worked out fine, the EZGarage Home Index page should open in your main browser: <img src="https://i.ibb.co/GWQt8dx/home-Index.png" alt="home" width="600px" style="margin-top: 20px;"/>

## Technical Details
# Authorization
There are 3 authorization roles altogether, namely:
1. Guest users - have access to all publicly available resources,
2. Customers - viewers, who can view and print privately available items upon authentication,
3. Employees - admins, who can create, read, update and delete various entities upon authentication.

# Layers
Following the best <a href=https://en.wikipedia.org/wiki/Multitier_architecture>Layered Acrhitecture</a> principles for maximum **decoupling**, **maintainability**, **extensibility** and **testability**, the EZGarage application is split into 3 main layers as follows:

1. Data Access Layer - EZGarage.Data
2. Business Logic Layer - EZGarage.Services
3. Presentation Layer - EZGarage.Web

There are 2 more libraries included in the application for error handling and testing purposes:
- Custom Exceptions Library - EZGarage.Exceptions
- Unit Testing Library - EZGarage.Tests

# 1. Data Access Layer: Entity Framework Core & Database
EZGarage utilizes **EF 3.1.5** (<a href=https://docs.microsoft.com/en-us/ef/>Entity Framework</a>) - the standard **ORM** (<a href="https://en.wikipedia.org/wiki/Object%E2%80%93relational_mapping">Object-Relational Mapping</a>) framework for .NET applications in order to map the CLR objects to tables in the <a href=https://en.wikipedia.org/wiki/SQL>SQL</a> database.

We have followed the _Code First Approach_ to build the EZGarage relational database. The following models are the building blocks of the application and are located in the **EZGarage.Data** layer:
* Manufacturer
* Model
* Role (user roles for authentication, 1 = Employee, 2 = Customer; Employees are also both admins and customers of EZGarage)
* Service (a set of service items) 
* Service item (a single unit of service offered by EZGarage) 
* Status (1 = Not Started, 2 = In Progress, 3 = Ready)
* User
* User reset (only instantiated upon a reset password request)
* Vehicle (has model, manufacturer, vehicle type and belongs to a customer)
* Vehicle type (1 = car, 2 = motorbike)
* Visit (a set of services, applied to a specified vehicle upon customer request)

The properties of each objects and the relations between the objects are set within each class via the attributes provided by the _System.ComponentModel.DataAnnotations_ namespace and are illustrated in the diagram below.

![database](https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/database.PNG)


# 2. Business Logic Layer: Services
The business logic as per the Technical Requirements is implemented in the **EZGarage.Services** project. The main principles we have strived to adhere to are <a href=https://en.wikipedia.org/wiki/KISS_principle>KISS</a>, <a href=https://en.wikipedia.org/wiki/SOLID>SOLID</a> and <a href=https://en.wikipedia.org/wiki/Don%27t_repeat_yourself>DRY</a>. Each service has a corresponding _interface_, used to leverage abstraction.


# 3. Presentation Layer: Web & HTTP
EZGarage is built via the **Client-Server Model**, where the communication in the form of the **Request/Response model** is established via **HTTP** (<a href=https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol>HyperText Transfer Protocol</a>). 

All valid **URLs** (<a href = https://en.wikipedia.org/wiki/URL>uniform resource locators</a>) shall return _Successful (2xx)_ or _Client Error (4xx)_ status codes when the request was successully or unsuccessfully processed, respectively.

## API Part
* Overview
EZGarage provides a **RESTful API** (Application Programming Interface, following the <a href=https://en.wikipedia.org/wiki/Representational_state_transfer>REpresentational State Transfer</a> architechtural standard), in order to conform to the decoupled server and client development and maintenance practices. All HTTP request methods for a given rersource are implemented in REST as usual:
1. GET - to view a representation of the resource,
2. POST - to create an instance of the resource,
3. PUT - to update the properties of the resource,
4. DELETE - to virtually remove the resource.

* Swagger Documentation
The API functionalities are documented via <a href="https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-5.0&tabs=visual-studio">Swagger</a> and are available at http://localhost:5000/swagger/index.html 
<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/swagger.PNG" width="600px">

* Authentication
In order for registered users to authenticate via the Authorize button:
1. Navigate to the Users API Controller and open the Authenticate Post endpoint.
<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/apiAuth.PNG" width="600px">

2. Enter a valid email and password and click the Execute button.

3. If the authentication was successful, the HTTP response should return the 200 status code and a response body as follows:
<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/apiAuth200.PNG" width="600px">

4. Copy the value corresponding to the "token" key and head to any of the keylock icons on the page.
<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/keylock.PNG" width="80px">

5. After clicking the keylock item, the following prompt should be visible:
<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/authPrompt.PNG" width="300px">

6. In the Value: field, type Bearer and paste your token as shown, then just click Authorize.
<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/bearer.PNG" width="300px">

## MVC Part

In order to achieve separation of concerns, we have followed the <a href="https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller">MVC (Model-View-Controller)</a> architectural pattern. We utilize <a href="https://docs.microsoft.com/en-us/aspnet/core/razor-pages/?view=aspnetcore-5.0&tabs=visual-studio">Razor pages</a> to embed C# code in the views' HTML.

The static frontend components such as the HTML, CSS, images and JavaScript, are all located in the <a href="https://docs.microsoft.com/en-us/aspnet/core/fundamentals/static-files?view=aspnetcore-5.0">wwwroot</a> folder.

The front end is built via <a href="https://getbootstrap.com/">Bootstrap 5.0.1</a>. 

### Views:
# Public views
Header: 
<img src="https://i.ibb.co/y5DFq0R/public-Header.png" alt="public-header" style="margin-top: 20px;"/>

Home page (http://localhost:5000/)

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/homeIndex.PNG" width="600px" />

Services page (http://localhost:5000/services)

The page displays all services offered at EZGarage. There is a search bar which queries all entries in the table below. Further, the entries can be sorted by cliking on the column titles.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/servicesHome.PNG" width="600px">

About Us page(http://localhost:5000/AboutUs)

A view with customer reviews.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/aboutusHome.PNG" width="600px">

Contact Us page(http://localhost:5000/contactus)

A contact form with client side validations, which sends an email with the entered information to ezgaragecustomerservice@gmail.com. Note that if a user wishes to register at EZGarage, they can send an enquiry to the employee team via this form as only employees are allowed to register new customers.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/contactus.PNG" width="600px">


Login page (http://localhost:5000/Account)

Contains a form where the user can enter their email and password to log into the EZGarage system.
<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/login.PNG" width="600px">

Upon entering an invalid username or password, the following view is displayed. If the user has forgotten their password, they can request a reset of their password.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/wrongPassword.PNG" width="600px">

Forgotten Password page (http://localhost:5000/forgottenpassword)

If the provided email is registered at EZGarage, the user will receive an email with information on how to reset their password.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/forgottenPassword.PNG" width="600px">

# Customer views:
MyServices page (http://localhost:5000/myservices/{id})

Displays all services requested by the logged user with user id {id}.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/myservices.PNG" width="600px">

MyVehicles page (http://localhost:5000/myvehicles/{id})

Displays all vehicles registered for the logged user with user id {id}.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/myvehicles.PNG" width="600px">

MyVisits page http://localhost:5000/myvisits/{id})

Displays all visits recorded at EZGarage for the logged user with user id {id}. The user can:
- request a detailed report for a single visit in Bulgarian leva - the base currency of EZGarage services via View Report (BGN), or
- request a detailed report in a foreign currency (USD/GBP/EUR) via the View Report (ForEx) button.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/myvisits.PNG" width="600px">

Detailed Report page (http://localhost:5000/myvisits/detailedreport/{visitId})

Loads the detailed report for visit with visit id {visitId}. The currency can be selected at the bottom at the page from a drop down menu. The resulting converted total price for the visit is recalculated dynamically via JavaScript. If the user changes their mind about forex, they can still select BGN from the options. The user can download a PDF version of the report as shown below. To obtain a sample PDF, please <a href=requirements/sampleReport.pdf>Click Here</a>.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/forex.PNG" width="600px">
<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/sampleReport.PNG" width="600px">


# Employee views:
Header:

Since employees can also be customers of EZGarage, the whole look and feel of the EZGarage app is changed to employee mode. The main theme color is switched to cyan.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/employeeHeader.PNG" width="600px">

Employee Home page (http://localhost:5000/)

The home page is also updated with the new employee look.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/employeeHome.PNG" width="600px">

EZCustomers page (http://localhost:5000/mngcustomers)

Displays a table with all registered customers. Supports the functionality to:
- add a new vehicle to a user via the Add Vehicle button inline, 
- to display all vehicles associated with the customer via Details, 
- show all visits linked to the customer via Visits, 
- edit the customer details (names, email, phone) via Edit, or 
- delete the customer via Delete.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/mngCustomers.PNG" width="600px">

EZServices page (http://localhost:5000/MngServices)

Shows a detailed table of all services logged at EZGarage. The status of each service is displayed in a user friendly fashion as a progress bar. The employee has the option to:
- advance the status of an active service via the Advance Status button inline, or 
- delete it via Delete.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/mngServices.PNG" width="600px">

EZVehicles page (http://localhost:5000/mngvehicles/index)

Displays a list of all registered vehicles. The employee can:
- create a new vehicle model by clicking the Create New Model button at the top of the page, 
- display the details for a single vehicle via the Details button inline, 
- edit its registration plate only via Edit, or 
- delete the designated vehicle via Delete.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/mngVehicles.PNG" width="600px">

EZVisits page (http://localhost:5000/MngVisits)

Shows all visits registered at EZGarage. Employees have the option to:
- create a new visit viw the button Create New Visit at the top of the page, 
- add services to an existing visit via the Add Services button inline, 
- examine the details for a given visit via Details, or
- delete a visit via Delete.

<img src="https://gitlab.com/dnetdxracers/smartgarage/-/raw/master/requirements/img/mngVisits.PNG" width="600px">
